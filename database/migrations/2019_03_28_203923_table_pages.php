<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablePages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('url');
            $table->string('title_ru');
            $table->text('content_ru')->nullable();
            $table->text('desc_ru')->nullable();
            $table->string('meta_title_ru')->nullable();
            $table->string('meta_desc_ru')->nullable();
            $table->string('meta_key_ru')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
