<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAdministratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('admins', function(Blueprint $table) {
            $table->integer('gender')->default(0);
            $table->integer('birthday')->nullable();
            $table->integer('telegram_two_step')->default(0);
            $table->string('telegram_secret')->nullable();
            $table->integer('chat_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('admins');
    }
}
