<?php
use Illuminate\Database\Seeder;

class AdminsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('admins')->insert([
            'role_id' => 1,
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('111111'),
        ]);
        DB::table('roles')->insert([
            'id' => 1,
            'role' => 'Admin',
            'accesses_id' => '{"view":["1","2","3","4","5","6","7","8","9","10","11"],"edit":["1","2","3","4","5","6","7","8","9","10","11"]}',
        ]);
    }
}
