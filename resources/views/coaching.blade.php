@extends('layouts.app')

@section('content')

    <main id=main data-page-id=coaching>
        <div style="background-image:linear-gradient(134deg,#7c55f2 0,#af89fa 51%,#fab6b6 100%)" data-bg='{ "deg": 134, "colors":[{ "c": "#7C55F2", "p": 0 }, { "c": "#AF89FA", "p": 0.52 }, { "c": "#FAB6B6", "p": 1 }] }' class=bg>
            <div id=waves class=waves_wrap>

                <svg id=wave-1 width=1920px height=371px viewBox="0 0 1920 371" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                    <title>from_1 </title>
                    <desc>Created with Sketch.</desc>
                    <defs>
                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=linearGradient-1>
                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                        </linearGradient>
                    </defs>
                    <g id=1920 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                        <g id=WAVES transform="translate(0.000000, -37.000000)" fill=url(#linearGradient-1) fill-rule=nonzero>
                            <g id=bg transform="translate(-3.000000, -2.000000)">
                                <g id=Cloud_1 transform="translate(3.000000, 0.000000)">
                                    <path d="M1920,408.651004 C643.117748,410.239444 3.11774828,410.239444 0,408.651004 L0,279.469546 C740.574468,345.677945 843.188992,-128.789792 1920,35.669798 C1920,44.1946085 1920,168.521677 1920,408.651004 Z" id=from_1- transform="translate(960.000000, 205.325502) scale(1, -1) translate(-960.000000, -205.325502) "></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>

                <svg id=wave-2 width=1920px height=387px viewBox="0 0 1920 387" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=linearGradient-1>
                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                        </linearGradient>
                    </defs>
                    <g id=1920 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                        <g id=WAVES transform="translate(0.000000, -39.000000)" fill=url(#linearGradient-1) fill-rule=nonzero>
                            <g id=bg transform="translate(0.000000, -2.000000)">
                                <g id=Cloud_2>
                                    <path d="M1920,425.977003 C643.117748,427.565442 3.11774828,427.565442 -2.84217094e-14,425.977003 L-4.54747351e-13,286.99596 C730.197368,344.977003 1195.61921,-102.022997 1920,22.9770026 C1920,31.5018132 1920,165.835146 1920,425.977003 Z" id=from_1- transform="translate(960.000000, 213.988501) scale(1, -1) translate(-960.000000, -213.988501) "></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>
        </div>
        <section class="coaching-hero-section active">
            <div class=container>
                <h1 class="title title-h1">{{__("Track the stuff that matters")}}</h1>
                <div class=content__wrap>
                    <div class=visuals__wrap>
                        <div class=visual--desktop>
                            <img src=# data-src=assets/img/desktop-screen-img.png class="lazy desktop-screen-img" alt="dashboard elaboration">
                        </div>
                        <div class=visual--mobile>
                            <img src=# data-src=assets/img/mobile-screen-img.png class="lazy mobile-screen-img" alt="dashboard elaboration">
                        </div>
                    </div>
                    <div class=text__block>
                        <h2 class=title-h2>{{__("An empathetic AI for professional coaches")}}</h2>
                        <p class=desc-1>{{__("Think of Big Elaboration as the ideal coaching companion that you never knew you needed.")}}</p>
                        <p class=desc-1>{{__("Big Elaboration will dramatically increase your ability as a coach through empathetic tracking & reporting to visualize growth & make it concrete for you and your clients.")}}</p>
                        <a href="{{route('coaching.form')}}" class="button sign-btn js-link leave-link">{{__("sign up for coaches")}}</a>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection