@extends('layouts.app')

@section('content')

    <main id=main data-page-id=demo>
        <div style="background-image:linear-gradient(134deg,#7c55f2 0,#af89fa 51%,#fab6b6 100%)" data-bg='{ "deg": 135, "colors":[{ "c": "#7C55F2", "p": 0 }, { "c": "#A173FF", "p": 0.51 }, { "c": "#CFA5FA", "p": 1 }] }' class=bg>
            <div id=waves class=waves_wrap>

                <svg id=wave-1 width=1920px height=371px viewBox="0 0 1920 371" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                    <title>from_1 </title>
                    <desc>Created with Sketch.</desc>
                    <defs>
                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=linearGradient-1>
                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                        </linearGradient>
                    </defs>
                    <g id=1920 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                        <g id=WAVES transform="translate(0.000000, -37.000000)" fill=url(#linearGradient-1) fill-rule=nonzero>
                            <g id=bg transform="translate(-3.000000, -2.000000)">
                                <g id=Cloud_1 transform="translate(3.000000, 0.000000)">
                                    <path d="M1920,408.651004 C643.117748,410.239444 3.11774828,410.239444 0,408.651004 L0,279.469546 C740.574468,345.677945 843.188992,-128.789792 1920,35.669798 C1920,44.1946085 1920,168.521677 1920,408.651004 Z" id=from_1- transform="translate(960.000000, 205.325502) scale(1, -1) translate(-960.000000, -205.325502) "></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>

                <svg id=wave-2 width=1920px height=387px viewBox="0 0 1920 387" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=linearGradient-1>
                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                        </linearGradient>
                    </defs>
                    <g id=1920 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                        <g id=WAVES transform="translate(0.000000, -39.000000)" fill=url(#linearGradient-1) fill-rule=nonzero>
                            <g id=bg transform="translate(0.000000, -2.000000)">
                                <g id=Cloud_2>
                                    <path d="M1920,425.977003 C643.117748,427.565442 3.11774828,427.565442 -2.84217094e-14,425.977003 L-4.54747351e-13,286.99596 C730.197368,344.977003 1195.61921,-102.022997 1920,22.9770026 C1920,31.5018132 1920,165.835146 1920,425.977003 Z" id=from_1- transform="translate(960.000000, 213.988501) scale(1, -1) translate(-960.000000, -213.988501) "></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>
        </div>
        <section class="beta-section active">
            <div class=container>
                <div class=content__block>
                    <div class=back_wrap>
                        <div class=back>
                            <a href=/developers class="link-text up-text leave-link js-link">{{__("developers")}}</a>

                            <svg width=7px height=6px viewBox="0 0 7 6" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                <title>➜</title>
                                <desc>Created with Sketch.</desc>
                                <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                    <g id=1440---Education-Hover transform="translate(-980.000000, -616.000000)" fill=#FFD2B3>
                                        <g id=Group-2 transform="translate(200.000000, 251.000000)">
                                            <g id=education transform="translate(622.000000, 0.000000)">
                                                <g id=1 transform="translate(0.000000, 199.000000)">
                                                    <g id=READ-MORE transform="translate(76.000000, 161.000000)">
                                                        <path d="M88.2476028,8.18494487 L86.8164062,9.79296875 C86.7434892,9.87369832 86.6601567,9.93554666 86.5664062,9.97851562 C86.4726558,10.0214846 86.3736984,10.0429688 86.2695312,10.0429688 C86.0716136,10.0429688 85.9049486,9.97786523 85.7695312,9.84765625 C85.6341139,9.71744727 85.5664062,9.55729262 85.5664062,9.3671875 C85.5664062,9.26302031 85.5865883,9.1627609 85.6269531,9.06640625 C85.6673179,8.9700516 85.72526,8.88411496 85.8007812,8.80859375 L86.3515625,8.2578125 L82.734375,8.2578125 C82.5286448,8.2578125 82.3548184,8.1875007 82.2128906,8.046875 C82.0709628,7.9062493 82,7.7304698 82,7.51953125 C82,7.31380105 82.0709628,7.13997467 82.2128906,6.99804688 C82.3548184,6.85611908 82.5286448,6.78515625 82.734375,6.78515625 L86.3515625,6.78515625 L85.8007812,6.15234375 C85.7226559,6.06901 85.6640627,5.98242232 85.625,5.89257812 C85.5859373,5.80273393 85.5664062,5.71354211 85.5664062,5.625 C85.5664062,5.45572832 85.636067,5.30924541 85.7753906,5.18554688 C85.9147142,5.06184834 86.0794261,5 86.2695312,5 C86.3763026,5 86.4772131,5.0221352 86.5722656,5.06640625 C86.6673182,5.1106773 86.7486976,5.17187461 86.8164062,5.25 L88.2470346,6.85462372 C88.5849295,7.23361386 88.5851738,7.80566626 88.2476028,8.18494487 Z" id=➜></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div class=text__block>
                        <h2 class="title title-h2">{{__("Join beta")}}</h2>
                        <p class="desc desc-1">{{__("In order to give everyone a great experience we will be slowly sending out invites to the Big Elaboration beta. Please let us know a bit about how you’d like to use Big Elaboration and we’ll send you an invite as soon as we’re ready for you!")}}</p>
                        <div class=form__wrap>
                            <form action="https://elaboration.us15.list-manage.com/subscribe/post?u=f0240fe9ce9d96c3853c2e892&id=8b029db22b" method=post class="form--beta custom-form" novalidate>
                                <div class=input__wrap>
                                    <label for=demo.firstName class=input-label>{{__("First name")}}</label>
                                    <input type=text id=demo.firstName name=FNAME data-validation-name=demo.firstName class="js-link required" required>
                                    <div class=error-tooltip__wrap>
                                        <span class=error-tooltip__content>{{__("You can use only English letters, space, comma, apostrophe, dash, and period (,.'-)")}}</span>
                                    </div>
                                </div>
                                <div class=input__wrap>
                                    <label for=demo.lastName class=input-label>{{__("Last name")}}</label>
                                    <input type=text id=demo.lastName name=LNAME data-validation-name=demo.lastName class="js-link required" required>
                                    <div class=error-tooltip__wrap>
                                        <span class=error-tooltip__content>{{__("You can use only English letters, space, comma, apostrophe, dash, and period (,.'-)")}}</span>
                                    </div>
                                </div>
                                <div class=input__wrap>
                                    <label for=demo.email class=input-label>{{__("Email")}}</label>
                                    <input type=email id=demo.email name=EMAIL data-validation-name=demo.email class="js-link required" required>
                                    <div class=error-tooltip__wrap>
                                        <span class=error-tooltip__content>{{__("Please, check that you include an '@' and '.' in the email address")}}</span>
                                    </div>
                                </div>
                                <div class=input__wrap>
                                    <label for=demo.jobTitle class=input-label>{{__("Job title")}}</label>
                                    <input type=text id=demo.jobTitle name=JOB data-validation-name=demo.jobTitle class="js-link required" required>
                                    <div class=error-tooltip__wrap>
                                        <span class=error-tooltip__content>{{__("Please, fill out this field")}}</span>
                                    </div>
                                </div>
                                <div class=input__wrap>
                                    <label for=demo.orgName class=input-label>{{__("Organization name")}}</label>
                                    <input type=text id=demo.orgName name=ORGNAME data-validation-name=demo.orgName class="js-link required" required>
                                    <div class=error-tooltip__wrap>
                                        <span class=error-tooltip__content>{{__("Please, fill out this field")}}</span>
                                    </div>
                                </div>
                                <div class=input__wrap>
                                    <label for=demo.orgSite class=input-label>{{__("Organization website")}}</label>
                                    <input type=text id=demo.orgSite name=ORGWEBSITE data-validation-name=demo.orgSite class="js-link required" required>
                                    <div class=error-tooltip__wrap>
                                        <span class=error-tooltip__content>{{__("Please, fill out this field")}}</span>
                                    </div>
                                </div>

                                <div class=input__wrap>
                                    <label for=demo.orgType class=input-label>{{__("Organization type")}}</label>
                                    <select name=ORGTYPE id=demo.orgType data-validation-name=demo.orgType class="js-link required">
                                        <option value=Startup>Startup</option>
                                        <option value=SmallBusiness>Small Business</option>
                                        <option value=LargeCompany>Large Company</option>
                                        <option value=Academia>Academia</option>
                                        <option value=Government>Government</option>
                                        <option value=Personal>Personal</option>
                                    </select>
                                </div>

                                <div class=input__wrap>
                                    <label for=demo.industry class=input-label>{{__("Industry")}}</label>
                                    <select name=INDUSTRY id=demo.industry data-validation-name=demo.industry class="js-link required">
                                        <option value=appdevelop>App Development</option>
                                        <option value=Art>Art</option>
                                        <option value=Aerospace>Aerospace</option>
                                        <option value=Construction>Construction</option>
                                        <option value=Defense>Defense</option>
                                        <option value=Energy>Energy</option>
                                        <option value=Education>Education</option>
                                        <option value=Finance>Finance</option>
                                        <option value=Food>Food</option>
                                        <option value=Gaming>Gaming</option>
                                        <option value=Healthcare>Healthcare</option>
                                        <option value=Marketing>Marketing</option>
                                        <option value=Manufacturing>Manufacturing</option>
                                        <option value=Media>Media</option>
                                        <option value=Retail>Retail</option>
                                        <option value=Sciences>Sciences</option>
                                        <option value=Technology>Technology</option>
                                        <option value=Other>Other</option>
                                    </select>
                                </div>

                                <div class=input__wrap>
                                    <label for=demo.location class=input-label>{{__("Location")}}</label>
                                    <select name=LOCATION id=demo.location data-validation-name=demo.location class="js-link required">
                                        <option selected=selected value=US>United States</option>
                                        <option value=CA>Canada</option>
                                        <option value=AD>Andorra</option>
                                        <option value=AE>United Arab Emirates</option>
                                        <option value=AF>Afghanistan</option>
                                        <option value=AG>Antigua and Barbuda</option>
                                        <option value=AI>Anguilla</option>
                                        <option value=AL>Albania</option>
                                        <option value=AM>Armenia</option>
                                        <option value=AO>Angola</option>
                                        <option value=AQ>Antarctica</option>
                                        <option value=AR>Argentina</option>
                                        <option value=AS>American Samoa</option>
                                        <option value=AT>Austria</option>
                                        <option value=AU>Australia</option>
                                        <option value=AW>Aruba</option>
                                        <option value=AX>Aland Islands</option>
                                        <option value=AZ>Azerbaijan</option>
                                        <option value=BA>Bosnia and Herzegovina</option>
                                        <option value=BB>Barbados</option>
                                        <option value=BD>Bangladesh</option>
                                        <option value=BE>Belgium</option>
                                        <option value=BF>Burkina Faso</option>
                                        <option value=BG>Bulgaria</option>
                                        <option value=BH>Bahrain</option>
                                        <option value=BI>Burundi</option>
                                        <option value=BJ>Benin</option>
                                        <option value=BL>Saint Bartelemey</option>
                                        <option value=BM>Bermuda</option>
                                        <option value=BN>Brunei Darussalam</option>
                                        <option value=BO>Bolivia</option>
                                        <option value=BQ>Bonaire, Saint Eustatius and Saba</option>
                                        <option value=BR>Brazil</option>
                                        <option value=BS>Bahamas</option>
                                        <option value=BT>Bhutan</option>
                                        <option value=BV>Bouvet Island</option>
                                        <option value=BW>Botswana</option>
                                        <option value=BY>Belarus</option>
                                        <option value=BZ>Belize</option>
                                        <option value=CC>Cocos (Keeling) Islands</option>
                                        <option value=CD>Congo, The Democratic Republic of the</option>
                                        <option value=CF>Central African Republic</option>
                                        <option value=CG>Congo</option>
                                        <option value=CH>Switzerland</option>
                                        <option value=CI>Cote d'Ivoire</option>
                                        <option value=CK>Cook Islands</option>
                                        <option value=CL>Chile</option>
                                        <option value=CM>Cameroon</option>
                                        <option value=CN>China</option>
                                        <option value=CO>Colombia</option>
                                        <option value=CR>Costa Rica</option>
                                        <option value=CU>Cuba</option>
                                        <option value=CV>Cape Verde</option>
                                        <option value=CW>Curacao</option>
                                        <option value=CX>Christmas Island</option>
                                        <option value=CY>Cyprus</option>
                                        <option value=CZ>Czech Republic</option>
                                        <option value=DE>Germany</option>
                                        <option value=DJ>Djibouti</option>
                                        <option value=DK>Denmark</option>
                                        <option value=DM>Dominica</option>
                                        <option value=DO>Dominican Republic</option>
                                        <option value=DZ>Algeria</option>
                                        <option value=EC>Ecuador</option>
                                        <option value=EE>Estonia</option>
                                        <option value=EG>Egypt</option>
                                        <option value=EH>Western Sahara</option>
                                        <option value=ER>Eritrea</option>
                                        <option value=ES>Spain</option>
                                        <option value=ET>Ethiopia</option>
                                        <option value=FI>Finland</option>
                                        <option value=FJ>Fiji</option>
                                        <option value=FK>Falkland Islands (Malvinas)</option>
                                        <option value=FM>Micronesia, Federated States of</option>
                                        <option value=FO>Faroe Islands</option>
                                        <option value=FR>France</option>
                                        <option value=GA>Gabon</option>
                                        <option value=GB>United Kingdom</option>
                                        <option value=GD>Grenada</option>
                                        <option value=GE>Georgia</option>
                                        <option value=GF>French Guiana</option>
                                        <option value=GG>Guernsey</option>
                                        <option value=GH>Ghana</option>
                                        <option value=GI>Gibraltar</option>
                                        <option value=GL>Greenland</option>
                                        <option value=GM>Gambia</option>
                                        <option value=GN>Guinea</option>
                                        <option value=GP>Guadeloupe</option>
                                        <option value=GQ>Equatorial Guinea</option>
                                        <option value=GR>Greece</option>
                                        <option value=GS>South Georgia and the South Sandwich Islands</option>
                                        <option value=GT>Guatemala</option>
                                        <option value=GU>Guam</option>
                                        <option value=GW>Guinea-Bissau</option>
                                        <option value=GY>Guyana</option>
                                        <option value=HK>Hong Kong</option>
                                        <option value=HM>Heard Island and McDonald Islands</option>
                                        <option value=HN>Honduras</option>
                                        <option value=HR>Croatia</option>
                                        <option value=HT>Haiti</option>
                                        <option value=HU>Hungary</option>
                                        <option value=ID>Indonesia</option>
                                        <option value=IE>Ireland</option>
                                        <option value=IL>Israel</option>
                                        <option value=IM>Isle of Man</option>
                                        <option value=IN>India</option>
                                        <option value=IO>British Indian Ocean Territory</option>
                                        <option value=IS>Iceland</option>
                                        <option value=IT>Italy</option>
                                        <option value=JE>Jersey</option>
                                        <option value=JM>Jamaica</option>
                                        <option value=JO>Jordan</option>
                                        <option value=JP>Japan</option>
                                        <option value=KE>Kenya</option>
                                        <option value=KG>Kyrgyzstan</option>
                                        <option value=KH>Cambodia</option>
                                        <option value=KI>Kiribati</option>
                                        <option value=KM>Comoros</option>
                                        <option value=KN>Saint Kitts and Nevis</option>
                                        <option value=KR>Korea, Republic of</option>
                                        <option value=KW>Kuwait</option>
                                        <option value=KY>Cayman Islands</option>
                                        <option value=KZ>Kazakhstan</option>
                                        <option value=LA>Lao People's Democratic Republic</option>
                                        <option value=LB>Lebanon</option>
                                        <option value=LC>Saint Lucia</option>
                                        <option value=LI>Liechtenstein</option>
                                        <option value=LK>Sri Lanka</option>
                                        <option value=LR>Liberia</option>
                                        <option value=LS>Lesotho</option>
                                        <option value=LT>Lithuania</option>
                                        <option value=LU>Luxembourg</option>
                                        <option value=LV>Latvia</option>
                                        <option value=MA>Morocco</option>
                                        <option value=MC>Monaco</option>
                                        <option value=MD>Moldova, Republic of</option>
                                        <option value=ME>Montenegro</option>
                                        <option value=MF>Saint Martin</option>
                                        <option value=MG>Madagascar</option>
                                        <option value=MH>Marshall Islands</option>
                                        <option value=MK>Macedonia</option>
                                        <option value=ML>Mali</option>
                                        <option value=MM>Myanmar</option>
                                        <option value=MN>Mongolia</option>
                                        <option value=MO>Macao</option>
                                        <option value=MP>Northern Mariana Islands</option>
                                        <option value=MQ>Martinique</option>
                                        <option value=MR>Mauritania</option>
                                        <option value=MS>Montserrat</option>
                                        <option value=MT>Malta</option>
                                        <option value=MU>Mauritius</option>
                                        <option value=MV>Maldives</option>
                                        <option value=MW>Malawi</option>
                                        <option value=MX>Mexico</option>
                                        <option value=MY>Malaysia</option>
                                        <option value=MZ>Mozambique</option>
                                        <option value=NA>Namibia</option>
                                        <option value=NC>New Caledonia</option>
                                        <option value=NE>Niger</option>
                                        <option value=NF>Norfolk Island</option>
                                        <option value=NG>Nigeria</option>
                                        <option value=NI>Nicaragua</option>
                                        <option value=NL>Netherlands</option>
                                        <option value=NO>Norway</option>
                                        <option value=NP>Nepal</option>
                                        <option value=NR>Nauru</option>
                                        <option value=NU>Niue</option>
                                        <option value=NZ>New Zealand</option>
                                        <option value=OM>Oman</option>
                                        <option value=PA>Panama</option>
                                        <option value=PE>Peru</option>
                                        <option value=PF>French Polynesia</option>
                                        <option value=PG>Papua New Guinea</option>
                                        <option value=PH>Philippines</option>
                                        <option value=PK>Pakistan</option>
                                        <option value=PL>Poland</option>
                                        <option value=PM>Saint Pierre and Miquelon</option>
                                        <option value=PN>Pitcairn</option>
                                        <option value=PR>Puerto Rico</option>
                                        <option value=PS>Palestinian Territory</option>
                                        <option value=PT>Portugal</option>
                                        <option value=PW>Palau</option>
                                        <option value=PY>Paraguay</option>
                                        <option value=QA>Qatar</option>
                                        <option value=RE>Reunion</option>
                                        <option value=RO>Romania</option>
                                        <option value=RS>Serbia</option>
                                        <option value=RU>Russian Federation</option>
                                        <option value=RW>Rwanda</option>
                                        <option value=SA>Saudi Arabia</option>
                                        <option value=SB>Solomon Islands</option>
                                        <option value=SC>Seychelles</option>
                                        <option value=SD>Sudan</option>
                                        <option value=SE>Sweden</option>
                                        <option value=SG>Singapore</option>
                                        <option value=SH>Saint Helena</option>
                                        <option value=SI>Slovenia</option>
                                        <option value=SJ>Svalbard and Jan Mayen</option>
                                        <option value=SK>Slovakia</option>
                                        <option value=SL>Sierra Leone</option>
                                        <option value=SM>San Marino</option>
                                        <option value=SN>Senegal</option>
                                        <option value=SO>Somalia</option>
                                        <option value=SR>Suriname</option>
                                        <option value=SS>South Sudan</option>
                                        <option value=ST>Sao Tome and Principe</option>
                                        <option value=SV>El Salvador</option>
                                        <option value=SX>Sint Maarten</option>
                                        <option value=SY>Syrian Arab Republic</option>
                                        <option value=SZ>Swaziland</option>
                                        <option value=TC>Turks and Caicos Islands</option>
                                        <option value=TD>Chad</option>
                                        <option value=TF>French Southern Territories</option>
                                        <option value=TG>Togo</option>
                                        <option value=TH>Thailand</option>
                                        <option value=TJ>Tajikistan</option>
                                        <option value=TK>Tokelau</option>
                                        <option value=TL>Timor-Leste</option>
                                        <option value=TM>Turkmenistan</option>
                                        <option value=TN>Tunisia</option>
                                        <option value=TO>Tonga</option>
                                        <option value=TR>Turkey</option>
                                        <option value=TT>Trinidad and Tobago</option>
                                        <option value=TV>Tuvalu</option>
                                        <option value=TW>Taiwan</option>
                                        <option value=TZ>Tanzania, United Republic of</option>
                                        <option value=UA>Ukraine</option>
                                        <option value=UG>Uganda</option>
                                        <option value=UM>United States Minor Outlying Islands</option>
                                        <option value=UY>Uruguay</option>
                                        <option value=UZ>Uzbekistan</option>
                                        <option value=VC>Saint Vincent and the Grenadines</option>
                                        <option value=VE>Venezuela</option>
                                        <option value=VG>Virgin Islands, British</option>
                                        <option value=VI>Virgin Islands, U.S.</option>
                                        <option value=VN>Vietnam</option>
                                        <option value=VU>Vanuatu</option>
                                        <option value=WF>Wallis and Futuna</option>
                                        <option value=WS>Samoa</option>
                                        <option value=YE>Yemen</option>
                                        <option value=YT>Mayotte</option>
                                        <option value=ZA>South Africa</option>
                                        <option value=ZM>Zambia</option>
                                        <option value=ZW>Zimbabwe</option>
                                    </select>
                                </div>

                                <div class=input__wrap>
                                    <label for=demo.experience class=input-label>{{__("How experienced are you with deep learning?")}}</label>
                                    <select name=EXPERIENCE id=demo.experience data-validation-name=demo.experience class="js-link required">
                                        <option value=firsttime>{{__("This is my first time")}}</option>
                                        <option value=basicconcepts>{{__("I know the basic concepts")}}</option>
                                        <option value=expert>{{__("I am an expert")}}</option>
                                    </select>
                                </div>

                                <div class="textarea__wrap input__wrap">
                                    <label for=demo.detailsUse class=input-label>{{__("Tell us more about how you’d like to use Big Elaboration:")}}</label>
                                    <textarea name=TELLMORE id=demo.detailsUse data-validation-name=demo.detailsUse class="js-link required" required></textarea>
                                    <div class=error-tooltip__wrap>
                                        <span class=error-tooltip__content>{{__("Please, fill out this field")}}</span>
                                    </div>
                                </div>

                                <div style="position:absolute;left:-5000px" aria-hidden=true><input type=text name=b_f0240fe9ce9d96c3853c2e892_8b029db22b tabindex=-1></div>
                                <div class="checkbox__wrap input__wrap">
                                    <input type=checkbox name=CHECKAGREE data-validation-name=demo.checkAgree id=check-agree class=required required onclick=this.blur()>
                                    <label for=check-agree>{{__("I have read and agree to the")}}

                                        <a href=/privacy target=_blank>{{__("Privacy Policy")}}</a></label>
                                </div>
                                <button type=submit class="button js-link">{{__("Requset invite")}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
@endsection