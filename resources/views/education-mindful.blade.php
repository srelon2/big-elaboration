@extends('layouts.app')

@section('content')

    <main id=main data-page-id=case>
        <div style="background-image:linear-gradient(134deg,#7c55f2 0,#af89fa 51%,#fab6b6 100%)" data-bg='{ "deg": 135, "colors":[{ "c": "#7C55F2", "p": 0 }, { "c": "#B479F2", "p": 0.51 }, { "c": "#FABBEA", "p": 1 }] }' class=bg>
            <div id=waves class=waves_wrap>

                <svg id=wave-1 width=1920px height=371px viewBox="0 0 1920 371" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                    <title>from_1 </title>
                    <desc>Created with Sketch.</desc>
                    <defs>
                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=linearGradient-1>
                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                        </linearGradient>
                    </defs>
                    <g id=1920 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                        <g id=WAVES transform="translate(0.000000, -37.000000)" fill=url(#linearGradient-1) fill-rule=nonzero>
                            <g id=bg transform="translate(-3.000000, -2.000000)">
                                <g id=Cloud_1 transform="translate(3.000000, 0.000000)">
                                    <path d="M1920,408.651004 C643.117748,410.239444 3.11774828,410.239444 0,408.651004 L0,279.469546 C740.574468,345.677945 843.188992,-128.789792 1920,35.669798 C1920,44.1946085 1920,168.521677 1920,408.651004 Z" id=from_1- transform="translate(960.000000, 205.325502) scale(1, -1) translate(-960.000000, -205.325502) "></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>

                <svg id=wave-2 width=1920px height=387px viewBox="0 0 1920 387" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=linearGradient-1>
                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                        </linearGradient>
                    </defs>
                    <g id=1920 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                        <g id=WAVES transform="translate(0.000000, -39.000000)" fill=url(#linearGradient-1) fill-rule=nonzero>
                            <g id=bg transform="translate(0.000000, -2.000000)">
                                <g id=Cloud_2>
                                    <path d="M1920,425.977003 C643.117748,427.565442 3.11774828,427.565442 -2.84217094e-14,425.977003 L-4.54747351e-13,286.99596 C730.197368,344.977003 1195.61921,-102.022997 1920,22.9770026 C1920,31.5018132 1920,165.835146 1920,425.977003 Z" id=from_1- transform="translate(960.000000, 213.988501) scale(1, -1) translate(-960.000000, -213.988501) "></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>
        </div>
        <section class="tabs-view-section active">
            <div class=container>
                <div class=tabs_content_container>
                    <div class=back_wrap>
                        <div class=back>
                            <a href="{{route('education')}}" class="link-text up-text js-link leave-link">{{__("education")}}</a>

                            <svg width=7px height=6px viewBox="0 0 7 6" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                <title>➜</title>
                                <desc>Created with Sketch.</desc>
                                <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                    <g id=1440---Education-Hover transform="translate(-980.000000, -616.000000)" fill=#FFD2B3>
                                        <g id=Group-2 transform="translate(200.000000, 251.000000)">
                                            <g id=education transform="translate(622.000000, 0.000000)">
                                                <g id=1 transform="translate(0.000000, 199.000000)">
                                                    <g id=READ-MORE transform="translate(76.000000, 161.000000)">
                                                        <path d="M88.2476028,8.18494487 L86.8164062,9.79296875 C86.7434892,9.87369832 86.6601567,9.93554666 86.5664062,9.97851562 C86.4726558,10.0214846 86.3736984,10.0429688 86.2695312,10.0429688 C86.0716136,10.0429688 85.9049486,9.97786523 85.7695312,9.84765625 C85.6341139,9.71744727 85.5664062,9.55729262 85.5664062,9.3671875 C85.5664062,9.26302031 85.5865883,9.1627609 85.6269531,9.06640625 C85.6673179,8.9700516 85.72526,8.88411496 85.8007812,8.80859375 L86.3515625,8.2578125 L82.734375,8.2578125 C82.5286448,8.2578125 82.3548184,8.1875007 82.2128906,8.046875 C82.0709628,7.9062493 82,7.7304698 82,7.51953125 C82,7.31380105 82.0709628,7.13997467 82.2128906,6.99804688 C82.3548184,6.85611908 82.5286448,6.78515625 82.734375,6.78515625 L86.3515625,6.78515625 L85.8007812,6.15234375 C85.7226559,6.06901 85.6640627,5.98242232 85.625,5.89257812 C85.5859373,5.80273393 85.5664062,5.71354211 85.5664062,5.625 C85.5664062,5.45572832 85.636067,5.30924541 85.7753906,5.18554688 C85.9147142,5.06184834 86.0794261,5 86.2695312,5 C86.3763026,5 86.4772131,5.0221352 86.5722656,5.06640625 C86.6673182,5.1106773 86.7486976,5.17187461 86.8164062,5.25 L88.2470346,6.85462372 C88.5849295,7.23361386 88.5851738,7.80566626 88.2476028,8.18494487 Z" id=➜></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div class=content>
                        <div class=list_wrap>
                            <ul class=anchor_list>

                                <li data-nav-target=1 class="anchor_item link-text js-link up-text js-active">{{__("overview")}}</li>

                                <li data-nav-target=2 class="anchor_item link-text js-link up-text">{{__("materials")}}</li>

                                <li data-nav-target=3 class="anchor_item link-text js-link up-text">{{__("process")}}</li>

                                <li data-nav-target=4 class="anchor_item link-text js-link up-text">{{__("how it works")}}</li>

                                <li data-nav-target=5 class="anchor_item link-text js-link up-text">{{__("reflect with maslo")}}</li>

                            </ul>
                        </div>
                        <div class=text_wrap>
                            <div class=title_wrap>
                                <h2 class="title-h2 low-text">{{__("mindful gratitude")}}</h2>
                            </div>

                            <div data-nav-id=1 class="tab_content active">
                                <div class=subtitle_wrap>
                                    <h4 class="title-h4 low-text">{{__("overview")}}</h4>
                                </div>
                                <div class=decs_wrap>

                                    <p class="description desc-1">{{__("Maslo wellbeing activities help students look inward and get to know themselves on a deeper level. They promote creativity and introspection, enhance problemsolving abilities, and encourage students to draw on their emotions as needed.")}}</p>

                                    <p class="description desc-1">{{__("The goal of this activity is to get acquainted with a breathing exercise that will help increase self-awareness, decrease stress and anxiety, improve concentration, and prevent migraines.")}}</p>

                                </div>
                            </div>

                            <div data-nav-id=2 class=tab_content>
                                <div class=subtitle_wrap>
                                    <h4 class="title-h4 low-text">{{__("materials")}}</h4>
                                </div>
                                <div class=decs_wrap>

                                    <p class="description desc-1">{{__("The Maslo app plays a key role in this activity. Maslo is an AI that features personified voice journaling and allows students to express themselves on an emotional level. The app can be used on any Apple device, including an iPod, iPhone, or iPad.")}}</p>

                                    <p class="description desc-1">{{__("Students will ideally work in groups of three or four to complete the breathing activity, with each student using their own device and Maslo AI.")}}</p>

                                </div>
                            </div>

                            <div data-nav-id=3 class=tab_content>
                                <div class=subtitle_wrap>
                                    <h4 class="title-h4 low-text">{{__("process")}}</h4>
                                </div>
                                <div class=decs_wrap>

                                    <p class="description desc-1">{{__("To start the activity, students will sit down facing their partner. One student will spend three minutes thanking their partner for something that they noticed recently. They can express their gratitude for a specific task their partner completed, or for something nice they did. Students are encouraged to get creative and discuss anything positive that resonated with them.")}}</p>

                                    <p class="description desc-1">{{__("After the first student expresses their gratitude, the partners can switch roles and repeat the process.")}}</p>

                                </div>
                            </div>

                            <div data-nav-id=4 class=tab_content>
                                <div class=subtitle_wrap>
                                    <h4 class="title-h4 low-text">{{__("how it works")}}</h4>
                                </div>
                                <div class=decs_wrap>

                                    <p class="description desc-1">{{__("Gratitude is a feeling, but above all else, it’s a mindset. Specifically, it’s an awareness of the people and things that make us feel appreciative. Gratitude often makes us smile or extend ourselves to others. It reminds us to be thankful for the contributions people make. It is designed to promote positive thinking, and help students see the good in others and the world around them.")}}</p>

                                </div>
                            </div>

                            <div data-nav-id=5 class=tab_content>
                                <div class=subtitle_wrap>
                                    <h4 class="title-h4 low-text">{{__("reflect with maslo")}}</h4>
                                </div>
                                <div class=decs_wrap>

                                    <p class="description desc-1">{{__("After completing the gratitude activity, students will open the Maslo app and journal about their experiences. Maslo will ask students what makes them grateful, how gratitude makes them feel, and whether the concept made sense to them.")}}</p>

                                    <p class="description desc-1">{{__("Students will then be asked to share a real-life example where gratitude helped them overcome a challenging situation.")}}</p>

                                </div>
                            </div>

                            <div class=buttons_wrap>
                                <a href=/education/heroic-virtues class="button js-link prev leave-link">{{__("previous activity")}}</a>
                                <a href=/education/open-palms class="button js-link next leave-link">{{__("next activity")}}</a>
                            </div>
                            <div class=mobile_buttons_wrap>
                                <div class="btn-wrap prev">

                                    <svg width=7px height=6px viewBox="0 0 7 6" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                        <title>➜</title>
                                        <desc>Created with Sketch.</desc>
                                        <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                            <g id=1440---Education-Hover transform="translate(-980.000000, -616.000000)" fill=#FFD2B3>
                                                <g id=Group-2 transform="translate(200.000000, 251.000000)">
                                                    <g id=education transform="translate(622.000000, 0.000000)">
                                                        <g id=1 transform="translate(0.000000, 199.000000)">
                                                            <g id=READ-MORE transform="translate(76.000000, 161.000000)">
                                                                <path d="M88.2476028,8.18494487 L86.8164062,9.79296875 C86.7434892,9.87369832 86.6601567,9.93554666 86.5664062,9.97851562 C86.4726558,10.0214846 86.3736984,10.0429688 86.2695312,10.0429688 C86.0716136,10.0429688 85.9049486,9.97786523 85.7695312,9.84765625 C85.6341139,9.71744727 85.5664062,9.55729262 85.5664062,9.3671875 C85.5664062,9.26302031 85.5865883,9.1627609 85.6269531,9.06640625 C85.6673179,8.9700516 85.72526,8.88411496 85.8007812,8.80859375 L86.3515625,8.2578125 L82.734375,8.2578125 C82.5286448,8.2578125 82.3548184,8.1875007 82.2128906,8.046875 C82.0709628,7.9062493 82,7.7304698 82,7.51953125 C82,7.31380105 82.0709628,7.13997467 82.2128906,6.99804688 C82.3548184,6.85611908 82.5286448,6.78515625 82.734375,6.78515625 L86.3515625,6.78515625 L85.8007812,6.15234375 C85.7226559,6.06901 85.6640627,5.98242232 85.625,5.89257812 C85.5859373,5.80273393 85.5664062,5.71354211 85.5664062,5.625 C85.5664062,5.45572832 85.636067,5.30924541 85.7753906,5.18554688 C85.9147142,5.06184834 86.0794261,5 86.2695312,5 C86.3763026,5 86.4772131,5.0221352 86.5722656,5.06640625 C86.6673182,5.1106773 86.7486976,5.17187461 86.8164062,5.25 L88.2470346,6.85462372 C88.5849295,7.23361386 88.5851738,7.80566626 88.2476028,8.18494487 Z" id=➜></path>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    <a href=/education/heroic-virtues class=button>{{__("previous")}}</a>
                                </div>
                                <div class="btn-wrap next">
                                    <a href=/education/open-palms class="button next">{{__("next")}}</a>

                                    <svg width=7px height=6px viewBox="0 0 7 6" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                        <title>➜</title>
                                        <desc>Created with Sketch.</desc>
                                        <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                            <g id=1440---Education-Hover transform="translate(-980.000000, -616.000000)" fill=#FFD2B3>
                                                <g id=Group-2 transform="translate(200.000000, 251.000000)">
                                                    <g id=education transform="translate(622.000000, 0.000000)">
                                                        <g id=1 transform="translate(0.000000, 199.000000)">
                                                            <g id=READ-MORE transform="translate(76.000000, 161.000000)">
                                                                <path d="M88.2476028,8.18494487 L86.8164062,9.79296875 C86.7434892,9.87369832 86.6601567,9.93554666 86.5664062,9.97851562 C86.4726558,10.0214846 86.3736984,10.0429688 86.2695312,10.0429688 C86.0716136,10.0429688 85.9049486,9.97786523 85.7695312,9.84765625 C85.6341139,9.71744727 85.5664062,9.55729262 85.5664062,9.3671875 C85.5664062,9.26302031 85.5865883,9.1627609 85.6269531,9.06640625 C85.6673179,8.9700516 85.72526,8.88411496 85.8007812,8.80859375 L86.3515625,8.2578125 L82.734375,8.2578125 C82.5286448,8.2578125 82.3548184,8.1875007 82.2128906,8.046875 C82.0709628,7.9062493 82,7.7304698 82,7.51953125 C82,7.31380105 82.0709628,7.13997467 82.2128906,6.99804688 C82.3548184,6.85611908 82.5286448,6.78515625 82.734375,6.78515625 L86.3515625,6.78515625 L85.8007812,6.15234375 C85.7226559,6.06901 85.6640627,5.98242232 85.625,5.89257812 C85.5859373,5.80273393 85.5664062,5.71354211 85.5664062,5.625 C85.5664062,5.45572832 85.636067,5.30924541 85.7753906,5.18554688 C85.9147142,5.06184834 86.0794261,5 86.2695312,5 C86.3763026,5 86.4772131,5.0221352 86.5722656,5.06640625 C86.6673182,5.1106773 86.7486976,5.17187461 86.8164062,5.25 L88.2470346,6.85462372 C88.5849295,7.23361386 88.5851738,7.80566626 88.2476028,8.18494487 Z" id=➜></path>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div class=icon_wrap>
                            <div class=icon_block>
                                <div class=shape>

                                    <svg width=190px height=190px viewBox="0 0 190 190" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                        <title>Shape</title>
                                        <desc>Created with Sketch.</desc>
                                        <defs>
                                            <radialGradient cx=50% cy=50% fx=50% fy=50% r=50% id=radialGradient-1>
                                                <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                                                <stop stop-color=#FFFFFF stop-opacity=0.05 offset=100%></stop>
                                            </radialGradient>
                                        </defs>
                                        <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                            <g id=1440---Education transform="translate(-843.000000, -261.000000)" fill=url(#radialGradient-1)>
                                                <g id=content transform="translate(200.000000, 261.000000)">
                                                    <g id=education transform="translate(643.000000, 0.000000)">
                                                        <g id=2>
                                                            <path d="M106.532053,0 C166.008047,0 206.361018,75.1604491 183.519714,134.09442 C160.67841,193.028391 145.090666,189.56616 118.843295,181.348697 C92.5959232,173.131233 53.7479816,209.67088 22.8692088,174.212236 C-5.10553198,148.544575 -3.53561472,108.849154 7.10489514,67.214306 C21.7847514,9.77405228 47.0560579,6.99720774e-15 106.532053,0 Z" id=Shape></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                                <div class=icon>

                                    <svg width=47px height=38px viewBox="0 0 47 38" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                        <title>icon 1</title>
                                        <desc>Created with Sketch.</desc>
                                        <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                            <g id=1440---Education transform="translate(-916.000000, -297.000000)" fill-rule=nonzero>
                                                <g id=content transform="translate(200.000000, 261.000000)">
                                                    <g id=education transform="translate(643.000000, 0.000000)">
                                                        <g id=2>
                                                            <g id=content transform="translate(54.000000, 37.000000)">
                                                                <g id=icon-1 transform="translate(20.000000, 0.000000)">
                                                                    <g id=speak transform="translate(18.000000, 0.000000)">
                                                                        <path d="M22.974542,3.25939082 C20.4351779,1.15754834 17.0704494,0 13.5,0 C9.9295506,0 6.56482207,1.15754834 4.025458,3.25946552 C1.42961009,5.40821645 0,8.28255458 0,11.3530418 C0,14.4236036 1.42961009,17.2980911 4.025458,19.4466927 C6.56482207,21.5486846 9.9295506,22.7063076 13.5,22.7063076 C13.863496,22.7063076 14.2297652,22.6939082 14.594283,22.6692589 L16.8937002,26.5503361 C17.0551972,26.8228235 17.3413382,26.9922317 17.6525831,26.9998506 C17.6596618,27 17.6665216,27 17.6737463,27 C17.9769637,27 18.2612803,26.8455308 18.4309507,26.5870114 L23.2992875,19.1678563 C25.6875902,17.051747 27,14.2849697 27,11.3531912 C27,8.28247987 25.5704629,5.40814175 22.974542,3.25939082 Z" id=Path-Copy stroke=#FFCFAF stroke-linecap=round stroke-linejoin=round></path>
                                                                        <circle id=Oval-Copy fill=#FFCFAF cx=9.025 cy=11.725 r=1></circle>
                                                                        <circle id=Oval-Copy-2 fill=#FFCFAF cx=13.75 cy=11.725 r=1></circle>
                                                                        <circle id=Oval-Copy-3 fill=#FFCFAF cx=18.475 cy=11.725 r=1></circle>
                                                                    </g>
                                                                    <g id=speak transform="translate(13.500000, 22.500000) scale(-1, 1) translate(-13.500000, -22.500000) translate(0.000000, 9.000000)">
                                                                        <path d="M22.974542,3.25939082 C20.4351779,1.15754834 17.0704494,0 13.5,0 C9.9295506,0 6.56482208,1.15754834 4.025458,3.25946552 C1.42961009,5.40821645 0,8.28255458 0,11.3530418 C0,14.4236036 1.42961009,17.2980911 4.025458,19.4466927 C6.56482208,21.5486846 9.9295506,22.7063076 13.5,22.7063076 C13.8634961,22.7063076 14.2297652,22.6939082 14.594283,22.6692589 L16.8937002,26.5503361 C17.0551972,26.8228235 17.3413382,26.9922317 17.6525831,26.9998506 C17.6596618,27 17.6665216,27 17.6737463,27 C17.9769637,27 18.2612803,26.8455308 18.4309507,26.5870114 L23.2992875,19.1678563 C25.6875902,17.051747 27,14.2849697 27,11.3531912 C27,8.28247987 25.5704629,5.40814175 22.974542,3.25939082 Z" id=Path-Copy stroke=#FFCFAF stroke-linecap=round stroke-linejoin=round></path>
                                                                        <circle id=Oval-Copy fill=#FFCFAF cx=9.025 cy=11.725 r=1></circle>
                                                                        <circle id=Oval-Copy-2 fill=#FFCFAF cx=13.75 cy=11.725 r=1></circle>
                                                                        <circle id=Oval-Copy-3 fill=#FFCFAF cx=18.475 cy=11.725 r=1></circle>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection