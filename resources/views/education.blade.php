@extends('layouts.app')

@section('content')

    <main id=main class=one-page-scroll data-page-id=education>
        <div style="background-image:linear-gradient(134deg,#7c55f2 0,#af89fa 51%,#fab6b6 100%)" data-bg='{ "deg": 135, "colors":[{ "c": "#7C55F2", "p": 0 }, { "c": "#B479F2", "p": 0.51 }, { "c": "#FABBEA", "p": 1 }] }' class=bg>
            <div id=waves class=waves_wrap>

                <svg id=wave-1 width=1920px height=371px viewBox="0 0 1920 371" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                    <title>from_1 </title>
                    <desc>Created with Sketch.</desc>
                    <defs>
                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=linearGradient-1>
                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                        </linearGradient>
                    </defs>
                    <g id=1920 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                        <g id=WAVES transform="translate(0.000000, -37.000000)" fill=url(#linearGradient-1) fill-rule=nonzero>
                            <g id=bg transform="translate(-3.000000, -2.000000)">
                                <g id=Cloud_1 transform="translate(3.000000, 0.000000)">
                                    <path d="M1920,408.651004 C643.117748,410.239444 3.11774828,410.239444 0,408.651004 L0,279.469546 C740.574468,345.677945 843.188992,-128.789792 1920,35.669798 C1920,44.1946085 1920,168.521677 1920,408.651004 Z" id=from_1- transform="translate(960.000000, 205.325502) scale(1, -1) translate(-960.000000, -205.325502) "></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>

                <svg id=wave-2 width=1920px height=387px viewBox="0 0 1920 387" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=linearGradient-1>
                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                        </linearGradient>
                    </defs>
                    <g id=1920 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                        <g id=WAVES transform="translate(0.000000, -39.000000)" fill=url(#linearGradient-1) fill-rule=nonzero>
                            <g id=bg transform="translate(0.000000, -2.000000)">
                                <g id=Cloud_2>
                                    <path d="M1920,425.977003 C643.117748,427.565442 3.11774828,427.565442 -2.84217094e-14,425.977003 L-4.54747351e-13,286.99596 C730.197368,344.977003 1195.61921,-102.022997 1920,22.9770026 C1920,31.5018132 1920,165.835146 1920,425.977003 Z" id=from_1- transform="translate(960.000000, 213.988501) scale(1, -1) translate(-960.000000, -213.988501) "></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>
        </div>
        <section class="education-hero-section active">
            <div class=container>
                <div class=content_container>
                    <div class=title-wrap>
                        <h1 class=title-h1>
                            {{__("education")}}
                        </h1>
                    </div>
                    <div class=content>
                        <div class=text_block>
                            <div class=title>
                                <h4 class=title-h4>{{__("elaboration wellbeing")}}</h4>
                            </div>
                            <div class=description>
                                <p class=desc-1>{{__("Big Elaboration wellbeing activities help students look inward and get to know themselves on a deeper level. They promote creativity and introspection, enhance problem-solving abilities, and encourage students to draw on their emotions as needed.")}}
                                </p>
                                <p class=desc-1>{{__("After completing an activity, students will open the Big Elaboration app and journal about their experience. Big Elaboration will ask students how the activity made them feel, whether the concept made sense to them, and when they might use this newly developed skill.")}}</p>
                            </div>
                            <div class=btn-wrap>
                                <a href=/edu-discount class="button leave-link js-link">{{__("SIGN UP FOR TEACHERS")}}</a>
                            </div>
                        </div>
                        <div class=tabs_block>
                            <ul class=nav_list>

                                <li class="nav_item js-link">
                                    <a class=leave-link href="{{route('education.mindful')}}">
                                        <div class=shape>

                                            <svg width=190px height=190px viewBox="0 0 190 190" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                                <title>Shape</title>
                                                <desc>Created with Sketch.</desc>
                                                <defs>
                                                    <radialGradient cx=50% cy=50% fx=50% fy=50% r=50% id=radialGradient-1>
                                                        <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                                                        <stop stop-color=#FFFFFF stop-opacity=0.05 offset=100%></stop>
                                                    </radialGradient>
                                                </defs>
                                                <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                                    <g id=1440---Education transform="translate(-843.000000, -261.000000)" fill=url(#radialGradient-1)>
                                                        <g id=content transform="translate(200.000000, 261.000000)">
                                                            <g id=education transform="translate(643.000000, 0.000000)">
                                                                <g id=2>
                                                                    <path d="M106.532053,0 C166.008047,0 206.361018,75.1604491 183.519714,134.09442 C160.67841,193.028391 145.090666,189.56616 118.843295,181.348697 C92.5959232,173.131233 53.7479816,209.67088 22.8692088,174.212236 C-5.10553198,148.544575 -3.53561472,108.849154 7.10489514,67.214306 C21.7847514,9.77405228 47.0560579,6.99720774e-15 106.532053,0 Z" id=Shape></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class=feature>
                                            <div class=icon>

                                                <svg width=47px height=38px viewBox="0 0 47 38" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                                    <title>icon 1</title>
                                                    <desc>Created with Sketch.</desc>
                                                    <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                                        <g id=1440---Education transform="translate(-916.000000, -297.000000)" fill-rule=nonzero>
                                                            <g id=content transform="translate(200.000000, 261.000000)">
                                                                <g id=education transform="translate(643.000000, 0.000000)">
                                                                    <g id=2>
                                                                        <g id=content transform="translate(54.000000, 37.000000)">
                                                                            <g id=icon-1 transform="translate(20.000000, 0.000000)">
                                                                                <g id=speak transform="translate(18.000000, 0.000000)">
                                                                                    <path d="M22.974542,3.25939082 C20.4351779,1.15754834 17.0704494,0 13.5,0 C9.9295506,0 6.56482207,1.15754834 4.025458,3.25946552 C1.42961009,5.40821645 0,8.28255458 0,11.3530418 C0,14.4236036 1.42961009,17.2980911 4.025458,19.4466927 C6.56482207,21.5486846 9.9295506,22.7063076 13.5,22.7063076 C13.863496,22.7063076 14.2297652,22.6939082 14.594283,22.6692589 L16.8937002,26.5503361 C17.0551972,26.8228235 17.3413382,26.9922317 17.6525831,26.9998506 C17.6596618,27 17.6665216,27 17.6737463,27 C17.9769637,27 18.2612803,26.8455308 18.4309507,26.5870114 L23.2992875,19.1678563 C25.6875902,17.051747 27,14.2849697 27,11.3531912 C27,8.28247987 25.5704629,5.40814175 22.974542,3.25939082 Z" id=Path-Copy stroke=#FFCFAF stroke-linecap=round stroke-linejoin=round></path>
                                                                                    <circle id=Oval-Copy fill=#FFCFAF cx=9.025 cy=11.725 r=1></circle>
                                                                                    <circle id=Oval-Copy-2 fill=#FFCFAF cx=13.75 cy=11.725 r=1></circle>
                                                                                    <circle id=Oval-Copy-3 fill=#FFCFAF cx=18.475 cy=11.725 r=1></circle>
                                                                                </g>
                                                                                <g id=speak transform="translate(13.500000, 22.500000) scale(-1, 1) translate(-13.500000, -22.500000) translate(0.000000, 9.000000)">
                                                                                    <path d="M22.974542,3.25939082 C20.4351779,1.15754834 17.0704494,0 13.5,0 C9.9295506,0 6.56482208,1.15754834 4.025458,3.25946552 C1.42961009,5.40821645 0,8.28255458 0,11.3530418 C0,14.4236036 1.42961009,17.2980911 4.025458,19.4466927 C6.56482208,21.5486846 9.9295506,22.7063076 13.5,22.7063076 C13.8634961,22.7063076 14.2297652,22.6939082 14.594283,22.6692589 L16.8937002,26.5503361 C17.0551972,26.8228235 17.3413382,26.9922317 17.6525831,26.9998506 C17.6596618,27 17.6665216,27 17.6737463,27 C17.9769637,27 18.2612803,26.8455308 18.4309507,26.5870114 L23.2992875,19.1678563 C25.6875902,17.051747 27,14.2849697 27,11.3531912 C27,8.28247987 25.5704629,5.40814175 22.974542,3.25939082 Z" id=Path-Copy stroke=#FFCFAF stroke-linecap=round stroke-linejoin=round></path>
                                                                                    <circle id=Oval-Copy fill=#FFCFAF cx=9.025 cy=11.725 r=1></circle>
                                                                                    <circle id=Oval-Copy-2 fill=#FFCFAF cx=13.75 cy=11.725 r=1></circle>
                                                                                    <circle id=Oval-Copy-3 fill=#FFCFAF cx=18.475 cy=11.725 r=1></circle>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class=title>
                                                <h6 class=title-h6>
                                                    {{__("mindful gratitude")}}
                                                </h6>
                                            </div>
                                            <div class=read_more>
                                                <span class="link-text up-text">{{__("read more")}}</span>

                                                <svg width=7px height=6px viewBox="0 0 7 6" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                                    <title>➜</title>
                                                    <desc>Created with Sketch.</desc>
                                                    <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                                        <g id=1440---Education-Hover transform="translate(-980.000000, -616.000000)" fill=#FFD2B3>
                                                            <g id=Group-2 transform="translate(200.000000, 251.000000)">
                                                                <g id=education transform="translate(622.000000, 0.000000)">
                                                                    <g id=1 transform="translate(0.000000, 199.000000)">
                                                                        <g id=READ-MORE transform="translate(76.000000, 161.000000)">
                                                                            <path d="M88.2476028,8.18494487 L86.8164062,9.79296875 C86.7434892,9.87369832 86.6601567,9.93554666 86.5664062,9.97851562 C86.4726558,10.0214846 86.3736984,10.0429688 86.2695312,10.0429688 C86.0716136,10.0429688 85.9049486,9.97786523 85.7695312,9.84765625 C85.6341139,9.71744727 85.5664062,9.55729262 85.5664062,9.3671875 C85.5664062,9.26302031 85.5865883,9.1627609 85.6269531,9.06640625 C85.6673179,8.9700516 85.72526,8.88411496 85.8007812,8.80859375 L86.3515625,8.2578125 L82.734375,8.2578125 C82.5286448,8.2578125 82.3548184,8.1875007 82.2128906,8.046875 C82.0709628,7.9062493 82,7.7304698 82,7.51953125 C82,7.31380105 82.0709628,7.13997467 82.2128906,6.99804688 C82.3548184,6.85611908 82.5286448,6.78515625 82.734375,6.78515625 L86.3515625,6.78515625 L85.8007812,6.15234375 C85.7226559,6.06901 85.6640627,5.98242232 85.625,5.89257812 C85.5859373,5.80273393 85.5664062,5.71354211 85.5664062,5.625 C85.5664062,5.45572832 85.636067,5.30924541 85.7753906,5.18554688 C85.9147142,5.06184834 86.0794261,5 86.2695312,5 C86.3763026,5 86.4772131,5.0221352 86.5722656,5.06640625 C86.6673182,5.1106773 86.7486976,5.17187461 86.8164062,5.25 L88.2470346,6.85462372 C88.5849295,7.23361386 88.5851738,7.80566626 88.2476028,8.18494487 Z" id=➜></path>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <li class="nav_item js-link">
                                    <a class=leave-link href=/education/open-palms>
                                        <div class=shape>

                                            <svg width=190px height=190px viewBox="0 0 190 190" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                                <title>Shape</title>
                                                <desc>Created with Sketch.</desc>
                                                <defs>
                                                    <radialGradient cx=50% cy=50% fx=50% fy=50% r=50% id=radialGradient-1>
                                                        <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                                                        <stop stop-color=#FFFFFF stop-opacity=0.05 offset=100%></stop>
                                                    </radialGradient>
                                                </defs>
                                                <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                                    <g id=1440---Education transform="translate(-843.000000, -261.000000)" fill=url(#radialGradient-1)>
                                                        <g id=content transform="translate(200.000000, 261.000000)">
                                                            <g id=education transform="translate(643.000000, 0.000000)">
                                                                <g id=2>
                                                                    <path d="M106.532053,0 C166.008047,0 206.361018,75.1604491 183.519714,134.09442 C160.67841,193.028391 145.090666,189.56616 118.843295,181.348697 C92.5959232,173.131233 53.7479816,209.67088 22.8692088,174.212236 C-5.10553198,148.544575 -3.53561472,108.849154 7.10489514,67.214306 C21.7847514,9.77405228 47.0560579,6.99720774e-15 106.532053,0 Z" id=Shape></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class=feature>
                                            <div class=icon>

                                                <svg width=45px height=32px viewBox="0 0 45 32" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                                    <title>icon 2</title>
                                                    <desc>Created with Sketch.</desc>
                                                    <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                                        <g id=1440---Education transform="translate(-1116.000000, -303.000000)" stroke=#FFCFAF>
                                                            <g id=content transform="translate(200.000000, 261.000000)">
                                                                <g id=education transform="translate(643.000000, 0.000000)">
                                                                    <g id=3 transform="translate(200.000000, 0.000000)">
                                                                        <g id=CONTENT transform="translate(65.000000, 43.000000)">
                                                                            <g id=icon-2 transform="translate(9.000000, 0.000000)">
                                                                                <g id=Group-6>
                                                                                    <path d="M8.19047619,16.7307692 L8.19047619,4.77692308 C8.19047619,3.89751582 8.8780389,3.18461538 9.72619048,3.18461538 C10.5743421,3.18461538 11.2619048,3.89751582 11.2619048,4.77692308 L11.2619048,15.6692308" id=Rectangle stroke-linecap=round></path>
                                                                                    <path d="M11.2619048,15.1605229 L11.2619048,1.59230769 C11.2619048,0.712900437 11.9494675,0 12.797619,0 C13.6457706,0 14.3333333,0.712900437 14.3333333,1.59230769 L14.3333333,15.1605229" id=Rectangle stroke-linecap=round></path>
                                                                                    <path d="M14.3333333,15.6692308 L14.3333333,3.71538462 C14.3333333,2.83597736 15.020896,2.12307692 15.8690476,2.12307692 C16.7171992,2.12307692 17.4047619,2.83597736 17.4047619,3.71538462 L17.4047619,15.6692308" id=Rectangle stroke-linecap=round></path>
                                                                                    <path d="M17.4047619,15.6692308 L17.4047619,6.9 C17.4047619,6.02059274 18.0923246,5.30769231 18.9404762,5.30769231 C19.7886278,5.30769231 20.4761905,6.02059274 20.4761905,6.9 L20.4761905,17.7923077" id=Rectangle stroke-linecap=round></path>
                                                                                    <path d="M20.4853138,16.0159876 L20.4853138,22.7722863 C20.4853138,31.3034201 8.84959058,31.2756565 6.38945192,27.4587755 C5.52155171,26.291905 3.45093833,22.5979107 0.177611775,16.3767924 C-0.201195213,15.6568942 0.045380942,14.7550709 0.73220162,14.3483912 C1.47096511,13.9109551 2.39625683,14.0091292 3.0342602,14.5926412 L7.50156605,18.6783976 C8.23673141,19.1656997 8.18535214,17.387405 8.18535214,16.3767924" id=Combined-Shape></path>
                                                                                </g>
                                                                                <g id=Group-6 transform="translate(32.500000, 15.000000) scale(-1, 1) translate(-32.500000, -15.000000) translate(22.000000, 0.000000)">
                                                                                    <path d="M8.19047619,16.7307692 L8.19047619,4.77692308 C8.19047619,3.89751582 8.8780389,3.18461538 9.72619048,3.18461538 C10.5743421,3.18461538 11.2619048,3.89751582 11.2619048,4.77692308 L11.2619048,15.6692308" id=Rectangle stroke-linecap=round></path>
                                                                                    <path d="M11.2619048,15.1605229 L11.2619048,1.59230769 C11.2619048,0.712900437 11.9494675,0 12.797619,0 C13.6457706,0 14.3333333,0.712900437 14.3333333,1.59230769 L14.3333333,15.1605229" id=Rectangle stroke-linecap=round></path>
                                                                                    <path d="M14.3333333,15.6692308 L14.3333333,3.71538462 C14.3333333,2.83597736 15.020896,2.12307692 15.8690476,2.12307692 C16.7171992,2.12307692 17.4047619,2.83597736 17.4047619,3.71538462 L17.4047619,15.6692308" id=Rectangle stroke-linecap=round></path>
                                                                                    <path d="M17.4047619,15.6692308 L17.4047619,6.9 C17.4047619,6.02059274 18.0923246,5.30769231 18.9404762,5.30769231 C19.7886278,5.30769231 20.4761905,6.02059274 20.4761905,6.9 L20.4761905,17.7923077" id=Rectangle stroke-linecap=round></path>
                                                                                    <path d="M20.4853138,16.0159876 L20.4853138,22.7722863 C20.4853138,31.3034201 8.84959058,31.2756565 6.38945192,27.4587755 C5.52155171,26.291905 3.45093833,22.5979107 0.177611775,16.3767924 C-0.201195213,15.6568942 0.045380942,14.7550709 0.73220162,14.3483912 C1.47096511,13.9109551 2.39625683,14.0091292 3.0342602,14.5926412 L7.50156605,18.6783976 C8.23673141,19.1656997 8.18535214,17.387405 8.18535214,16.3767924" id=Combined-Shape></path>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class=title>
                                                <h6 class=title-h6>
                                                    {{__("open palms")}}
                                                </h6>
                                            </div>
                                            <div class=read_more>
                                                <span class="link-text up-text">{{__("read more")}}</span>

                                                <svg width=7px height=6px viewBox="0 0 7 6" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                                    <title>➜</title>
                                                    <desc>Created with Sketch.</desc>
                                                    <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                                        <g id=1440---Education-Hover transform="translate(-980.000000, -616.000000)" fill=#FFD2B3>
                                                            <g id=Group-2 transform="translate(200.000000, 251.000000)">
                                                                <g id=education transform="translate(622.000000, 0.000000)">
                                                                    <g id=1 transform="translate(0.000000, 199.000000)">
                                                                        <g id=READ-MORE transform="translate(76.000000, 161.000000)">
                                                                            <path d="M88.2476028,8.18494487 L86.8164062,9.79296875 C86.7434892,9.87369832 86.6601567,9.93554666 86.5664062,9.97851562 C86.4726558,10.0214846 86.3736984,10.0429688 86.2695312,10.0429688 C86.0716136,10.0429688 85.9049486,9.97786523 85.7695312,9.84765625 C85.6341139,9.71744727 85.5664062,9.55729262 85.5664062,9.3671875 C85.5664062,9.26302031 85.5865883,9.1627609 85.6269531,9.06640625 C85.6673179,8.9700516 85.72526,8.88411496 85.8007812,8.80859375 L86.3515625,8.2578125 L82.734375,8.2578125 C82.5286448,8.2578125 82.3548184,8.1875007 82.2128906,8.046875 C82.0709628,7.9062493 82,7.7304698 82,7.51953125 C82,7.31380105 82.0709628,7.13997467 82.2128906,6.99804688 C82.3548184,6.85611908 82.5286448,6.78515625 82.734375,6.78515625 L86.3515625,6.78515625 L85.8007812,6.15234375 C85.7226559,6.06901 85.6640627,5.98242232 85.625,5.89257812 C85.5859373,5.80273393 85.5664062,5.71354211 85.5664062,5.625 C85.5664062,5.45572832 85.636067,5.30924541 85.7753906,5.18554688 C85.9147142,5.06184834 86.0794261,5 86.2695312,5 C86.3763026,5 86.4772131,5.0221352 86.5722656,5.06640625 C86.6673182,5.1106773 86.7486976,5.17187461 86.8164062,5.25 L88.2470346,6.85462372 C88.5849295,7.23361386 88.5851738,7.80566626 88.2476028,8.18494487 Z" id=➜></path>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <li class="nav_item js-link">
                                    <a class=leave-link href=/education/healthy-breathing>
                                        <div class=shape>

                                            <svg width=190px height=190px viewBox="0 0 190 190" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                                <title>Shape</title>
                                                <desc>Created with Sketch.</desc>
                                                <defs>
                                                    <radialGradient cx=50% cy=50% fx=50% fy=50% r=50% id=radialGradient-1>
                                                        <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                                                        <stop stop-color=#FFFFFF stop-opacity=0.05 offset=100%></stop>
                                                    </radialGradient>
                                                </defs>
                                                <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                                    <g id=1440---Education transform="translate(-843.000000, -261.000000)" fill=url(#radialGradient-1)>
                                                        <g id=content transform="translate(200.000000, 261.000000)">
                                                            <g id=education transform="translate(643.000000, 0.000000)">
                                                                <g id=2>
                                                                    <path d="M106.532053,0 C166.008047,0 206.361018,75.1604491 183.519714,134.09442 C160.67841,193.028391 145.090666,189.56616 118.843295,181.348697 C92.5959232,173.131233 53.7479816,209.67088 22.8692088,174.212236 C-5.10553198,148.544575 -3.53561472,108.849154 7.10489514,67.214306 C21.7847514,9.77405228 47.0560579,6.99720774e-15 106.532053,0 Z" id=Shape></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class=feature>
                                            <div class=icon>

                                                <svg width=38px height=33px viewBox="0 0 38 33" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                                    <title>icon 3</title>
                                                    <desc>Created with Sketch.</desc>
                                                    <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                                        <g id=1440---Education transform="translate(-920.000000, -498.000000)" stroke=#FFCFAF>
                                                            <g id=content transform="translate(200.000000, 261.000000)">
                                                                <g id=education transform="translate(643.000000, 0.000000)">
                                                                    <g id=1 transform="translate(0.000000, 194.000000)">
                                                                        <g transform="translate(47.000000, 44.000000)" id=icon-3>
                                                                            <g transform="translate(31.000000, 0.000000)">
                                                                                <path d="M20.5714286,24.4239005 C20.5714286,29.7357358 26.0288667,32.004931 30.5372882,30.5857245 C36.6429436,28.6637247 36.5710744,23.4453242 35.3354774,20.0885795 L30.5372882,8.06031925 C28.0956877,0.606866567 20.5714286,2.34297785 20.5714286,8.06031925 C20.5714286,9.83791387 20.5714286,15.292441 20.5714286,24.4239005 Z" id=Path-4></path>
                                                                                <path d="M15.4285714,24.4239005 C15.4285714,29.7357358 9.97113334,32.004931 5.4627118,30.5857245 C-0.642943609,28.6637247 -0.571074429,23.4453242 0.664522628,20.0885795 L5.4627118,8.06031925 C7.90431228,0.606866567 15.4285714,2.34297785 15.4285714,8.06031925 L15.4285714,24.4239005 Z" id=Path-4></path>
                                                                                <g id=Group-3 transform="translate(4.114286, 0.000000)" stroke-linecap=round stroke-linejoin=round>
                                                                                    <path d="M13.8857143,0 L13.8857143,6.35624776" id=Path-5></path>
                                                                                    <g transform="translate(7.200000, 13.615686) scale(-1, 1) translate(-7.200000, -13.615686) translate(0.514286, 6.382353)">
                                                                                        <path d="M0,0 L8.59310256,8.68930953" id=Path-5></path>
                                                                                        <polyline id=Path-6 points="8.13708528 2.55294118 6.31168831 6.20471909 10.8180119 6.20471909"></polyline>
                                                                                        <polyline id=Path-6 transform="translate(9.161843, 11.038490) rotate(122.000000) translate(-9.161843, -11.038490) " points="8.40145426 7.76870339 7.24130455 14.3082763 11.0823809 11.4204813"></polyline>
                                                                                    </g>
                                                                                    <g id=Group-4 transform="translate(13.885714, 6.382353)">
                                                                                        <polyline id=Path-6 points="8.13708528 2.55294118 6.31168831 6.20471909 10.8180119 6.20471909"></polyline>
                                                                                        <path d="M0,0 L8.59310256,8.68930953" id=Path-5></path>
                                                                                        <polyline id=Path-6 transform="translate(9.161843, 11.038490) rotate(122.000000) translate(-9.161843, -11.038490) " points="8.40145426 7.76870339 7.24130455 14.3082763 11.0823809 11.4204813"></polyline>
                                                                                    </g>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class=title>
                                                <h6 class=title-h6>
                                                    {{__("healthy breathing")}}
                                                </h6>
                                            </div>
                                            <div class=read_more>
                                                <span class="link-text up-text">{{__("read more")}}</span>

                                                <svg width=7px height=6px viewBox="0 0 7 6" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                                    <title>➜</title>
                                                    <desc>Created with Sketch.</desc>
                                                    <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                                        <g id=1440---Education-Hover transform="translate(-980.000000, -616.000000)" fill=#FFD2B3>
                                                            <g id=Group-2 transform="translate(200.000000, 251.000000)">
                                                                <g id=education transform="translate(622.000000, 0.000000)">
                                                                    <g id=1 transform="translate(0.000000, 199.000000)">
                                                                        <g id=READ-MORE transform="translate(76.000000, 161.000000)">
                                                                            <path d="M88.2476028,8.18494487 L86.8164062,9.79296875 C86.7434892,9.87369832 86.6601567,9.93554666 86.5664062,9.97851562 C86.4726558,10.0214846 86.3736984,10.0429688 86.2695312,10.0429688 C86.0716136,10.0429688 85.9049486,9.97786523 85.7695312,9.84765625 C85.6341139,9.71744727 85.5664062,9.55729262 85.5664062,9.3671875 C85.5664062,9.26302031 85.5865883,9.1627609 85.6269531,9.06640625 C85.6673179,8.9700516 85.72526,8.88411496 85.8007812,8.80859375 L86.3515625,8.2578125 L82.734375,8.2578125 C82.5286448,8.2578125 82.3548184,8.1875007 82.2128906,8.046875 C82.0709628,7.9062493 82,7.7304698 82,7.51953125 C82,7.31380105 82.0709628,7.13997467 82.2128906,6.99804688 C82.3548184,6.85611908 82.5286448,6.78515625 82.734375,6.78515625 L86.3515625,6.78515625 L85.8007812,6.15234375 C85.7226559,6.06901 85.6640627,5.98242232 85.625,5.89257812 C85.5859373,5.80273393 85.5664062,5.71354211 85.5664062,5.625 C85.5664062,5.45572832 85.636067,5.30924541 85.7753906,5.18554688 C85.9147142,5.06184834 86.0794261,5 86.2695312,5 C86.3763026,5 86.4772131,5.0221352 86.5722656,5.06640625 C86.6673182,5.1106773 86.7486976,5.17187461 86.8164062,5.25 L88.2470346,6.85462372 C88.5849295,7.23361386 88.5851738,7.80566626 88.2476028,8.18494487 Z" id=➜></path>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <li class="nav_item js-link">
                                    <a class=leave-link href=/education/heroic-virtues>
                                        <div class=shape>

                                            <svg width=190px height=190px viewBox="0 0 190 190" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                                <title>Shape</title>
                                                <desc>Created with Sketch.</desc>
                                                <defs>
                                                    <radialGradient cx=50% cy=50% fx=50% fy=50% r=50% id=radialGradient-1>
                                                        <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                                                        <stop stop-color=#FFFFFF stop-opacity=0.05 offset=100%></stop>
                                                    </radialGradient>
                                                </defs>
                                                <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                                    <g id=1440---Education transform="translate(-843.000000, -261.000000)" fill=url(#radialGradient-1)>
                                                        <g id=content transform="translate(200.000000, 261.000000)">
                                                            <g id=education transform="translate(643.000000, 0.000000)">
                                                                <g id=2>
                                                                    <path d="M106.532053,0 C166.008047,0 206.361018,75.1604491 183.519714,134.09442 C160.67841,193.028391 145.090666,189.56616 118.843295,181.348697 C92.5959232,173.131233 53.7479816,209.67088 22.8692088,174.212236 C-5.10553198,148.544575 -3.53561472,108.849154 7.10489514,67.214306 C21.7847514,9.77405228 47.0560579,6.99720774e-15 106.532053,0 Z" id=Shape></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class=feature>
                                            <div class=icon>

                                                <svg width=30px height=38px viewBox="0 0 30 38" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                                    <title>icon</title>
                                                    <desc>Created with Sketch.</desc>
                                                    <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                                        <g id=1440---Education transform="translate(-1129.000000, -495.000000)">
                                                            <g id=content transform="translate(200.000000, 261.000000)">
                                                                <g id=education transform="translate(643.000000, 0.000000)">
                                                                    <g id=4 transform="translate(207.000000, 194.000000)">
                                                                        <g id=content transform="translate(61.000000, 41.000000)">
                                                                            <g id=icon transform="translate(19.000000, 0.000000)">
                                                                                <g id=Group-8 stroke=#FFCFAF>
                                                                                    <g id=Group transform="translate(5.000000, 19.000000)" stroke-linecap=round>
                                                                                        <path d="M5.18181818,11 C7.14078036,12.1497692 10.706543,12.2652255 12.7052557,11" id=Path-3></path>
                                                                                    </g>
                                                                                    <path d="M14,36 C21.7319865,36 28,26.0360488 28,16.7727273 C28,7.50940579 24.3709214,0 14,0 C3.62907858,1.63294916e-15 0,7.50940579 0,16.7727273 C0,26.0360488 6.2680135,36 14,36 Z" id=Oval-3></path>
                                                                                </g>
                                                                                <polygon id=Path-2 fill=#FFCFAF points="12.1743437 7 10 13.812019 13.2613784 13.812019 11.8749916 19 18.6913617 10.9324984 14.4281627 10.9324984 15.697693 7"></polygon>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class=title>
                                                <h6 class=title-h6>
                                                    {{__("heroic virtues")}}
                                                </h6>
                                            </div>
                                            <div class=read_more>
                                                <span class="link-text up-text">{{__("read more")}}</span>

                                                <svg width=7px height=6px viewBox="0 0 7 6" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                                    <title>➜</title>
                                                    <desc>Created with Sketch.</desc>
                                                    <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                                        <g id=1440---Education-Hover transform="translate(-980.000000, -616.000000)" fill=#FFD2B3>
                                                            <g id=Group-2 transform="translate(200.000000, 251.000000)">
                                                                <g id=education transform="translate(622.000000, 0.000000)">
                                                                    <g id=1 transform="translate(0.000000, 199.000000)">
                                                                        <g id=READ-MORE transform="translate(76.000000, 161.000000)">
                                                                            <path d="M88.2476028,8.18494487 L86.8164062,9.79296875 C86.7434892,9.87369832 86.6601567,9.93554666 86.5664062,9.97851562 C86.4726558,10.0214846 86.3736984,10.0429688 86.2695312,10.0429688 C86.0716136,10.0429688 85.9049486,9.97786523 85.7695312,9.84765625 C85.6341139,9.71744727 85.5664062,9.55729262 85.5664062,9.3671875 C85.5664062,9.26302031 85.5865883,9.1627609 85.6269531,9.06640625 C85.6673179,8.9700516 85.72526,8.88411496 85.8007812,8.80859375 L86.3515625,8.2578125 L82.734375,8.2578125 C82.5286448,8.2578125 82.3548184,8.1875007 82.2128906,8.046875 C82.0709628,7.9062493 82,7.7304698 82,7.51953125 C82,7.31380105 82.0709628,7.13997467 82.2128906,6.99804688 C82.3548184,6.85611908 82.5286448,6.78515625 82.734375,6.78515625 L86.3515625,6.78515625 L85.8007812,6.15234375 C85.7226559,6.06901 85.6640627,5.98242232 85.625,5.89257812 C85.5859373,5.80273393 85.5664062,5.71354211 85.5664062,5.625 C85.5664062,5.45572832 85.636067,5.30924541 85.7753906,5.18554688 C85.9147142,5.06184834 86.0794261,5 86.2695312,5 C86.3763026,5 86.4772131,5.0221352 86.5722656,5.06640625 C86.6673182,5.1106773 86.7486976,5.17187461 86.8164062,5.25 L88.2470346,6.85462372 C88.5849295,7.23361386 88.5851738,7.80566626 88.2476028,8.18494487 Z" id=➜></path>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection