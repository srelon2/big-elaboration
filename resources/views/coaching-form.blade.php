@extends('layouts.app')

@section('content')

    <main id=main data-page-id=coachingForm>
        <div style="background-image:linear-gradient(134deg,#7c55f2 0,#af89fa 51%,#fab6b6 100%)" data-bg='{ "deg": 135, "colors":[{ "c": "#7C55F2", "p": 0 }, { "c": "#A173FF", "p": 0.51 }, { "c": "#CFA5FA", "p": 1 }] }' class=bg>
            <div id=waves class=waves_wrap>
                
                <svg id=wave-1 width=1920px height=371px viewBox="0 0 1920 371" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                    <title>from_1 </title>
                    <desc>Created with Sketch.</desc>
                    <defs>
                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=linearGradient-1>
                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                        </linearGradient>
                    </defs>
                    <g id=1920 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                        <g id=WAVES transform="translate(0.000000, -37.000000)" fill=url(#linearGradient-1) fill-rule=nonzero>
                            <g id=bg transform="translate(-3.000000, -2.000000)">
                                <g id=Cloud_1 transform="translate(3.000000, 0.000000)">
                                    <path d="M1920,408.651004 C643.117748,410.239444 3.11774828,410.239444 0,408.651004 L0,279.469546 C740.574468,345.677945 843.188992,-128.789792 1920,35.669798 C1920,44.1946085 1920,168.521677 1920,408.651004 Z" id=from_1- transform="translate(960.000000, 205.325502) scale(1, -1) translate(-960.000000, -205.325502) "></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>

                <svg id=wave-2 width=1920px height=387px viewBox="0 0 1920 387" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=linearGradient-1>
                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                        </linearGradient>
                    </defs>
                    <g id=1920 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                        <g id=WAVES transform="translate(0.000000, -39.000000)" fill=url(#linearGradient-1) fill-rule=nonzero>
                            <g id=bg transform="translate(0.000000, -2.000000)">
                                <g id=Cloud_2>
                                    <path d="M1920,425.977003 C643.117748,427.565442 3.11774828,427.565442 -2.84217094e-14,425.977003 L-4.54747351e-13,286.99596 C730.197368,344.977003 1195.61921,-102.022997 1920,22.9770026 C1920,31.5018132 1920,165.835146 1920,425.977003 Z" id=from_1- transform="translate(960.000000, 213.988501) scale(1, -1) translate(-960.000000, -213.988501) "></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>
        </div>
        <section class="beta-section active">
            <div class=container>
                <div class=content__block>
                    <div class=back_wrap>
                        <div class=back>
                            <a href={{route('coaching')}} class="link-text up-text js-link leave-link">{{__("coaching")}}</a>
                            
                            <svg width=7px height=6px viewBox="0 0 7 6" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                <title>➜</title>
                                <desc>{{__("Created with Sketch.")}}</desc>
                                <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                    <g id=1440---Education-Hover transform="translate(-980.000000, -616.000000)" fill=#FFD2B3>
                                        <g id=Group-2 transform="translate(200.000000, 251.000000)">
                                            <g id=education transform="translate(622.000000, 0.000000)">
                                                <g id=1 transform="translate(0.000000, 199.000000)">
                                                    <g id=READ-MORE transform="translate(76.000000, 161.000000)">
                                                        <path d="M88.2476028,8.18494487 L86.8164062,9.79296875 C86.7434892,9.87369832 86.6601567,9.93554666 86.5664062,9.97851562 C86.4726558,10.0214846 86.3736984,10.0429688 86.2695312,10.0429688 C86.0716136,10.0429688 85.9049486,9.97786523 85.7695312,9.84765625 C85.6341139,9.71744727 85.5664062,9.55729262 85.5664062,9.3671875 C85.5664062,9.26302031 85.5865883,9.1627609 85.6269531,9.06640625 C85.6673179,8.9700516 85.72526,8.88411496 85.8007812,8.80859375 L86.3515625,8.2578125 L82.734375,8.2578125 C82.5286448,8.2578125 82.3548184,8.1875007 82.2128906,8.046875 C82.0709628,7.9062493 82,7.7304698 82,7.51953125 C82,7.31380105 82.0709628,7.13997467 82.2128906,6.99804688 C82.3548184,6.85611908 82.5286448,6.78515625 82.734375,6.78515625 L86.3515625,6.78515625 L85.8007812,6.15234375 C85.7226559,6.06901 85.6640627,5.98242232 85.625,5.89257812 C85.5859373,5.80273393 85.5664062,5.71354211 85.5664062,5.625 C85.5664062,5.45572832 85.636067,5.30924541 85.7753906,5.18554688 C85.9147142,5.06184834 86.0794261,5 86.2695312,5 C86.3763026,5 86.4772131,5.0221352 86.5722656,5.06640625 C86.6673182,5.1106773 86.7486976,5.17187461 86.8164062,5.25 L88.2470346,6.85462372 C88.5849295,7.23361386 88.5851738,7.80566626 88.2476028,8.18494487 Z" id=➜></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div class=text__block>
                        <h2 class="title title-h2">{{__("Join beta")}}</h2>
                        <p class="desc desc-1">{{__("In order to give everyone a great experience we will be slowly sending out invites to the Big Elaboration beta. Please let us know a bit about how you’d like to use Big Elaboration and we’ll send you an invite as soon as we’re ready for you!")}}</p>
                        <div class=form__wrap>
                            <form action="" method=post class="form--beta custom-form" novalidate>
                                <div class=input__wrap>
                                    <label for=demo.firstName class=input-label>{{__("First name")}}</label>
                                    <input type=text id=demo.firstName name=FNAME data-validation-name=demo.firstName class="js-link required" required>
                                    <div class=error-tooltip__wrap>
                                        <span class=error-tooltip__content>{{__("You can use only English letters, space, comma, apostrophe, dash, and period (,.'-)")}}</span>
                                    </div>
                                </div>
                                <div class=input__wrap>
                                    <label for=demo.lastName class=input-label>{{__("Last name")}}</label>
                                    <input type=text id=demo.lastName name=LNAME data-validation-name=demo.lastName class="js-link required" required>
                                    <div class=error-tooltip__wrap>
                                        <span class=error-tooltip__content>{{__("You can use only English letters, space, comma, apostrophe, dash, and period (,.'-)")}}</span>
                                    </div>
                                </div>
                                <div class=input__wrap>
                                    <label for=demo.email class=input-label>{{__("Email")}}</label>
                                    <input type=email id=demo.email name=EMAIL data-validation-name=demo.email class="js-link required" required>
                                    <div class=error-tooltip__wrap>
                                        <span class=error-tooltip__content>{{__("Please, check that you include an '@' and '.' in the email address")}}</span>
                                    </div>
                                </div>
                                <div class=input__wrap>
                                    <label for=demo.jobTitle class=input-label>{{__("Job title")}}</label>
                                    <input type=text id=demo.jobTitle name=JOB data-validation-name=demo.jobTitle class="js-link required" required>
                                    <div class=error-tooltip__wrap>
                                        <span class=error-tooltip__content>{{__("Please, fill out this field")}}</span>
                                    </div>
                                </div>
                                <div class=input__wrap>
                                    <label for=demo.orgName class=input-label>{{__("Organization name")}}</label>
                                    <input type=text id=demo.orgName name=ORGNAME data-validation-name=demo.orgName class="js-link required" required>
                                    <div class=error-tooltip__wrap>
                                        <span class=error-tooltip__content>{{__("Please, fill out this field")}}</span>
                                    </div>
                                </div>
                                <div class=input__wrap>
                                    <label for=demo.orgSite class=input-label>{{__("Organization website")}}</label>
                                    <input type=text id=demo.orgSite name=ORGWEBSITE data-validation-name=demo.orgSite class="js-link required" required>
                                    <div class=error-tooltip__wrap>
                                        <span class=error-tooltip__content>{{__("Please, fill out this field")}}</span>
                                    </div>
                                </div>

                                <div class=input__wrap>
                                    <label for=demo.orgType class=input-label>{{__("Organization type")}}</label>
                                    <select name=ORGTYPE id=demo.orgType data-validation-name=demo.orgType class="js-link required">
                                        <option value="Individual Coach">{{__("Individual Coach")}}</option>
                                        <option value="Multi-Partner Practice">{{__("Multi-Partner Practice")}}</option>
                                        <option value="HR Consulting">{{__("HR Consulting")}}</option>
                                        <option value=Other>{{__("Other")}}</option>
                                    </select>
                                </div>

                                <div class="textarea__wrap input__wrap">
                                    <label for=demo.detailsUse class=input-label>{{__("Tell us more about how you’d like to use Big Elaboration:")}}</label>
                                    <textarea name=TELLMORE id=demo.detailsUse data-validation-name=demo.detailsUse class="js-link required" required></textarea>
                                    <div class=error-tooltip__wrap>
                                        <span class=error-tooltip__content>{{__("Please, fill out this field")}}</span>
                                    </div>
                                </div>

                                <div style="position:absolute;left:-5000px" aria-hidden=true><input type=text name=b_f0240fe9ce9d96c3853c2e892_8b029db22b tabindex=-1></div>
                                <div class="checkbox__wrap input__wrap">
                                    <input type=checkbox name=CHECKAGREE data-validation-name=demo.checkAgree id=check-agree class=required required onclick=this.blur()>
                                    <label for=check-agree>{{__("I have read and agree to the")}}

                                        <a href=/privacy target=_blank>{{__("Privacy Policy")}}</a></label>
                                </div>
                                <button type=submit class="button js-link">{{__("Requset invite")}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
@endsection