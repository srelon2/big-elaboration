@extends('layouts.app')

@section('content')
    <main id=main data-page-id=blog>
        <div style="background-image:linear-gradient(134deg,#7c55f2 0,#af89fa 51%,#fab6b6 100%)" data-bg='{ "deg": 134, "colors":[{ "c": "#7C55F2", "p": 0 }, { "c": "#AF89FA", "p": 0.51 }, { "c": "#FFDABF", "p": 1 }] }' class=bg>
            <div id=waves class=waves_wrap>
                
                <svg id=wave-1 width=1920px height=371px viewBox="0 0 1920 371" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                    <title>from_1 </title>
                    <desc>Created with Sketch.</desc>
                    <defs>
                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=linearGradient-1>
                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                        </linearGradient>
                    </defs>
                    <g id=1920 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                        <g id=WAVES transform="translate(0.000000, -37.000000)" fill=url(#linearGradient-1) fill-rule=nonzero>
                            <g id=bg transform="translate(-3.000000, -2.000000)">
                                <g id=Cloud_1 transform="translate(3.000000, 0.000000)">
                                    <path d="M1920,408.651004 C643.117748,410.239444 3.11774828,410.239444 0,408.651004 L0,279.469546 C740.574468,345.677945 843.188992,-128.789792 1920,35.669798 C1920,44.1946085 1920,168.521677 1920,408.651004 Z" id=from_1- transform="translate(960.000000, 205.325502) scale(1, -1) translate(-960.000000, -205.325502) "></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>

                <svg id=wave-2 width=1920px height=387px viewBox="0 0 1920 387" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=linearGradient-1>
                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                        </linearGradient>
                    </defs>
                    <g id=1920 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                        <g id=WAVES transform="translate(0.000000, -39.000000)" fill=url(#linearGradient-1) fill-rule=nonzero>
                            <g id=bg transform="translate(0.000000, -2.000000)">
                                <g id=Cloud_2>
                                    <path d="M1920,425.977003 C643.117748,427.565442 3.11774828,427.565442 -2.84217094e-14,425.977003 L-4.54747351e-13,286.99596 C730.197368,344.977003 1195.61921,-102.022997 1920,22.9770026 C1920,31.5018132 1920,165.835146 1920,425.977003 Z" id=from_1- transform="translate(960.000000, 213.988501) scale(1, -1) translate(-960.000000, -213.988501) "></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>
        </div>
        <section class="blog-section active">
            <div class=container>
                <div class=content__block--left>
                    <h1 class="title title-h1">{{__("Big Elaboration blog")}}</h1>
                    <div class="categories__wrap s-hidden">
                        <ul id=categories__list class=categories__list></ul>
                    </div>
                    <div class="categories-show__wrap s-visible">
                        <a href=# id=filter-show class="link-text js-link up-text">{{__("Show filters")}}</a>
                    </div>
                    <div id=modal-categories>
                        <div class=modal-bg id=modal-bg>
                            <div class=waves_wrap>

                                <svg width=768px height=249px viewBox="0 0 768 249" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <defs>
                                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=waves-linearGradient-1>
                                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                                        </linearGradient>
                                        <path d="M768.226952,307.512465 L1.68403639,307.512465 L0.974335439,178.580503 C225.742085,49.0218441 481.492957,6.11682642 768.226952,49.8654497 C769.257683,54.6763271 769.257683,140.558666 768.226952,307.512465 Z" id=path-2></path>
                                    </defs>
                                    <g id=768 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                        <g id=768---menu-copy transform="translate(0.000000, -716.000000)">
                                            <g id=bg transform="translate(-1.000000, 0.000000)">
                                                <g id=Clouds transform="translate(385.000000, 870.000000) rotate(-180.000000) translate(-385.000000, -870.000000) translate(0.000000, 716.000000)">
                                                    <mask id=mask-3 fill=white>
                                                        <use xlink:href=#path-2></use>
                                                    </mask>
                                                    <use id=Shape-1 fill=url(#waves-linearGradient-1) fill-rule=nonzero transform="translate(384.987168, 170.406233) scale(1, -1) translate(-384.987168, -170.406233) " xlink:href=#path-2></use>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>

                                <svg width=768px height=270px viewBox="0 0 768 270" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <defs>
                                        <linearGradient x1=50% y1=89.1572565% x2=50% y2=21.9809334% id=waves-linearGradient-2>
                                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                                            <stop stop-color=#FFFFFF stop-opacity=0.1 offset=100%></stop>
                                        </linearGradient>
                                    </defs>
                                    <g id=768 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                        <g id=768---menu-copy transform="translate(0.000000, -721.000000)" fill=url(#waves-linearGradient-2) fill-rule=nonzero>
                                            <g id=bg transform="translate(-1.000000, 0.000000)">
                                                <g id=Clouds transform="translate(385.000000, 870.000000) rotate(-180.000000) translate(-385.000000, -870.000000) translate(0.000000, 716.000000)">
                                                    <path d="M768.226952,303.03 L0.615474886,303.03 L0.353600869,107.351444 C74.548422,66.4856818 130.763888,37.9855663 169,21.8510977 C332.327261,-47.0680067 519.373575,66.3624219 768.226952,96.6194559 C769.257683,101.41654 769.257683,170.220055 768.226952,303.03 Z" id=Shape-2 transform="translate(384.676800, 151.515000) scale(1, -1) translate(-384.676800, -151.515000) "></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div id=filter-close class=close-btn-wrap>
                            <a href=# class="burger-menu__link menu-link up-text">
                                <span class="burger-menu__text link-text up-text">{{__("close")}}</span>
                                <div class=burger-cross></div>
                            </a>
                        </div>
                        <div class=categories__wrap>
                            <div class="title title-h1"> {{__("Filters")}}</div>
                            <ul class=categories__list></ul>
                        </div>
                    </div>
                </div>
                <div class=content__block--right>
                    <div class=posts-list__wrap>
                        <span class="error-message title-h3">{{__("Couldn’t get recents posts!")}}</span>
                        <div class=column>
                        </div>
                    </div>
                    <div class="icon__wrap bodymovin__wrap" data-bodymovin-path=assets/bodymovin/preloader.json></div>
                </div>
            </div>
        </section>

    </main>
@endsection