@extends('layouts.app')

@section('content')

    <main id=main data-page-id=case>
        <div style="background-image:linear-gradient(134deg,#7c55f2 0,#af89fa 51%,#fab6b6 100%)" data-bg='{ "deg": 135, "colors":[{ "c": "#7C55F2", "p": 0 }, { "c": "#B479F2", "p": 0.51 }, { "c": "#FABBEA", "p": 1 }] }' class=bg>
            <div id=waves class=waves_wrap>
                
                <svg id=wave-1 width=1920px height=371px viewBox="0 0 1920 371" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                    <title>from_1 </title>
                    <desc>Created with Sketch.</desc>
                    <defs>
                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=linearGradient-1>
                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                        </linearGradient>
                    </defs>
                    <g id=1920 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                        <g id=WAVES transform="translate(0.000000, -37.000000)" fill=url(#linearGradient-1) fill-rule=nonzero>
                            <g id=bg transform="translate(-3.000000, -2.000000)">
                                <g id=Cloud_1 transform="translate(3.000000, 0.000000)">
                                    <path d="M1920,408.651004 C643.117748,410.239444 3.11774828,410.239444 0,408.651004 L0,279.469546 C740.574468,345.677945 843.188992,-128.789792 1920,35.669798 C1920,44.1946085 1920,168.521677 1920,408.651004 Z" id=from_1- transform="translate(960.000000, 205.325502) scale(1, -1) translate(-960.000000, -205.325502) "></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>

                <svg id=wave-2 width=1920px height=387px viewBox="0 0 1920 387" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=linearGradient-1>
                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                        </linearGradient>
                    </defs>
                    <g id=1920 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                        <g id=WAVES transform="translate(0.000000, -39.000000)" fill=url(#linearGradient-1) fill-rule=nonzero>
                            <g id=bg transform="translate(0.000000, -2.000000)">
                                <g id=Cloud_2>
                                    <path d="M1920,425.977003 C643.117748,427.565442 3.11774828,427.565442 -2.84217094e-14,425.977003 L-4.54747351e-13,286.99596 C730.197368,344.977003 1195.61921,-102.022997 1920,22.9770026 C1920,31.5018132 1920,165.835146 1920,425.977003 Z" id=from_1- transform="translate(960.000000, 213.988501) scale(1, -1) translate(-960.000000, -213.988501) "></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>
        </div>
        <section class="tabs-view-section active">
            <div class=container>
                <div class=tabs_content_container>
                    <div class=back_wrap>
                        <div class=back>
                            <a href=/education class="link-text up-text js-link leave-link">{{__("education")}}</a>
                            
                            <svg width=7px height=6px viewBox="0 0 7 6" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                <title>➜</title>
                                <desc>Created with Sketch.</desc>
                                <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                    <g id=1440---Education-Hover transform="translate(-980.000000, -616.000000)" fill=#FFD2B3>
                                        <g id=Group-2 transform="translate(200.000000, 251.000000)">
                                            <g id=education transform="translate(622.000000, 0.000000)">
                                                <g id=1 transform="translate(0.000000, 199.000000)">
                                                    <g id=READ-MORE transform="translate(76.000000, 161.000000)">
                                                        <path d="M88.2476028,8.18494487 L86.8164062,9.79296875 C86.7434892,9.87369832 86.6601567,9.93554666 86.5664062,9.97851562 C86.4726558,10.0214846 86.3736984,10.0429688 86.2695312,10.0429688 C86.0716136,10.0429688 85.9049486,9.97786523 85.7695312,9.84765625 C85.6341139,9.71744727 85.5664062,9.55729262 85.5664062,9.3671875 C85.5664062,9.26302031 85.5865883,9.1627609 85.6269531,9.06640625 C85.6673179,8.9700516 85.72526,8.88411496 85.8007812,8.80859375 L86.3515625,8.2578125 L82.734375,8.2578125 C82.5286448,8.2578125 82.3548184,8.1875007 82.2128906,8.046875 C82.0709628,7.9062493 82,7.7304698 82,7.51953125 C82,7.31380105 82.0709628,7.13997467 82.2128906,6.99804688 C82.3548184,6.85611908 82.5286448,6.78515625 82.734375,6.78515625 L86.3515625,6.78515625 L85.8007812,6.15234375 C85.7226559,6.06901 85.6640627,5.98242232 85.625,5.89257812 C85.5859373,5.80273393 85.5664062,5.71354211 85.5664062,5.625 C85.5664062,5.45572832 85.636067,5.30924541 85.7753906,5.18554688 C85.9147142,5.06184834 86.0794261,5 86.2695312,5 C86.3763026,5 86.4772131,5.0221352 86.5722656,5.06640625 C86.6673182,5.1106773 86.7486976,5.17187461 86.8164062,5.25 L88.2470346,6.85462372 C88.5849295,7.23361386 88.5851738,7.80566626 88.2476028,8.18494487 Z" id=➜></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </div>
                    </div>
                    <div class=content>
                        <div class=list_wrap>
                            <ul class=anchor_list>

                                <li data-nav-target=1 class="anchor_item link-text js-link up-text js-active">{{__("overview")}}</li>

                                <li data-nav-target=2 class="anchor_item link-text js-link up-text">{{__("materials")}}</li>

                                <li data-nav-target=3 class="anchor_item link-text js-link up-text">{{__("process")}}</li>

                                <li data-nav-target=4 class="anchor_item link-text js-link up-text">{{__("how it works")}}</li>

                                <li data-nav-target=5 class="anchor_item link-text js-link up-text">{{__("reflect with maslo")}}</li>

                            </ul>
                        </div>
                        <div class=text_wrap>
                            <div class=title_wrap>
                                <h2 class="title-h2 low-text">{{__("Open palms")}}</h2>
                            </div>

                            <div data-nav-id=1 class="tab_content active">
                                <div class=subtitle_wrap>
                                    <h4 class="title-h4 low-text">{{__("overview")}}</h4>
                                </div>
                                <div class=decs_wrap>

                                    <p class="description desc-1">{{__("Maslo activities help students look inward and get to know themselves on a deeper level. They promote creativity and introspection, enhance problem-solving abilities, and encourage students to draw on their emotions as needed.")}}</p>

                                    <p class="description desc-1">{{__("The goal of this activity is for students to become more receptive to creativity and innovation. Students will learn that sometimes the most unlikely ideas make for the best solutions.")}}</p>

                                </div>
                            </div>

                            <div data-nav-id=2 class=tab_content>
                                <div class=subtitle_wrap>
                                    <h4 class="title-h4 low-text">{{__("materials")}}</h4>
                                </div>
                                <div class=decs_wrap>

                                    <p class="description desc-1">{{__("The Maslo app plays a key role in this activity. Maslo is an AI that features personified voice journaling and allows students to express themselves on an emotional level. The app can be used on any Apple device, including an iPod, iPhone, or iPad. Students will ideally work in groups of three or four to complete the activity, with each student using their own device and Maslo AI.")}}</p>

                                </div>
                            </div>

                            <div data-nav-id=3 class=tab_content>
                                <div class=subtitle_wrap>
                                    <h4 class="title-h4 low-text">{{__("process")}}</h4>
                                </div>
                                <div class=decs_wrap>

                                    <p class="description desc-1">{{__("To start the activity, students will sit down with their palms facing the ceiling. In just 15 seconds, they will start to experience a shift in their mindset and feel more  open to the world around them.")}}</p>

                                    <p class="description desc-1">{{__("Keeping their palms up, students will then start asking a peer questions about a recent problem they faced. They will switch roles after a minute goes by, and should take note of how they feel throughout the process.")}}</p>

                                </div>
                            </div>

                            <div data-nav-id=4 class=tab_content>
                                <div class=subtitle_wrap>
                                    <h4 class="title-h4 low-text">{{__("how it works")}}</h4>
                                </div>
                                <div class=decs_wrap>

                                    <p class="description desc-1">{{__("Facing the palms upward stimulates a powerful physical and emotional response. Specifically, this position helps us adopt a mindset of consideration, where we become more receptive to new information, opportunities, and solutions.")}}</p>

                                    <p class="description desc-1">{{__("Conversely, with the palms down we tend to adopt a mindset of resistance, where we close ourselves off to new information. With upturned palms, however, we become more likely to help others and more willing to ask for help when we need it. For centuries upon centuries, we have associated upward-facing palms with openness and receptiveness. This activity helps remind us of that.")}}</p>

                                </div>
                            </div>

                            <div data-nav-id=5 class=tab_content>
                                <div class=subtitle_wrap>
                                    <h4 class="title-h4 low-text">{{__("reflect with maslo")}}</h4>
                                </div>
                                <div class=decs_wrap>

                                    <p class="description desc-1">{{__("After completing the activity, students will open the Maslo app and journal about their experiences. Maslo will ask students how the activity made them feel and whether the concept made sense to them.")}}</p>

                                    <p class="description desc-1">{{__("Students will then be asked to share a real-life example where this practice could help them navigate a challenge or keep a positive mindset.")}}</p>

                                </div>
                            </div>

                            <div class=buttons_wrap>
                                <a href=/education/mindful class="button js-link prev leave-link">{{__("previous activity")}}</a>
                                <a href=/education/healthy-breathing class="button js-link next leave-link">{{__("next activity")}}</a>
                            </div>
                            <div class=mobile_buttons_wrap>
                                <div class="btn-wrap prev">
                                    
                                    <svg width=7px height=6px viewBox="0 0 7 6" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                        <title>➜</title>
                                        <desc>Created with Sketch.</desc>
                                        <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                            <g id=1440---Education-Hover transform="translate(-980.000000, -616.000000)" fill=#FFD2B3>
                                                <g id=Group-2 transform="translate(200.000000, 251.000000)">
                                                    <g id=education transform="translate(622.000000, 0.000000)">
                                                        <g id=1 transform="translate(0.000000, 199.000000)">
                                                            <g id=READ-MORE transform="translate(76.000000, 161.000000)">
                                                                <path d="M88.2476028,8.18494487 L86.8164062,9.79296875 C86.7434892,9.87369832 86.6601567,9.93554666 86.5664062,9.97851562 C86.4726558,10.0214846 86.3736984,10.0429688 86.2695312,10.0429688 C86.0716136,10.0429688 85.9049486,9.97786523 85.7695312,9.84765625 C85.6341139,9.71744727 85.5664062,9.55729262 85.5664062,9.3671875 C85.5664062,9.26302031 85.5865883,9.1627609 85.6269531,9.06640625 C85.6673179,8.9700516 85.72526,8.88411496 85.8007812,8.80859375 L86.3515625,8.2578125 L82.734375,8.2578125 C82.5286448,8.2578125 82.3548184,8.1875007 82.2128906,8.046875 C82.0709628,7.9062493 82,7.7304698 82,7.51953125 C82,7.31380105 82.0709628,7.13997467 82.2128906,6.99804688 C82.3548184,6.85611908 82.5286448,6.78515625 82.734375,6.78515625 L86.3515625,6.78515625 L85.8007812,6.15234375 C85.7226559,6.06901 85.6640627,5.98242232 85.625,5.89257812 C85.5859373,5.80273393 85.5664062,5.71354211 85.5664062,5.625 C85.5664062,5.45572832 85.636067,5.30924541 85.7753906,5.18554688 C85.9147142,5.06184834 86.0794261,5 86.2695312,5 C86.3763026,5 86.4772131,5.0221352 86.5722656,5.06640625 C86.6673182,5.1106773 86.7486976,5.17187461 86.8164062,5.25 L88.2470346,6.85462372 C88.5849295,7.23361386 88.5851738,7.80566626 88.2476028,8.18494487 Z" id=➜></path>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    <a href=/education/mindful class=button>{{__("previous")}}</a>
                                </div>
                                <div class="btn-wrap next">
                                    <a href=/education/healthy-breathing class="button next">{{__("next")}}</a>
                                    
                                    <svg width=7px height=6px viewBox="0 0 7 6" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                        <title>➜</title>
                                        <desc>Created with Sketch.</desc>
                                        <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                            <g id=1440---Education-Hover transform="translate(-980.000000, -616.000000)" fill=#FFD2B3>
                                                <g id=Group-2 transform="translate(200.000000, 251.000000)">
                                                    <g id=education transform="translate(622.000000, 0.000000)">
                                                        <g id=1 transform="translate(0.000000, 199.000000)">
                                                            <g id=READ-MORE transform="translate(76.000000, 161.000000)">
                                                                <path d="M88.2476028,8.18494487 L86.8164062,9.79296875 C86.7434892,9.87369832 86.6601567,9.93554666 86.5664062,9.97851562 C86.4726558,10.0214846 86.3736984,10.0429688 86.2695312,10.0429688 C86.0716136,10.0429688 85.9049486,9.97786523 85.7695312,9.84765625 C85.6341139,9.71744727 85.5664062,9.55729262 85.5664062,9.3671875 C85.5664062,9.26302031 85.5865883,9.1627609 85.6269531,9.06640625 C85.6673179,8.9700516 85.72526,8.88411496 85.8007812,8.80859375 L86.3515625,8.2578125 L82.734375,8.2578125 C82.5286448,8.2578125 82.3548184,8.1875007 82.2128906,8.046875 C82.0709628,7.9062493 82,7.7304698 82,7.51953125 C82,7.31380105 82.0709628,7.13997467 82.2128906,6.99804688 C82.3548184,6.85611908 82.5286448,6.78515625 82.734375,6.78515625 L86.3515625,6.78515625 L85.8007812,6.15234375 C85.7226559,6.06901 85.6640627,5.98242232 85.625,5.89257812 C85.5859373,5.80273393 85.5664062,5.71354211 85.5664062,5.625 C85.5664062,5.45572832 85.636067,5.30924541 85.7753906,5.18554688 C85.9147142,5.06184834 86.0794261,5 86.2695312,5 C86.3763026,5 86.4772131,5.0221352 86.5722656,5.06640625 C86.6673182,5.1106773 86.7486976,5.17187461 86.8164062,5.25 L88.2470346,6.85462372 C88.5849295,7.23361386 88.5851738,7.80566626 88.2476028,8.18494487 Z" id=➜></path>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div class=icon_wrap>
                            <div class=icon_block>
                                <div class=shape>
                                    
                                    <svg width=190px height=190px viewBox="0 0 190 190" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                        <title>Shape</title>
                                        <desc>Created with Sketch.</desc>
                                        <defs>
                                            <radialGradient cx=50% cy=50% fx=50% fy=50% r=50% id=radialGradient-1>
                                                <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                                                <stop stop-color=#FFFFFF stop-opacity=0.05 offset=100%></stop>
                                            </radialGradient>
                                        </defs>
                                        <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                            <g id=1440---Education transform="translate(-843.000000, -261.000000)" fill=url(#radialGradient-1)>
                                                <g id=content transform="translate(200.000000, 261.000000)">
                                                    <g id=education transform="translate(643.000000, 0.000000)">
                                                        <g id=2>
                                                            <path d="M106.532053,0 C166.008047,0 206.361018,75.1604491 183.519714,134.09442 C160.67841,193.028391 145.090666,189.56616 118.843295,181.348697 C92.5959232,173.131233 53.7479816,209.67088 22.8692088,174.212236 C-5.10553198,148.544575 -3.53561472,108.849154 7.10489514,67.214306 C21.7847514,9.77405228 47.0560579,6.99720774e-15 106.532053,0 Z" id=Shape></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                                <div class=icon>
                                    
                                    <svg width=45px height=32px viewBox="0 0 45 32" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                        <title>icon 2</title>
                                        <desc>Created with Sketch.</desc>
                                        <g id=1440 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                            <g id=1440---Education transform="translate(-1116.000000, -303.000000)" stroke=#FFCFAF>
                                                <g id=content transform="translate(200.000000, 261.000000)">
                                                    <g id=education transform="translate(643.000000, 0.000000)">
                                                        <g id=3 transform="translate(200.000000, 0.000000)">
                                                            <g id=CONTENT transform="translate(65.000000, 43.000000)">
                                                                <g id=icon-2 transform="translate(9.000000, 0.000000)">
                                                                    <g id=Group-6>
                                                                        <path d="M8.19047619,16.7307692 L8.19047619,4.77692308 C8.19047619,3.89751582 8.8780389,3.18461538 9.72619048,3.18461538 C10.5743421,3.18461538 11.2619048,3.89751582 11.2619048,4.77692308 L11.2619048,15.6692308" id=Rectangle stroke-linecap=round></path>
                                                                        <path d="M11.2619048,15.1605229 L11.2619048,1.59230769 C11.2619048,0.712900437 11.9494675,0 12.797619,0 C13.6457706,0 14.3333333,0.712900437 14.3333333,1.59230769 L14.3333333,15.1605229" id=Rectangle stroke-linecap=round></path>
                                                                        <path d="M14.3333333,15.6692308 L14.3333333,3.71538462 C14.3333333,2.83597736 15.020896,2.12307692 15.8690476,2.12307692 C16.7171992,2.12307692 17.4047619,2.83597736 17.4047619,3.71538462 L17.4047619,15.6692308" id=Rectangle stroke-linecap=round></path>
                                                                        <path d="M17.4047619,15.6692308 L17.4047619,6.9 C17.4047619,6.02059274 18.0923246,5.30769231 18.9404762,5.30769231 C19.7886278,5.30769231 20.4761905,6.02059274 20.4761905,6.9 L20.4761905,17.7923077" id=Rectangle stroke-linecap=round></path>
                                                                        <path d="M20.4853138,16.0159876 L20.4853138,22.7722863 C20.4853138,31.3034201 8.84959058,31.2756565 6.38945192,27.4587755 C5.52155171,26.291905 3.45093833,22.5979107 0.177611775,16.3767924 C-0.201195213,15.6568942 0.045380942,14.7550709 0.73220162,14.3483912 C1.47096511,13.9109551 2.39625683,14.0091292 3.0342602,14.5926412 L7.50156605,18.6783976 C8.23673141,19.1656997 8.18535214,17.387405 8.18535214,16.3767924" id=Combined-Shape></path>
                                                                    </g>
                                                                    <g id=Group-6 transform="translate(32.500000, 15.000000) scale(-1, 1) translate(-32.500000, -15.000000) translate(22.000000, 0.000000)">
                                                                        <path d="M8.19047619,16.7307692 L8.19047619,4.77692308 C8.19047619,3.89751582 8.8780389,3.18461538 9.72619048,3.18461538 C10.5743421,3.18461538 11.2619048,3.89751582 11.2619048,4.77692308 L11.2619048,15.6692308" id=Rectangle stroke-linecap=round></path>
                                                                        <path d="M11.2619048,15.1605229 L11.2619048,1.59230769 C11.2619048,0.712900437 11.9494675,0 12.797619,0 C13.6457706,0 14.3333333,0.712900437 14.3333333,1.59230769 L14.3333333,15.1605229" id=Rectangle stroke-linecap=round></path>
                                                                        <path d="M14.3333333,15.6692308 L14.3333333,3.71538462 C14.3333333,2.83597736 15.020896,2.12307692 15.8690476,2.12307692 C16.7171992,2.12307692 17.4047619,2.83597736 17.4047619,3.71538462 L17.4047619,15.6692308" id=Rectangle stroke-linecap=round></path>
                                                                        <path d="M17.4047619,15.6692308 L17.4047619,6.9 C17.4047619,6.02059274 18.0923246,5.30769231 18.9404762,5.30769231 C19.7886278,5.30769231 20.4761905,6.02059274 20.4761905,6.9 L20.4761905,17.7923077" id=Rectangle stroke-linecap=round></path>
                                                                        <path d="M20.4853138,16.0159876 L20.4853138,22.7722863 C20.4853138,31.3034201 8.84959058,31.2756565 6.38945192,27.4587755 C5.52155171,26.291905 3.45093833,22.5979107 0.177611775,16.3767924 C-0.201195213,15.6568942 0.045380942,14.7550709 0.73220162,14.3483912 C1.47096511,13.9109551 2.39625683,14.0091292 3.0342602,14.5926412 L7.50156605,18.6783976 C8.23673141,19.1656997 8.18535214,17.387405 8.18535214,16.3767924" id=Combined-Shape></path>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection