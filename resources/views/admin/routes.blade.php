@extends('admin.layouts.layout')

@push('topBtn')
    @if($edit)
        <a class="btn btn-success btn-sm mg-l-10" href="{{route('admin.routes.info')}}"><i class="fa fa-plus mg-r-10"></i>Создать</a>

        <div class="pull-right">
            <a href="{{route('admin.routes.seeds')}}" class="btn btn-outline-primary btn-sm"><i class="fa fa-download mg-r-10"></i> Скачать Seeds</a>
            <a href="{{route('admin.routes.refresh')}}" class="btn btn-outline-danger btn-sm"><i class="fa fa-refresh mg-r-10"></i> Обновить Seeds</a>
        </div>
    @endif
@endpush

@section('content')
    <div class="box-body card">
        <div class="table-responsive">
            <table id="datatable1" class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Name</th>
                    <th>Controller</th>
                    <th></th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div><!-- table-wrapper -->
    </div><!-- card -->

@endsection

@push('scripts')
<script>
    $(function(){

        var dataTable= $('#datatable1').DataTable({
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            bPaginate:true,
            responsive: false,
            order: [[0, 'desc']],
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            "ajax": {
                "url": "{{route('admin.routes.search')}}",
            },
            "drawCallback": function () {
                //Warning Message
                $('.sa-warning').click(function(){
                    event= $(this);
                    swal({
                        title: "Вы уверены?",
                        text: "Страница будет помещена в корзину!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Да, удалить",
                        closeOnConfirm: false
                    }, function(){
                        $.ajax({
                            type:'get',
                            url: "{{route('admin.routes.item.action', ['action'=>'delete'])}}",
                            data: {
                                'data': $(event).data('id'),
                                'ajax': true,
                            },
                            headers: {
                                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                            },
                            success:function(data){
                                swal("Deleted!", data['msg'], data['status']);
                                dataTable.draw()
                                $('[data-toggle="tooltip"]').tooltip('dispose');
                            },
                            error: function(msg){
                                console.log(msg);
                                swal("Deleted!", 'Не удалось удалить запись.', 'error');
                            }
                        });


                    });
                });
            }
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

    });
</script>

@endpush