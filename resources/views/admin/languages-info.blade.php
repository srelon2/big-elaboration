@extends('admin.layouts.layout')

@push('topBtn')
    @if($edit && isset($item))<a class="btn btn-success btn-sm mg-l-10" href="{{route('admin.languages.info')}}"><i class="fa fa-plus mg-r-10"></i>Создать</a><a class="btn btn-primary btn-sm mg-l-10 btn-outline" data-toggle="modal" data-target="#backups" href="#"><i class="fa fa-history mg-r-5"></i> Бекапы</a> @endif
@endpush

@section('content')
    @if(isset($backups))
        <div id="backups" class="modal fade" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-lg wd-100p" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-header pd-x-20">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">История изменений</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body pd-20">
                        <div class="table-responsive">
                            <table id="table-log" class="table table-striped tx-14">
                                <thead>
                                <tr>
                                    <th>Admin</th>
                                    <th class="text-center">Date</th>
                                    <th>Action</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($backups as $key=>$backup)
                                    <tr>
                                        <td><a class="tx-inverse tx-14 tx-medium d-block" href="{{route('admin.administrators.info', ['id'=> $backup->admin_id])}}">{{$backup->admin}}</a> </td>
                                        <td class="text-center">{{$backup->date}}</td>
                                        <td class="wd-200">{{$backup->desc}}</td>
                                        <td><a href="{{route('admin.languages.info', ['id'=>$item->id, 'backup'=>$key])}}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-undo mg-r-10"></i> Восстановить</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div><!-- modal-dialog -->
        </div>
    @endif
    @if($edit)
    <form method="post" action="{{route('admin.languages.update', ['id'=>(isset($item) ? $item->id : '')])}}">
        @csrf
    @endif
        <div class="card pd-20 pd-sm-20">
        <div class="form-layout">
            <p class="mg-b-20 mg-sm-b-30">
                Просмотр перевода
            </p>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="link">Key: <span class="tx-danger">*</span></label>
                    <input type="text" name="key" id="key" class="form-control {{ $errors->has('key') ? ' parsley-error' : '' }}" placeholder="Enter key" value="{{(isset($item) ? $item->key : old('key') )}}" @if(isset($item)) disabled @endif>
                    @if ($errors->has('key'))
                        <span class="help-block">
                        <ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required">{{ $errors->first('key') }}</li></ul>
                    </span>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label for="name">Full Name: <span class="tx-danger">*</span></label>
                    <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? ' parsley-error' : '' }}" placeholder="Enter name" value="{{(isset($item) ? $item->name : old('name') )}}" >
                    @if ($errors->has('name'))
                        <span class="help-block">
                        <ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="translation" class="control-label">Translation</label>
                <pre class="ace-editor ht-500" id="translation" data-plugin="ace" data-mode="javascript"></pre>
                <textarea name="translation">{{(isset($item) ? $translation : ((old('translation')) ? old('translation') : $translation))}}</textarea>
            </div>
        </div>
        <div class="form-layout-footer">
            @if($edit)
                <button type="submit" class="btn btn-info mg-r-5">Сохранить</button>
            @endif
            <a href="{{ route('admin.languages') }}" class="btn btn-secondary">Назад</a>
        </div>
    </div>
    @if($edit)
        </form>
    @endif
@endsection


@push('scripts')
<script src="{{asset('admin')}}/assets/vendor_plugins/ace-builds-master/src-min-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>

<script>

    $(document).ready(function () {
        var editor = ace.edit("translation");
        var textarea = $('textarea[name="translation"]').hide();
        editor.setTheme("ace/theme/solarized_light");
        editor.getSession().setMode("ace/mode/javascript");
        editor.setShowPrintMargin(false);
        editor.getSession().setValue(textarea.val());
        editor.getSession().on('change', function(){
            textarea.val(editor.getSession().getValue());
        });

        $('#table-log').DataTable({
            order: [[1, 'desc']],
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            "ajax": null,
        });
        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    });
</script>
@endpush