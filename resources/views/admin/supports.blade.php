@extends('admin.layouts.layout')

@push('topBtn')
    @if($id)
        <a href="{{route("admin.support.read", ['id'=>$id])}}" class="btn btn-outline-primary btn-sm mg-l-10">Просмотреть</a>
    @endif
@endpush

@section('content')
    <div class="card box-body">
        <div class="row">
            <div class="col-lg-3 col-12">
                <div class="h-p100 p-15 bg-light">
                    <a href="{{route('admin.support.compose')}}" class="btn btn-success btn-block btn-shadow margin-bottom">Написать сообщение</a>
                    @include('admin.supports-right-bar')
                    <!-- /.box -->
                </div>
            </div>
            <div class="col-lg-9 col-12">
                <div>
                    <div class="table-responsive">
                        <table id="datatable1" class="table table-striped table-mailbox">
                            <thead>
                            <tr>
                                <th class="wd-20 pd-r-0-force">
                                    <input id="all" data-group="all" type="checkbox" class="filled-in checkbox-all toggle-all checkbox-toggle">
                                    <label for="all" class="ckbox mg-b-0"></label>
                                </th>
                                <th class="wd-20 pd-r-0-force"></th>
                                <th>Email</th>
                                <th>Message</th>
                                <th class="text-center">Date</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- card -->
    <input type="hidden" class="data">
@endsection

@push('scripts')
<script>
    $(function(){

        @switch($type)
            @case('trash')
                var btnGroup= '<div class="btn-group mg-b-0 hidden-xs-down mg-r-15 group-action"><button class="btn btn-outline btn-sm" data-type="forceDelete" title="Удалить" data-toggle="tooltip" data-placement="bottom"><i class="icon ion-ios-trash-outline tx-20"></i></button><button class="btn btn-outline btn-sm" data-type="inbox" title="Восстановить" data-toggle="tooltip" data-placement="bottom"><i class="icon ion-ios-filing-outline tx-24"></i></button></div>';
                @break
            @case('important')
                var btnGroup= '<div class="btn-group mg-b-0 hidden-xs-down mg-r-15 group-action"><button class="btn btn-outline btn-sm" data-type="" title="Убрать из избранного" data-toggle="tooltip" data-placement="bottom"><i class="icon ion-ios-filing-outline tx-24"></i></button><button class="btn btn-outline btn-sm" data-type="archive" title="Добавить в архив" data-toggle="tooltip" data-placement="bottom"><i class="icon ion-ios-folder-outline tx-20"></i></button><button class="btn btn-outline btn-sm" data-type="deleted" title="Добавить в корзину" data-toggle="tooltip" data-placement="bottom"><i class="icon ion-ios-trash-outline tx-20"></i></button></div>';
                @break
            @case('archive')
                var btnGroup= '<div class="btn-group mg-b-0 hidden-xs-down mg-r-15 group-action"><button class="btn btn-outline btn-sm" data-type="" title="Убрать из архива" data-toggle="tooltip" data-placement="bottom"><i class="icon ion-ios-filing-outline tx-24"></i></button><button class="btn btn-outline btn-sm" data-type="deleted" title="Добавить в корзину" data-toggle="tooltip" data-placement="bottom"><i class="icon ion-ios-trash-outline tx-20"></i></button></div>';
                @break
            @case('draft')
                var btnGroup= '<div class="btn-group mg-b-0 hidden-xs-down mg-r-15 group-action"><button class="btn btn-outline btn-sm" data-type="deleted" title="Добавить в корзину" data-toggle="tooltip" data-placement="bottom"><i class="icon ion-ios-trash-outline tx-20"></i></button></div>';
                @break
            @default
                var btnGroup= '<div class="btn-group mg-b-0 hidden-xs-down mg-r-15 group-action"> <button class="btn btn-outline btn-sm" data-type="important" title="Добавить в избранное" data-toggle="tooltip" data-placement="bottom"><i class="icon ion-ios-star-outline tx-20"></i></button> <button class="btn btn-outline btn-sm" data-type="archive" title="Добавить в архив" data-toggle="tooltip" data-placement="bottom"><i class="icon ion-ios-folder-outline tx-20"></i></button><button class="btn btn-outline btn-sm" data-type="deleted" title="Добавить в корзину" data-toggle="tooltip" data-placement="bottom"><i class="icon ion-ios-trash-outline tx-20"></i></button></div>';
        @endswitch


        var dataTable= $('#datatable1').DataTable({
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            bPaginate:true,
            responsive: false,
            order: [[4, 'desc']],
            "language": {
                "sLengthMenu": '<div class="mailbox-controls">'+btnGroup+' _MENU_ Записей </div>',
                "sInfo": btnGroup+'Showing _START_ to _END_ of _TOTAL_ entries</span>',
            },
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [0, 1, 5] /* 1st one, start by the right */
            }],
            "ajax": {
                "url": "{{route('admin.supports.search', ['type'=>$type, 'table'=>$table, 'id'=>$id])}}",
            },
            "columns": [
                {className:'pd-r-0-force'},
                {className:'pd-r-0-force'},
                false,
                {className:'row-message'},
                {className: 'text-center'},
                {className:'row-nav'}
            ],
            "autoWidth": false,
            "drawCallback": function () {
                $data= $('.data');
                @if($edit)
                    $(".checkbox-toggle").prop("checked", false);

                    $(".checkbox").on("change", function() {
                        var group = $(this).data('group');
                        var allChecked = $('.checkbox[data-group="' + group + '"]:not(:checked)').length == 0;
                        $('.toggle-'+group).prop("checked", allChecked);
                        if(group=='all' && !this.checked) {
                            if(!this.checked) {}
                            $('.checkbox[data-id="' + $(this).data('id')+'"]').prop("checked", this.checked);
                            $('.toggle-all').prop("checked", this.checked);
                        }
                    });
                    $(".checkbox-toggle").on("change", function() {
                        var group = $(this).data('group');
                        $('.checkbox-'+group).prop("checked", this.checked);

                        if(group=='all' && !this.checked) {
                            $('.checkbox-all').prop("checked", false);
                        }
                    });

                $('.row-message').on('click', function () {
                    $(this).toggleClass('active');
                });
                $('.btn-table').on('click', function () {
                    $id= $(this).parents('tr').find('.btn-item').data('id');
                    $checked={
                        checked: [$id],
                        type: $(this).attr('data-type'),
                    };
                    $draw= false;
                    $data.attr('data-route', "{{route('admin.support.action')}}");
                    switch ($(this).data('value')){
                        case 'important':
                            if($(this).find('.fa').is('.fa-star')) {
                                $(this).attr('data-original-title', 'Добавить в избранное').tooltip('hide').tooltip('update').tooltip('show');
                            } else {
                                $(this).attr('data-original-title', 'Убрать из избранного').tooltip('hide').tooltip('update').tooltip('show');
                            };
                            $(this).find('.fa').toggleClass('fa-star').toggleClass('fa-star-o');
                            break;
                        case 'view':
                            if($(this).find('.fa').is('.fa-envelope')) {
                                $(this).attr('data-original-title', 'Пометить как непрочитанное').attr('data-type', 'viewed').tooltip('hide').tooltip('update').tooltip('show');
                            } else {
                                $(this).attr('data-original-title', 'Пометить как прочитанное').attr('data-type', 'not-viewed').tooltip('hide').tooltip('update').tooltip('show');
                            };
                            $(this).toggleClass('btn-primary').toggleClass('btn-default').find('.fa').toggleClass('fa-envelope-open').toggleClass('fa-envelope');
                            $(this).parents('tr').find('.message').toggleClass('tx-bold');
                            break;
                        default:
                            $draw= true;
                    }
                    $('[data-toggle="tooltip"]').tooltip('update');

                    ajax($checked, $draw);
                });

                @else
                    $('input').prop('readonly', true);
                @endif

                $('[data-toggle="tooltip"]').tooltip({
                    'delay': {
                        'show': 600,
                        'hide': 0,
                    }
                });
            }
        });
        $('.group-action .btn').on('click', function () {
            $data.attr('data-route', "{{route('admin.support.action')}}");
            $checked={
                checked: [],
                type: $(this).data('type'),
            };
            if($('.checkbox:checked').length) {
                $('.checkbox:checked').each(function(){
                    if($(this).prop('checked')) $checked.checked[$checked.checked.length]=$(this).val();
                });

                ajax($checked, true);
            }
        });

        function  ajax($value, $draw, $type= 'post') {
            $data= $('.data');
            $.ajax({
                type:$type,
                url: $data.data('route'),
                data: {
                    'data': $value,
                    'table': '{{$table}}'
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
//                    var msgBlock= $('.msg-block .alert-'+data['status']);
//                    msgBlock.removeClass('d-none').find('.align-items-center span').text(data['msg']);
                    if(data['status']=='success') {
                        $.toast({
                            heading: 'Alert. Success!',
                            text: data['msg'],
                            position: 'top-right',
                            loaderBg: '#ff6849',
                            icon: 'success',
                            hideAfter: 3500,
                            stack: 6
                        });
                    } else {
                        $.toast({
                            heading: 'Alert. Error!',
                            text: data['msg'],
                            position: 'top-right',
                            loaderBg: '#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }

                    if($draw) {
                        dataTable.draw()
                        $('[data-toggle="tooltip"]').tooltip('dispose');
                    }
                },
                error: function(msg){
                    console.log(msg);
                }
            });
        }

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    });
</script>

@endpush