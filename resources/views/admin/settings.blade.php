@extends('admin.layouts.layout')

@section('content')
    <div class="card box-body">
        <div class="row">
            <div class="col-lg-3 col-12">
                @include('admin.settings-left-bar')
            <!-- /.box -->
            </div>
            @include ('admin.settings-'.$type)
        </div>
    </div><!-- card -->
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('.select2').select2();
        });
    </script>
@endpush