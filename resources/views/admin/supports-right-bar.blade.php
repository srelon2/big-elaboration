<div class="box bg-transparent no-border no-shadow">
    <div class="box-header with-border">
        <h3 class="box-title">Входящие</h3>
    </div>
    <div class="box-body no-padding mailbox-nav">
        <ul class="nav nav-pills flex-column">
            <li class="nav-item"><a class="nav-link  @if(isset($type) && $type=='inbox') active @endif" href="{{route('admin.supports')}}"><i class="ion ion-ios-email"></i> Почтовый ящик
                    <span class="label label-success pull-right">{{$newMessages->count()}}</span>
                </a>
            </li>
            {{--<li class="nav-item"><a class="nav-link" href="javascript:void(0)"><i class="ion ion-paper-airplane"></i> Sent</a></li>--}}
            <li class="nav-item"><a class="nav-link @if(isset($type) && $type=='archive') active @endif" href="{{route('admin.supports', ['type'=>'archive'])}}"><i class="ion ion-ios-folder"></i> Архив</a></li>
            <li class="nav-item"><a class="nav-link @if(isset($type) && $type=='important') active @endif" href="{{route('admin.supports', ['type'=>'important'])}}"><i class="ion ion-star"></i>  Избранное</a>
            </li>
            <li class="nav-item"><a class="nav-link @if(isset($type) && $type=='trash' && $table== 'supports') active @endif" href="{{route('admin.supports', ['type'=>'trash'])}}"><i class="ion ion-trash-a"></i> Корзина</a></li>
        </ul>
    </div>
    <!-- /.box-body -->
</div>
<!-- /. box -->
<div class="box bg-transparent no-border no-shadow">
    <div class="box-header with-border">
        <h3 class="box-title">Исходящие</h3>
    </div>
    <div class="box-body no-padding mailbox-nav">
        <ul class="nav nav-pills flex-column">
            <li class="nav-item"><a class="nav-link  @if(isset($type) && $type=='sent') active @endif" href="{{route('admin.supports', ['type'=>'sent', 'table'=> 'answers'])}}"><i class="fa fa-paper-plane"></i> Отправленные</a></li>
            <li class="nav-item"><a class="nav-link  @if(isset($type) && $type=='draft') active @endif" href="{{route('admin.supports', ['type'=>'draft', 'table'=> 'answers'])}}"><i class="fa fa-file-text  {{(isset($type) && $type=='draft') ? '' : 'text-light-blue'}}"></i> Черновики</a></li>
            <li class="nav-item"><a class="nav-link  @if(isset($type) && $type=='trash' && $table== 'answers') active @endif" href="{{route('admin.supports', ['type'=>'trash', 'table'=> 'answers'])}}"><i class="fa fa-trash  {{(isset($type) && $type=='trash') ? '' : 'text-danger'}}"></i> Корзина</a></li>
        </ul>
    </div>
    <!-- /.box-body -->
</div>