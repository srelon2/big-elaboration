@extends('admin.layouts.layout')

@section('content')
    <div class="box-body card">
        <div class="table-responsive">
            <table id="table-log" class="table table-striped" >
                <thead>
                    <tr>
                        <th>Admin</th>
                        <th>Date</th>
                        <th>Action</th>
                        <th>Model</th>
                        <th>location</th>
                        <th>ip</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($items as $key => $item)
                        <tr>
                            <td><a class="tx-inverse tx-14 tx-medium d-block" href="{{route('admin.administrators.info', ['id'=> $item->admin_id])}}">{{$item->admin}}</a> </td>
                            <td>{{$item->date}}</td>
                            <td>{{$item->desc}}</td>
                            <td>{{$item->model}}</td>
                            <td>{{$item->location}}</td>
                            <td>{{$item->ip}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $('#table-log').DataTable({
                "order": [1, 'desc'],
                "ajax": null,
            });
            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
        });
    </script>
@endpush