@extends('admin.layouts.layout')
@push('topBtn')
    @if($table=='answers' && isset($item->indicator_id))
        <a href="{{route("admin.support.read", ['id'=>$item->indicator_id])}}" class="btn btn-outline-primary btn-sm mg-l-10">Входящее</a>
    @endif
@endpush

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-3 col-12">
                    <div class="h-p100 p-15 bg-light">
                        <a href="{{route('admin.supports')}}" class="btn btn-success btn-block btn-shadow margin-bottom">В почтовый яшик</a>
                        @include('admin.supports-right-bar')
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-lg-9 col-12">
                    <div class="box bg-transparent no-border no-shadow">
                        <!-- /.box-header -->
                        <div class="box-body b-1">
                            <div class="mailbox-read-info">
                                <h3>@switch($table)
                                    @case('answers')
                                        {{($item->drafts) ? 'Черновое' : 'Отправленное'}} сообщение
                                    @break
                                    @default
                                        Полученное сообщение
                                @endswitch</h3>
                            </div>
                            <div class="mailbox-read-info clearfix">
                                <div class="left-float margin-r-5"><a href="#"><img src="{{asset('admin')}}/images/1.jpg" alt="user" width="40" class="rounded-circle"></a></div>
                                <h5 class="no-margin"> {{(isset($item->name)) ? $item->name : 'Не указано'}}<br>
                                    <small>From: {{$item->email}}</small>
                                    <span class="mailbox-read-time pull-right">{{$item->created_at->format('F j, Y, g:i a')}}</span></h5>
                            </div>
                            <!-- /.mailbox-read-info -->
                            <div class="mailbox-controls with-border clearfix mt-15">
                                <div class="left-float">
                                    @if($table=='supports')
                                        <a href="{{route('admin.support.item.action', ['action'=> 'view','id'=>$item->id, 'table'=> $table])}}" class="btn btn-outline btn-sm" data-toggle="tooltip" title="{{($item->view) ? 'Отметить как просмотренное' : 'Отметить как не просмотренное'}}"><i class="fa {{($item->view) ? 'fa-envelope-open': 'fa-envelope' }}"></i></a>
                                    @endif
                                    <button type="button" class="btn btn-outline btn-sm" data-toggle="tooltip" title="Print"><i class="fa fa-print"></i></button>
                                </div>
                                <div class="right-float">
                                    <div class="btn-group">
                                        @if($item->trashed())
                                            <a href="{{route('admin.support.item.action', ['action'=> 'restore','id'=>$item->id, 'table'=> $table])}}" class="btn btn-outline btn-sm" data-toggle="tooltip" data-container="body" title="Restore"><i class="icon ion-ios-filing-outline"></i></a>
                                            <a href="{{route('admin.support.item.action', ['action'=> 'forceDelete','id'=>$item->id, 'table'=> $table])}}" class="btn btn-outline btn-sm" data-toggle="tooltip" data-container="body" title="Force delete"><i class="fa fa-trash-o"></i></a>
                                        @else
                                            <a href="{{route('admin.support.item.action', ['action'=> 'delete','id'=>$item->id, 'table'=> $table])}}" class="btn btn-outline btn-sm"  data-toggle="tooltip" data-container="body" title="Delete"><i class="fa fa-trash-o"></i></a>
                                        @endif

                                        @switch($table)
                                            @case('answers')
                                                <a  href="{{route('admin.support.compose', ['id'=>$item->id])}}" class="btn btn-outline btn-sm" data-toggle="tooltip" data-container="body" title="Переслать"><i class="fa fa-share"></i></a>
                                            @break
                                            @default
                                                <a  href="{{route('admin.support.compose', ['id'=>$item->id, 'answer'=> 'answer'])}}" class="btn btn-outline btn-sm" data-toggle="tooltip" data-container="body" title="Ответить"><i class="fa fa-reply"></i></a>
                                                <a  href="{{route('admin.supports', ['type'=>'sent', 'table'=>'answers', 'id'=> $item->id])}}" class="btn btn-outline btn-sm" data-toggle="tooltip" data-container="body" title="Ответы"><i class="fa fa-share"></i></a>
                                        @endswitch
                                    </div>
                                </div>
                                <!-- /.btn-group -->
                            </div>
                            <!-- /.mailbox-controls -->
                            <div class="mailbox-read-message">
                                {!! $item->text !!}
                            </div>
                            <!-- /.mailbox-read-message -->
                        </div>
                        <!-- /.box-body -->
                        {{--<div class="box-footer">--}}
                            {{--<h5><i class="fa fa-paperclip m-r-10 m-b-10"></i> Attachments <span>(3)</span></h5>--}}
                            {{--<ul class="mailbox-attachments clearfix">--}}
                                {{--<li>--}}
                                    {{--<span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>--}}

                                    {{--<div class="mailbox-attachment-info">--}}
                                        {{--<a href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> Mag.pdf</a>--}}
                                        {{--<span class="mailbox-attachment-size">--}}
                          {{--5,215 KB--}}
                          {{--<a href="#" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>--}}
                        {{--</span>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<span class="mailbox-attachment-icon"><i class="fa fa-file-word-o"></i></span>--}}

                                    {{--<div class="mailbox-attachment-info">--}}
                                        {{--<a href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> Documents.docx</a>--}}
                                        {{--<span class="mailbox-attachment-size">--}}
                          {{--2,145 KB--}}
                          {{--<a href="#" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>--}}
                        {{--</span>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<span class="mailbox-attachment-icon has-img"><img src="{{asset('admin')}}/images/photo1.png" alt="Attachment"></span>--}}

                                    {{--<div class="mailbox-attachment-info">--}}
                                        {{--<a href="#" class="mailbox-attachment-name"><i class="fa fa-camera"></i> Image.png</a>--}}
                                        {{--<span class="mailbox-attachment-size">--}}
                          {{--2.67 MB--}}
                          {{--<a href="#" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>--}}
                        {{--</span>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                        <!-- /.box-footer -->
                        <div class="box-footer">
                            <div class="pull-right">
                                @switch($table)
                                    @case('answers')
                                        <a href="{{route('admin.support.compose', ['id'=>$item->id])}}" class="btn btn-info"><i class="fa fa-share mg-r-10"></i> Переслать</a>
                                    @break
                                    @default
                                        <a href="{{route('admin.support.compose', ['id'=>$item->id, 'answer'=> 'answer'])}}" class="btn btn-success"><i class="fa fa-reply mg-r-10"></i> Ответить</a>
                                        <a href="{{route('admin.supports', ['type'=>'sent', 'table'=>'answers', 'id'=> $item->id])}}" class="btn btn-info"><i class="fa fa-share mg-r-10"></i> Ответы</a>
                                @endswitch
                            </div>
                            @if($item->trashed())
                                <a href="{{route('admin.support.item.action', ['action'=> 'restore','id'=>$item->id, 'table'=> $table])}}" class="btn btn-info"><i class="icon ion-ios-filing-outline mg-r-10"></i> Воостановить</a>
                                <a href="{{route('admin.support.item.action', ['action'=> 'forceDelete','id'=>$item->id, 'table'=> $table])}}" class="btn btn-danger"><i class="fa fa-trash-o mg-r-10"></i> Удалить</a>
                            @else
                                @if($table=='supports')
                                    <a href="{{route('admin.support.item.action', ['action'=> 'view','id'=>$item->id, 'table'=> $table])}}" class="btn {{($item->view) ? 'btn-default': 'btn-primary' }}"><i class="fa mg-r-10 {{($item->view) ? 'fa-envelope-open': 'fa-envelope' }}"></i> {{($item->view) ? 'Не просмотренно' : 'Просмотренно'}}</a>
                                @endif
                                <a href="{{route('admin.support.item.action', ['action'=> 'delete','id'=>$item->id, 'table'=> $table])}}" class="btn btn-danger"><i class="fa fa-trash-o mg-r-10"></i> Удалить</a>
                            @endif
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /. box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </div>
<!-- /.content -->



    <!-- /.content-wrapper -->

@endsection