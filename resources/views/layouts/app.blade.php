<!DOCTYPE html>
<html lang=en>

<head>

    <meta charset=utf-8>

    <title>Big Elaboration</title>

    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=viewport content="width=device-width,initial-scale=1,maximum-scale=1">
    <meta name=description content="Elaboration intelligence cloud computing for all.">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property=og:description content="Elaboration intelligence cloud computing for all.">
    <meta property=og:title content=Big Elaboration>
    <meta property=og:image content=assets/img/poster-site.png>

    <link rel="icon" href="{{asset('favicon.ico')}}">
    <link rel=apple-touch-icon sizes=180x180 href={{asset('/')}}assets/img/favicon-180x180.png>
    <link rel=icon type=image/png sizes=32x32 href={{asset('/')}}assets/img/favicon-32x32.png>
    <link rel=icon type=image/png sizes=16x16 href={{asset('/')}}assets/img/favicon-16x16.png>
    <meta name=msapplication-TileColor content=#7C55F2>
    <meta name=theme-color content=#7C55F2>


    <link href="https://fonts.googleapis.com/css?family=Muli:200,300,400,600,700,800,900&amp;subset=latin-ext" rel=stylesheet>
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700&amp;subset=latin-ext" rel=stylesheet>


    <script id=window.appReady>
        'use strict';var ild=!1,cbs=[],w=window,ercbs=[];w.addEventListener('load',function(){ild=!0,cbs.forEach(function(a){try{a()}catch(b){ercbs.forEach(function(d){return d(b)})}}),cbs=null}),w.appReady=function(a){a&&(ild?a():cbs.push(a))},w.appReady.addErrorHandler=function(a){ercbs.push(a)};
    </script>



    <link href="{{asset('/')}}css/app.554b65.css" rel="stylesheet">
    <link href="{{asset('/')}}css/main.css" rel="stylesheet">
    <style>html{--css-variable-test:1}#css-variable-test{display:none;opacity:var(--css-variable-test)}</style>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106568684-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-106568684-1');
    </script>
</head>

<body @if(isset($pageScroll)) class="one-page-scroll" @endif>
    <div id=cursor-1></div>
    <div id=cursor-2></div>

        <header id=header class=header>


        <div class="container header__container">
            <div class=logos__wrap>
                <div class="logo__item logo__item--elaboration">
                    <a href="{{route('main')}}" class="logo js-link leave-link">
                        {{--<img src=assets/img/logo.svg alt="elaboration logo" class=logo__image>--}}
                        {{__("logo")}}
                    </a>
                </div>
                <a href=mailto:support@bigelaboration.com rel="nofollow noreferrer noopener" class="link-text up-text email js-link">

                    <svg width=18px height=13px viewBox="0 0 18 13" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g id=Symbols stroke=none stroke-width=1 fill=none fill-rule=evenodd opacity=0.800000012>
                            <g id=main-screen-1920/-default transform="translate(-1700.000000, -37.000000)" fill-rule=nonzero stroke=#FFFFFF>
                                <g id=main-screen-elements transform="translate(48.000000, 27.000000)">
                                    <g id=header>
                                        <g id=right transform="translate(1086.000000, 0.000000)">
                                            <g id=mail transform="translate(566.000000, 10.000000)">
                                                <rect id=Rectangle x=0.5 y=0.5 width=17 height=12 rx=2></rect>
                                                <path d="M0.720991041,2.69934946 L7.21576177,7.22805702 C8.24675027,7.94695005 9.61657431,7.94695005 10.6475628,7.22805702 L17.1423335,2.69934946" id=Path-7 stroke-linecap=round></path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                    <span class="link-text up-text">{{__("support@bigelaboration.com")}}</span>
                </a>
            </div>
            <div class=nav__wrap>
                <nav class=menu__wrap>
                    <ul class=menu__list>
                        <li class=menu__item>

                        </li><li class=menu__item>
                            <a href="{{route('developers')}}" rel="nofollow noreferrer noopener" class="link-text up-text menu__item__link js-link leave-link">Developers</a>
                        </li>
                        <li class=menu__item>
                            <a href="{{route('about')}}" class="link-text up-text menu__item__link js-link leave-link">about</a>
                        </li>
                        <li class=menu__item>
                            <a href="{{route('blog')}}" rel="nofollow noreferrer noopener" class="link-text up-text menu__item__link js-link leave-link">blog</a>
                        </li>
                    </ul>
                </nav>
                <div class=download-icons__wrap>
                    <a href="#" target=_blank rel="nofollow noreferrer noopener" class="download__link download__link--apple js-link">
                        <svg width=16 height=21 viewBox="0 0 16 21" fill=none xmlns="http://www.w3.org/2000/svg">
                            <path d="M6.65451 20.2011C5.33111 20.2011 4.42332 18.9215 3.76709 17.9699C2.46556 16.0778 1.12029 12.2389 2.74993 9.43893C3.49366 8.1046 4.9155 7.24056 6.46858 7.19681C7.15762 7.17493 7.81386 7.43743 8.3279 7.64523C8.63414 7.76554 8.94039 7.88585 9.11538 7.88585C9.25757 7.88585 9.57474 7.75461 9.85911 7.64523C10.5044 7.39368 11.3247 7.06556 12.1997 7.164C13.4903 7.20775 14.7371 7.87491 15.4918 8.9577C15.5793 9.07801 15.6121 9.23113 15.5793 9.38425C15.5465 9.53737 15.459 9.65768 15.3277 9.74518C14.4309 10.292 13.884 11.2436 13.8731 12.2935C13.8731 13.4529 14.573 14.5138 15.6668 14.9622C15.9293 15.0716 16.0605 15.356 15.973 15.6294C15.7105 16.4825 15.3058 17.2918 14.7918 18.0246C14.1902 18.9215 13.3481 20.1683 11.9372 20.1902C11.3247 20.2011 10.8763 20.0152 10.4935 19.8621C10.1325 19.709 9.8263 19.5777 9.32319 19.5777C8.77633 19.5777 8.44821 19.7199 8.07635 19.873C7.71542 20.0261 7.31074 20.1902 6.74201 20.2121C6.7092 20.2011 6.67639 20.2011 6.65451 20.2011ZM6.52327 8.29053C6.51233 8.29053 6.50139 8.29053 6.49045 8.29053C5.33111 8.32334 4.25927 8.97957 3.70147 9.98579C2.4765 12.0967 3.28585 15.356 4.66394 17.3575C5.32017 18.2981 5.98734 19.1184 6.68732 19.1184C7.05919 19.1074 7.31074 18.998 7.63886 18.8559C8.06541 18.6809 8.54665 18.4731 9.31225 18.4731C10.045 18.4731 10.5263 18.6809 10.9091 18.8449C11.2481 18.9871 11.5106 19.1074 11.9153 19.0855C12.7028 19.0746 13.2496 18.3418 13.8949 17.3903C14.2668 16.8653 14.5621 16.3184 14.7809 15.7278C13.5559 15.0169 12.7793 13.7044 12.7793 12.2717C12.7903 11.0576 13.3371 9.93111 14.2449 9.1655C13.6871 8.60771 12.9215 8.26865 12.1231 8.24678C11.4669 8.18116 10.8216 8.43271 10.2529 8.66239C9.8263 8.83739 9.45443 8.97957 9.10444 8.97957C8.72164 8.97957 8.3279 8.82645 7.91229 8.65146C7.46386 8.47646 6.98263 8.29053 6.52327 8.29053Z" fill=white />
                            <path d="M8.71071 6.10931C8.69977 6.10931 8.6779 6.10931 8.66696 6.10931C8.38259 6.10931 8.14197 5.87963 8.13104 5.59526C8.05448 4.4578 8.42634 3.36408 9.18101 2.51097C9.93568 1.65787 10.9638 1.12195 12.1012 1.00164C12.2434 0.990703 12.3965 1.03445 12.5059 1.13289C12.6153 1.23132 12.6918 1.36257 12.7028 1.51569C12.7903 2.67503 12.4294 3.80156 11.6856 4.69841C10.9528 5.59526 9.87005 6.10931 8.71071 6.10931ZM11.6091 2.21567C10.9856 2.39066 10.4278 2.74065 10.0013 3.23283C9.56381 3.725 9.30132 4.32655 9.23569 4.96091C9.85911 4.8406 10.4278 4.50154 10.8435 3.99843C10.8435 3.99843 10.8435 3.99843 10.8544 3.9875C11.27 3.47345 11.5325 2.86096 11.6091 2.21567Z" fill=white />
                            <path class=stroke d="M6.65451 20.2011C5.33111 20.2011 4.42332 18.9215 3.76709 17.9699C2.46556 16.0778 1.12029 12.2389 2.74993 9.43893C3.49366 8.1046 4.9155 7.24056 6.46858 7.19681C7.15762 7.17493 7.81385 7.43743 8.3279 7.64523C8.63414 7.76554 8.94039 7.88585 9.11538 7.88585C9.25756 7.88585 9.57474 7.75461 9.85911 7.64523C10.5044 7.39368 11.3247 7.06556 12.1997 7.164C13.4903 7.20775 14.7371 7.87492 15.4918 8.9577C15.5793 9.07801 15.6121 9.23113 15.5793 9.38425C15.5465 9.53737 15.459 9.65768 15.3277 9.74518C14.4309 10.292 13.884 11.2436 13.8731 12.2935C13.8731 13.4529 14.573 14.5138 15.6668 14.9622C15.9293 15.0716 16.0605 15.356 15.973 15.6294C15.7105 16.4825 15.3058 17.2918 14.7918 18.0246C14.1902 18.9215 13.3481 20.1683 11.9372 20.1902C11.3247 20.2011 10.8763 20.0152 10.4935 19.8621C10.1325 19.709 9.8263 19.5777 9.32319 19.5777C8.77633 19.5777 8.44821 19.7199 8.07635 19.873C7.71542 20.0261 7.31074 20.1902 6.74201 20.2121C6.7092 20.2011 6.67639 20.2011 6.65451 20.2011Z" fill=white />
                            <path class=stroke d="M8.71071 6.10931C8.69977 6.10931 8.6779 6.10931 8.66696 6.10931C8.38259 6.10931 8.14197 5.87963 8.13104 5.59526C8.05448 4.4578 8.42634 3.36408 9.18101 2.51097C9.93568 1.65787 10.9638 1.12195 12.1012 1.00164C12.2434 0.990703 12.3965 1.03445 12.5059 1.13289C12.6153 1.23132 12.6918 1.36257 12.7028 1.51569C12.7903 2.67503 12.4294 3.80156 11.6856 4.69841C10.9528 5.59526 9.87005 6.10931 8.71071 6.10931Z" fill=white />
                        </svg>

                    </a>
                    <a href="#" class="download__link download__link--android js-link leave-link">

                        <svg width=16px height=19px viewBox="0 0 16 19" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                            <title>Android-logo (1)</title>
                            <desc>Created with Sketch.</desc>
                            <g id=Page-1 stroke=none stroke-width=1 fill=none fill-rule=evenodd opacity=0.8>
                                <g id=Android-logo-(1) transform="translate(0.000000, -1.000000)" fill=#FFFFFF>
                                    <g id=fill>
                                        <path d="M2,15.65 C2.56,15.65 3.07,15.42 3.43,15.05 C3.6,15.34 3.8,15.55 4,15.69 L4,17.96 C4,19.08 4.9,19.99 6,19.99 C7.1,19.99 8,19.08 8,17.96 L8,17.99 C8,19.09 8.9,19.99 10,19.99 C11.1,19.99 12,19.08 12,17.96 L12,15.69 C12.21,15.55 12.4,15.35 12.57,15.05 C12.93,15.42 13.44,15.65 14,15.65 C15.1,15.65 16,14.75 16,13.65 L16,8.98 C16,7.88 15.1,6.98 14,6.98 C13.62,6.98 13.26,7.09 12.96,7.27 C12.98,7.18 13,7.08 13,6.98 C13,5.69 12.54,4.45 11.75,3.49 L11.95,3.28 C12.44,2.75 12.44,1.929999 11.95,1.409999 C11.45,0.869999 10.52,0.869999 10.02,1.409999 L9.51,1.949999 C8.53,1.609999 7.44,1.619999 6.47,1.949999 L5.96,1.399999 C5.46,0.859999 4.53,0.859999 4.02,1.399999 C3.53,1.919999 3.53,2.74 4.02,3.27 L4.24,3.5 C3.44,4.46 3,5.68 3,6.98 C3,7.08 3.02,7.18 3.04,7.27 C2.74,7.09 2.38,6.98 2,6.98 C0.9,6.98 0,7.88 0,8.98 L0,13.65 C0,14.75 0.9,15.65 2,15.65 Z" id=Path></path>
                                    </g>
                                    <g id=stroke>
                                        <path d="M2,15.65 C2.56,15.65 3.07,15.42 3.43,15.05 C3.6,15.34 3.8,15.55 4,15.69 L4,17.96 C4,19.08 4.9,19.99 6,19.99 C7.1,19.99 8,19.08 8,17.96 L8,17.99 C8,19.09 8.9,19.99 10,19.99 C11.1,19.99 12,19.08 12,17.96 L12,15.69 C12.21,15.55 12.4,15.35 12.57,15.05 C12.93,15.42 13.44,15.65 14,15.65 C15.1,15.65 16,14.75 16,13.65 L16,8.98 C16,7.88 15.1,6.98 14,6.98 C13.62,6.98 13.26,7.09 12.96,7.27 C12.98,7.18 13,7.08 13,6.98 C13,5.69 12.54,4.45 11.75,3.49 L11.95,3.28 C12.44,2.75 12.44,1.929999 11.95,1.409999 C11.45,0.869999 10.52,0.869999 10.02,1.409999 L9.51,1.949999 C8.53,1.609999 7.44,1.619999 6.47,1.949999 L5.96,1.399999 C5.46,0.859999 4.53,0.859999 4.02,1.399999 C3.53,1.919999 3.53,2.74 4.02,3.27 L4.24,3.5 C3.44,4.46 3,5.68 3,6.98 C3,7.08 3.02,7.18 3.04,7.27 C2.74,7.09 2.38,6.98 2,6.98 C0.9,6.98 0,7.88 0,8.98 L0,13.65 C0,14.75 0.9,15.65 2,15.65 Z M13,8.98 C13,8.43 13.45,7.98 14,7.98 C14.55,7.98 15,8.43 15,8.98 L15,13.65 C15,14.2 14.55,14.65 14,14.65 C13.45,14.65 13,14.2 13,13.65 L13,8.98 Z M5.63,3.53 L4.76,2.6 C4.63,2.46 4.63,2.24 4.76,2.09 C4.89,1.949999 5.1,1.949999 5.23,2.09 L6.22,3.15 C6.76,2.86 7.36,2.7 8,2.7 C8.63,2.7 9.23,2.86 9.76,3.15 L10.75,2.09 C10.88,1.949999 11.09,1.949999 11.22,2.09 C11.35,2.23 11.35,2.45 11.22,2.6 L10.35,3.53 C11.35,4.3 12,5.56 12,6.98 L4,6.98 C4,5.56 4.65,4.31 5.63,3.53 Z M4,7.98 L12,7.98 L12,12.98 C12,13.35 12,14.98 11,14.98 L11,17.95 C11,18.5 10.55,18.98 10,18.98 C9.45,18.98 9,18.53 9,17.98 L9,14.98 L7,14.98 L7,17.95 C7,18.5 6.55,18.98 6,18.98 C5.45,18.98 5,18.5 5,17.95 L5,14.98 C4,14.98 4,13.35 4,12.98 L4,7.98 Z M1,8.98 C1,8.43 1.45,7.98 2,7.98 C2.55,7.98 3,8.43 3,8.98 L3,13.65 C3,14.2 2.55,14.65 2,14.65 C1.45,14.65 1,14.2 1,13.65 L1,8.98 Z" id=Shape fill-rule=nonzero></path>
                                        <polygon id=Path points="7 4.98 6 4.98 6 5.98 7 5.98"></polygon>
                                        <polygon id=Path points="10 4.98 9 4.98 9 5.98 10 5.98"></polygon>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="burger-menu s-visible">
                <a href=# id=mob-menu-link class="burger-menu__link menu-link up-text hamburger">
                    <span class="burger-menu__text link-text up-text">menu</span>
                    <div class=burger-button>
                        <span class=burger-button-dot></span>
                        <span class=burger-button-dot></span>
                        <span class=burger-button-dot></span>
                        <span class=burger-button-dot></span>
                        <span class=burger-button-dot></span>
                        <span class=burger-button-dot></span>
                        <span class=burger-button-dot></span>
                        <span class=burger-button-dot></span>
                        <span class=burger-button-dot></span>
                    </div>
                </a>
            </div>
        </div>

    </header>

        @yield('content')

        <footer id=footer class=footer>

        <div class="container footer__container">
            <div class=socials__wrap>
                <div class=social-state-one__wrap>
                    <button class="socials__link socials__link--all js-link">

                        <svg width=15px height=16px viewBox="0 0 15 16" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <g id=Symbols stroke=none stroke-width=1 fill=none fill-rule=evenodd opacity=0.600000012>
                                <g id=main-screen-1440-/-default transform="translate(-36.000000, -760.000000)" fill-rule=nonzero stroke=#FFFFFF>
                                    <g id=social transform="translate(36.000000, 760.000000)">
                                        <circle id=Oval cx=3 cy=8 r=2.5></circle>
                                        <circle id=Oval-Copy cx=12 cy=3 r=2.5></circle>
                                        <circle id=Oval-Copy-2 cx=12 cy=13 r=2.5></circle>
                                        <path d="M5.45686417,6.53665526 L9.39949832,4.26975511" id=Path-2></path>
                                        <path d="M5.55754322,11.5 L9.5,9.43066406" id=Path-2-Copy transform="translate(7.528772, 10.465332) scale(1, -1) translate(-7.528772, -10.465332) "></path>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <span class="link-text up-text">{{__("social")}}</span>
                    </button>

                    <div class=copy_wrap>
                        <ul class=copy_wrap_list>
                            <li class=copy_wrap_item><a href="{{route('privacy')}}" class="small-text up-text js-link leave-link">Your Privacy</a></li>
                            <li class="copy_wrap_item small-text up-text">{{__("2019 Big Elaboration, Inc.")}}</li>
                        </ul>
                    </div>

                </div>
                <ul class="socials__link socials__link--list">
                    <li class="social__item social__item--facebook">
                        <a href="#" class="social__item--link js-link" data-enter=hide target=_blank rel="nofollow noreferrer noopener">

                        <svg width=8px height=16px viewBox="0 0 8 16" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <g id=Symbols stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                <g id=main-screen-1440-/-active transform="translate(-36.000000, -760.000000)" fill=#C3A6FF fill-rule=nonzero>
                                    <g id=social transform="translate(36.000000, 748.000000)">
                                        <g id=facebook-letter-logo transform="translate(0.000000, 12.000000)">
                                            <path d="M1.61058005,3.09230626 C1.61058005,3.49468213 1.61058005,5.29065429 1.61058005,5.29065429 L0,5.29065429 L0,7.97880278 L1.61058005,7.97880278 L1.61058005,15.9670348 L4.91905336,15.9670348 L4.91905336,7.97902552 L7.13919258,7.97902552 C7.13919258,7.97902552 7.34711833,6.69007889 7.44790719,5.28074246 C7.158942,5.28074246 4.93156381,5.28074246 4.93156381,5.28074246 C4.93156381,5.28074246 4.93156381,3.71686311 4.93156381,3.4427471 C4.93156381,3.16803712 5.2922877,2.79851508 5.64881671,2.79851508 C6.00467749,2.79851508 6.75574942,2.79851508 7.45139675,2.79851508 C7.45139675,2.43251972 7.45139675,1.16792575 7.45139675,0 C6.52272854,0 5.46620882,0 5.00050116,0 C1.52879814,-0.000185614849 1.61058005,2.69063573 1.61058005,3.09230626 Z" id=Facebook></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        </a>
                    </li>
                    <li class="social__item social__item--instagram">
                        <a href="#" class="social__item--link js-link" data-enter=hide target=_blank rel="nofollow noreferrer noopener">

                        <svg width=16px height=16px viewBox="0 0 16 16" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <g id=Symbols stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                <g id=main-screen-1440-/-active transform="translate(-94.000000, -760.000000)" fill=#C3A6FF fill-rule=nonzero>
                                    <g id=social transform="translate(36.000000, 748.000000)">
                                        <g id=instagram-hover transform="translate(58.000000, 12.000000)">
                                            <path d="M11,0 C13.761,0 16,2.239 16,5 L16,11 C16,13.761 13.761,16 11,16 L5,16 C2.239,16 0,13.761 0,11 L0,5 C0,2.239 2.239,0 5,0 L11,0 Z M8,4 C5.791,4 4,5.791 4,8 C4,10.209 5.791,12 8,12 C10.209,12 12,10.209 12,8 C12,5.791 10.209,4 8,4 Z M8,10.5 C6.622,10.5 5.5,9.378 5.5,8 C5.5,6.621 6.622,5.5 8,5.5 C9.378,5.5 10.5,6.621 10.5,8 C10.5,9.378 9.378,10.5 8,10.5 Z M12.767,4.167 C13.3192847,4.167 13.767,3.71928475 13.767,3.167 C13.767,2.61471525 13.3192847,2.167 12.767,2.167 C12.2147153,2.167 11.767,2.61471525 11.767,3.167 C11.767,3.71928475 12.2147153,4.167 12.767,4.167 Z" id=Combined-Shape></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        </a>
                    </li>
                    <li class="social__item social__item--twitter">
                        <a href="#" class="social__item--link js-link" data-enter=hide target=_blank rel="nofollow noreferrer noopener">

                            <svg width=19px height=15px viewBox="0 0 19 15" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <g id=Symbols stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                    <g id=main-screen-1440-/-active transform="translate(-160.000000, -761.000000)" fill=#C3A6FF fill-rule=nonzero>
                                        <g id=social transform="translate(36.000000, 748.000000)">
                                            <g id=twitter-logo-silhouette transform="translate(124.000000, 13.000000)">
                                                <path d="M18.999969,1.77159639 C18.3006634,2.07222892 17.5503186,2.27611446 16.7619428,2.36713855 C17.5669281,1.89945783 18.1831242,1.1576506 18.4751716,0.276445783 C17.7200768,0.709548193 16.8865915,1.02403614 15.9984967,1.19451807 C15.2873317,0.458463855 14.275768,0 13.1537745,0 C11.0012484,0 9.25594935,1.69328313 9.25594935,3.78051205 C9.25594935,4.07653614 9.29037908,4.36566265 9.35687908,4.64213855 C6.118,4.48430723 3.24598856,2.97879518 1.32379085,0.691114458 C0.98778268,1.24864458 0.796633987,1.89828313 0.796633987,2.59174699 C0.796633987,3.90376506 1.4852598,5.06141566 2.53007353,5.73870482 C1.89130719,5.71798193 1.29054085,5.5475 0.764594771,5.26412651 L0.764594771,5.31135542 C0.764594771,7.14286145 2.10859641,8.67141566 3.89068464,9.01930723 C3.56417647,9.10454819 3.21987908,9.15177711 2.86369118,9.15177711 C2.61200327,9.15177711 2.36860458,9.12759036 2.12995588,9.08150602 C2.62622222,10.5847289 4.06522386,11.6778614 5.7701634,11.7078313 C4.4368415,12.721506 2.75565196,13.3239458 0.929633987,13.3239458 C0.61501634,13.3239458 0.305117647,13.305512 0,13.2721084 C1.72511928,14.3468373 3.77317647,14.9734639 5.97438235,14.9734639 C13.1443366,14.9734639 17.0635523,9.21168675 17.0635523,4.21478916 L17.050482,3.72524096 C17.8162565,3.19533133 18.4787418,2.52954819 18.999969,1.77159639 Z" id=Shape></path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </a>
                    </li>
                    <li class="social__item social__item--medium">
                        <a href="#" class="social__item--link js-link" data-enter=hide target=_blank rel="nofollow noreferrer noopener">

                            <svg width=16px height=16px viewBox="0 0 16 16" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <g id=Symbols stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                    <g id=main-screen-1440-/-active transform="translate(-393.000000, -760.000000)" fill=#C3A6FF>
                                        <g id=social transform="translate(36.000000, 748.000000)">
                                            <g id=medium-seeklogo.com transform="translate(357.000000, 12.000000)">
                                                <path d="M0,0 L16,0 L16,16 L0,16 L0,0 Z M3.81818095,5.35103634 L3.81818095,10.1018104 C3.84741611,10.2731885 3.79339176,10.4482524 3.67265676,10.5733762 L2.54358974,11.9429235 L2.54358974,12.1235231 L5.74512197,12.1235231 L5.74512197,11.9429235 L4.61605497,10.5733762 C4.49443446,10.4484595 4.43706484,10.2745492 4.46049462,10.1018104 L4.46049462,5.99316843 L7.27061694,12.1235231 L7.59679188,12.1235231 L10.0104862,5.99316843 L10.0104862,10.8793923 C10.0104862,11.0098253 10.0104862,11.0349086 9.925179,11.1201918 L9.05705188,11.9629901 L9.05705188,12.1435898 L13.2722354,12.1435898 L13.2722354,11.9629901 L12.4342168,11.1402584 C12.3602399,11.0838689 12.3235436,10.9911888 12.3388734,10.8994589 L12.3388734,4.85438731 C12.3235436,4.76265733 12.3602399,4.66997725 12.4342168,4.61358778 L13.2923077,3.79085606 L13.2923077,3.61025641 L10.321607,3.61025641 L8.203979,8.89279606 L5.79530274,3.61025641 L2.67907779,3.61025641 L2.67907779,3.79085606 L3.68269291,4.99987036 C3.78121846,5.08867536 3.83153541,5.21908982 3.81818095,5.35103634 Z" id=Rectangle-2></path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </a>
                    </li>
                    <li class="social__item social__item--meetup">
                        <a href="#" class="social__item--link js-link" data-enter=hide target=_blank rel="nofollow noreferrer noopener">

                        <svg width=33px height=14px viewBox="0 0 33 14" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <g id=Symbols stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                <g id=main-screen-1440-/-active transform="translate(-229.000000, -762.000000)" fill=#C3A6FF>
                                    <g id=social transform="translate(36.000000, 748.000000)">
                                        <g id=meetup-logo-(1) transform="translate(193.000000, 14.000000)">
                                            <path d="M6.688,3.24966667 C6.618,3.009 6.41333333,2.70733333 6.199,2.63033333 C5.849,2.50433333 5.44133333,2.51633333 5.05666667,2.51933333 C4.92033333,2.52033333 4.70933333,2.63433333 4.66066667,2.75066667 C4.39233333,3.39366667 4.12333333,4.04133333 3.93266667,4.70933333 C3.73133333,5.415 3.619,6.14566667 3.455,6.91766667 C3.38133333,6.794 3.323,6.70533333 3.27333333,6.61166667 C2.90933333,5.91466667 2.54966667,5.217 2.185,4.52133333 C1.818,3.82166667 1.61233333,3.71766667 0.823666667,3.84766667 C0.627666667,3.87933333 0.427666667,3.926 0.247333333,4.007 C0.157666667,4.04766667 0.0536666667,4.17766667 0.0506666667,4.27033333 C-0.006,5.88266667 -0.067,7.49366667 0.179,9.10033333 C0.349,10.2146667 0.555666667,10.3583333 1.62066667,9.95066667 C1.67366667,9.93 1.72233333,9.9 1.77533333,9.87766667 C2.016,9.77633333 2.12566667,9.61433333 2.08233333,9.339 C2.006,8.863 1.95233333,8.38333333 1.89066667,7.905 C1.91266667,7.90333333 1.93533333,7.90133333 1.95766667,7.89966667 C2.27866667,8.42133333 2.60966667,8.937 2.918,9.46633333 C3.17066667,9.89733333 3.46633333,10.179 4.019,10.0143333 C4.23333333,9.95033333 4.481,9.99366667 4.695,9.92833333 C4.84366667,9.883 5.01666667,9.762 5.08166667,9.628 C5.171,9.44233333 5.19333333,9.214 5.20533333,9.00166667 C5.257,8.104 5.29266667,7.204 5.33966667,6.306 C5.34733333,6.14066667 5.38033333,5.97633333 5.40666667,5.77766667 C5.88866667,7.319 6.359,8.79566667 6.80733333,10.28 C6.88533333,10.539 6.974,10.6463333 7.25,10.5646667 C7.632,10.4526667 8.02,10.3633333 8.33533333,10.2826667 C7.989,8.755 7.65766667,7.26833333 7.31466667,5.78366667 C7.11966667,4.936 6.93,4.08466667 6.688,3.24966667 Z" id=Path></path>
                                            <path d="M32.4653333,5.06366667 C32.394,4.42 32.2076667,3.79666667 31.4253333,3.61533333 C31.1373333,3.548 30.8743333,3.34866667 30.586,3.30566667 C30.2806667,3.261 29.9116667,3.23633333 29.6506667,3.366 C28.6793333,3.85033333 28.0846667,4.69266667 27.8123333,5.71733333 C27.5643333,6.64933333 27.4356667,7.61333333 27.245,8.56133333 C27.224,8.67 27.1673333,8.817 27.0823333,8.861 C26.626,9.09866667 26.596,9.36566667 26.9673333,9.71633333 C27.0926667,9.83433333 27.1796667,10.0346667 27.2063333,10.2093333 C27.371,11.263 27.509,12.3203333 27.6713333,13.375 C27.6893333,13.495 27.7976667,13.6953333 27.8713333,13.699 C28.3073333,13.7176667 28.744,13.6886667 29.1886667,13.6756667 C28.7606667,11.9233333 28.6553333,10.8016667 28.8343333,10.0236667 C29.1076667,9.924 29.413,9.88133333 29.6326667,9.72066667 C30.2156667,9.29233333 30.7676667,8.82033333 31.3206667,8.353 C32.3506667,7.48566667 32.6046667,6.32666667 32.4653333,5.06366667 Z M28.8823333,8.528 C29.1926667,7.02666667 29.3793333,5.52133333 30.6326667,4.43433333 C31.2346667,5.98166667 30.4696667,7.707 28.8823333,8.528 Z" id=Shape fill-rule=nonzero></path>
                                            <path d="M11.697,9.63766667 C11.0793333,9.919 10.61,9.80966667 10.256,9.263 C9.891,8.70033333 9.92166667,8.07466667 10.0056667,7.44566667 C10.705,7.48133333 11.3553333,7.51466667 12.0066667,7.55 C12.2553333,7.56366667 12.3263333,7.44233333 12.3836667,7.20433333 C12.6093333,6.26233333 12.4243333,5.329 12.372,4.393 C12.3426667,3.85833333 12.2183333,3.414 11.546,3.29766667 C10.9393333,3.193 10.3973333,3.00233333 9.83933333,3.50433333 C9.27966667,4.00933333 8.75433333,4.49133333 8.53966667,5.25366667 C8.27166667,6.20133333 8.301,7.16433333 8.28566667,8.12933333 C8.27333333,8.94933333 8.56566667,9.54366667 9.296,10.0103333 C10.3043333,10.6543333 12.2466667,10.701 13.2566667,9.98 C12.8143333,9.51933333 12.2373333,9.392 11.697,9.63766667 Z M10.204,5.88366667 C10.3,5.53233333 10.462,5.19966667 10.5946667,4.85833333 C10.6433333,4.87033333 10.691,4.88233333 10.7386667,4.89433333 C10.7506667,5.419 10.7623333,5.944 10.774,6.50333333 C10.063,6.47233333 10.045,6.46733333 10.204,5.88366667 Z" id=Shape fill-rule=nonzero></path>
                                            <path d="M17.362,9.20933333 C17.243,9.17866667 17.0863333,9.19366667 16.9743333,9.247 C16.718,9.37066667 16.4946667,9.57266667 16.2296667,9.666 C15.79,9.82066667 15.351,9.803 15.0116667,9.41233333 C14.6006667,8.93933333 14.266,8.43933333 14.499,7.72833333 C15.2273333,7.82766667 15.9486667,8.084 16.607,7.669 C16.6726667,6.90533333 16.7316667,6.183 16.7993333,5.463 C16.8583333,4.85633333 16.9473333,4.25233333 16.9786667,3.64466667 C16.9856667,3.5 16.8466667,3.27733333 16.716,3.20966667 C16.3633333,3.031 15.986,2.89033333 15.6036667,2.78666667 C15.4276667,2.73866667 15.133,2.73666667 15.0393333,2.843 C14.525,3.427 13.9256667,3.99 13.6033333,4.67666667 C13.1513333,5.63966667 12.8796667,6.68466667 12.9166667,7.79566667 C12.9553333,8.945 13.6133333,9.90633333 14.718,10.188 C15.0203333,10.2643333 15.343,10.2603333 15.649,10.327 C16.4663333,10.5036667 17.148,10.173 17.8313333,9.824 C17.827,9.469 17.65,9.28133333 17.362,9.20933333 Z M15.295,4.833 C15.3413333,4.84333333 15.387,4.854 15.4333333,4.86533333 C15.4263333,5.38133333 15.4453333,5.89933333 15.405,6.41266667 C15.3576667,7.026 15.314,7.03766667 14.591,6.81966667 C14.8263333,6.15366667 15.0613333,5.493 15.295,4.833 Z" id=Shape fill-rule=nonzero></path>
                                            <path d="M20.5536667,6.13366667 C20.5463333,6.225 20.4453333,6.308 20.387,6.39533333 C20.3456667,6.28666667 20.2663333,6.17633333 20.2716667,6.07 C20.2846667,5.793 20.3676667,5.518 20.3643333,5.243 C20.3483333,3.98866667 20.2726667,2.734 20.3003333,1.48133333 C20.318,0.666 20.3563333,0.625666667 19.596,0.284 C18.8546667,-0.049 18.505,0.142333333 18.4846667,0.958333333 C18.4653333,1.728 18.4953333,2.501 18.554,3.26866667 C18.6366667,4.33166667 18.769,5.391 18.8856667,6.515 C18.573,6.50133333 18.2923333,6.49133333 18.0136667,6.475 C17.6806667,6.45533333 17.4496667,6.57266667 17.4233333,6.93333333 C17.3946667,7.29833333 17.619,7.395 17.9366667,7.40066667 C18.2753333,7.40733333 18.6126667,7.436 18.9793333,7.45666667 C19.0263333,8.039 19.0746667,8.57666667 19.1113333,9.11466667 C19.1476667,9.64866667 19.1723333,10.183 19.203,10.745 C19.6153333,10.6763333 19.9713333,10.6173333 20.3433333,10.5556667 C20.3213333,10.0276667 20.2946667,9.54433333 20.283,9.05966667 C20.271,8.57666667 20.296,8.09166667 20.262,7.61 C20.2413333,7.336 20.3896667,7.348 20.5666667,7.32533333 C21.069,7.262 21.5693333,7.18866667 22.0696667,7.119 C22.0926667,7.06833333 22.1166667,7.01733333 22.1386667,6.96733333 C21.7366667,6.60833333 21.35,6.22866667 20.9233333,5.90166667 C20.7503333,5.76966667 20.5766667,5.86866667 20.5536667,6.13366667 Z" id=Path></path>
                                            <path d="M26.5526667,5.606 C26.3566667,5.52133333 26.1703333,5.40733333 25.9653333,5.35133333 C25.83,5.31366667 25.6103333,5.29733333 25.5383333,5.37433333 C25.4176667,5.50366667 25.3373333,5.71166667 25.3266667,5.89233333 C25.3113333,6.142 25.3996667,6.39633333 25.401,6.649 C25.4053333,7.73166667 25.14,8.71033333 24.2213333,9.49433333 C24.1773333,9.35166667 24.1283333,9.26 24.122,9.16566667 C24.068,8.311 23.9896667,7.45633333 23.9813333,6.60033333 C23.9736667,5.76266667 23.9903333,5.73833333 23.203,5.392 C23.085,5.34033333 22.9703333,5.277 22.849,5.23766667 C22.444,5.107 22.245,5.23866667 22.231,5.66266667 C22.22,5.96133333 22.21,6.267 22.2583333,6.56 C22.4293333,7.58233333 22.5996667,8.606 22.8236667,9.61766667 C22.908,10.0046667 23.1923333,10.3193333 23.6363333,10.3306667 C24.0186667,10.3396667 24.412,10.3153333 24.7826667,10.2283333 C24.9903333,10.18 25.1873333,10.0006667 25.342,9.836 C25.569,9.594 25.7413333,9.30266667 25.9576667,9.05066667 C26.562,8.34433333 27.0553333,7.60633333 27.0896667,6.61966667 C27.108,6.137 27.0073333,5.801 26.5526667,5.606 Z" id=Path></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

    </footer>

        <div id=mobile-menu>
            <div class=mobile-menu-bg id=mobile-menu-bg>
                <div class=waves_wrap>

                    <svg width=768px height=249px viewBox="0 0 768 249" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <defs>
                            <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=waves-linearGradient-1>
                                <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                                <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                            </linearGradient>
                            <path d="M768.226952,307.512465 L1.68403639,307.512465 L0.974335439,178.580503 C225.742085,49.0218441 481.492957,6.11682642 768.226952,49.8654497 C769.257683,54.6763271 769.257683,140.558666 768.226952,307.512465 Z" id=path-2></path>
                        </defs>
                        <g id=768 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                            <g id=768---menu-copy transform="translate(0.000000, -716.000000)">
                                <g id=bg transform="translate(-1.000000, 0.000000)">
                                    <g id=Clouds transform="translate(385.000000, 870.000000) rotate(-180.000000) translate(-385.000000, -870.000000) translate(0.000000, 716.000000)">
                                        <mask id=mask-3 fill=white>
                                            <use xlink:href=#path-2></use>
                                        </mask>
                                        <use id=Shape-1 fill=url(#waves-linearGradient-1) fill-rule=nonzero transform="translate(384.987168, 170.406233) scale(1, -1) translate(-384.987168, -170.406233) " xlink:href=#path-2></use>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>

                    <svg width=768px height=270px viewBox="0 0 768 270" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <defs>
                            <linearGradient x1=50% y1=89.1572565% x2=50% y2=21.9809334% id=waves-linearGradient-2>
                                <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                                <stop stop-color=#FFFFFF stop-opacity=0.1 offset=100%></stop>
                            </linearGradient>
                        </defs>
                        <g id=768 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                            <g id=768---menu-copy transform="translate(0.000000, -721.000000)" fill=url(#waves-linearGradient-2) fill-rule=nonzero>
                                <g id=bg transform="translate(-1.000000, 0.000000)">
                                    <g id=Clouds transform="translate(385.000000, 870.000000) rotate(-180.000000) translate(-385.000000, -870.000000) translate(0.000000, 716.000000)">
                                        <path d="M768.226952,303.03 L0.615474886,303.03 L0.353600869,107.351444 C74.548422,66.4856818 130.763888,37.9855663 169,21.8510977 C332.327261,-47.0680067 519.373575,66.3624219 768.226952,96.6194559 C769.257683,101.41654 769.257683,170.220055 768.226952,303.03 Z" id=Shape-2 transform="translate(384.676800, 151.515000) scale(1, -1) translate(-384.676800, -151.515000) "></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="container header__container mobile-menu__header__container">
                <div class=content__left>
                    <div class="logo__item logo__item--elaboration">
                        <a href=/ class=logo>
                            <img src=assets/img/logo.svg alt="elaboration logo" class=logo__image>
                        </a>
                    </div>
                    <a href=mailto:support@bigelaboration.com rel="nofollow noreferrer noopener" class="link-text up-text email js-link">

                        <svg width=18px height=13px viewBox="0 0 18 13" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <g id=Symbols stroke=none stroke-width=1 fill=none fill-rule=evenodd opacity=0.800000012>
                                <g id=main-screen-1920/-default transform="translate(-1700.000000, -37.000000)" fill-rule=nonzero stroke=#FFFFFF>
                                    <g id=main-screen-elements transform="translate(48.000000, 27.000000)">
                                        <g id=header>
                                            <g id=right transform="translate(1086.000000, 0.000000)">
                                                <g id=mail transform="translate(566.000000, 10.000000)">
                                                    <rect id=Rectangle x=0.5 y=0.5 width=17 height=12 rx=2></rect>
                                                    <path d="M0.720991041,2.69934946 L7.21576177,7.22805702 C8.24675027,7.94695005 9.61657431,7.94695005 10.6475628,7.22805702 L17.1423335,2.69934946" id=Path-7 stroke-linecap=round></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <span class="link-text up-text">{{__("support@bigelaboration.com")}}</span>
                    </a>
                </div>
                <div class=content__right>
                    <div class="download-icons__wrap xxs-hidden">
                        <a href="#" target=_blank rel="nofollow noreferrer noopener" class="download__link download__link--apple js-link">
                            <svg width=16 height=21 viewBox="0 0 16 21" fill=none xmlns="http://www.w3.org/2000/svg">
                                <path d="M6.65451 20.2011C5.33111 20.2011 4.42332 18.9215 3.76709 17.9699C2.46556 16.0778 1.12029 12.2389 2.74993 9.43893C3.49366 8.1046 4.9155 7.24056 6.46858 7.19681C7.15762 7.17493 7.81386 7.43743 8.3279 7.64523C8.63414 7.76554 8.94039 7.88585 9.11538 7.88585C9.25757 7.88585 9.57474 7.75461 9.85911 7.64523C10.5044 7.39368 11.3247 7.06556 12.1997 7.164C13.4903 7.20775 14.7371 7.87491 15.4918 8.9577C15.5793 9.07801 15.6121 9.23113 15.5793 9.38425C15.5465 9.53737 15.459 9.65768 15.3277 9.74518C14.4309 10.292 13.884 11.2436 13.8731 12.2935C13.8731 13.4529 14.573 14.5138 15.6668 14.9622C15.9293 15.0716 16.0605 15.356 15.973 15.6294C15.7105 16.4825 15.3058 17.2918 14.7918 18.0246C14.1902 18.9215 13.3481 20.1683 11.9372 20.1902C11.3247 20.2011 10.8763 20.0152 10.4935 19.8621C10.1325 19.709 9.8263 19.5777 9.32319 19.5777C8.77633 19.5777 8.44821 19.7199 8.07635 19.873C7.71542 20.0261 7.31074 20.1902 6.74201 20.2121C6.7092 20.2011 6.67639 20.2011 6.65451 20.2011ZM6.52327 8.29053C6.51233 8.29053 6.50139 8.29053 6.49045 8.29053C5.33111 8.32334 4.25927 8.97957 3.70147 9.98579C2.4765 12.0967 3.28585 15.356 4.66394 17.3575C5.32017 18.2981 5.98734 19.1184 6.68732 19.1184C7.05919 19.1074 7.31074 18.998 7.63886 18.8559C8.06541 18.6809 8.54665 18.4731 9.31225 18.4731C10.045 18.4731 10.5263 18.6809 10.9091 18.8449C11.2481 18.9871 11.5106 19.1074 11.9153 19.0855C12.7028 19.0746 13.2496 18.3418 13.8949 17.3903C14.2668 16.8653 14.5621 16.3184 14.7809 15.7278C13.5559 15.0169 12.7793 13.7044 12.7793 12.2717C12.7903 11.0576 13.3371 9.93111 14.2449 9.1655C13.6871 8.60771 12.9215 8.26865 12.1231 8.24678C11.4669 8.18116 10.8216 8.43271 10.2529 8.66239C9.8263 8.83739 9.45443 8.97957 9.10444 8.97957C8.72164 8.97957 8.3279 8.82645 7.91229 8.65146C7.46386 8.47646 6.98263 8.29053 6.52327 8.29053Z" fill=white />
                                <path d="M8.71071 6.10931C8.69977 6.10931 8.6779 6.10931 8.66696 6.10931C8.38259 6.10931 8.14197 5.87963 8.13104 5.59526C8.05448 4.4578 8.42634 3.36408 9.18101 2.51097C9.93568 1.65787 10.9638 1.12195 12.1012 1.00164C12.2434 0.990703 12.3965 1.03445 12.5059 1.13289C12.6153 1.23132 12.6918 1.36257 12.7028 1.51569C12.7903 2.67503 12.4294 3.80156 11.6856 4.69841C10.9528 5.59526 9.87005 6.10931 8.71071 6.10931ZM11.6091 2.21567C10.9856 2.39066 10.4278 2.74065 10.0013 3.23283C9.56381 3.725 9.30132 4.32655 9.23569 4.96091C9.85911 4.8406 10.4278 4.50154 10.8435 3.99843C10.8435 3.99843 10.8435 3.99843 10.8544 3.9875C11.27 3.47345 11.5325 2.86096 11.6091 2.21567Z" fill=white />
                                <path class=stroke d="M6.65451 20.2011C5.33111 20.2011 4.42332 18.9215 3.76709 17.9699C2.46556 16.0778 1.12029 12.2389 2.74993 9.43893C3.49366 8.1046 4.9155 7.24056 6.46858 7.19681C7.15762 7.17493 7.81385 7.43743 8.3279 7.64523C8.63414 7.76554 8.94039 7.88585 9.11538 7.88585C9.25756 7.88585 9.57474 7.75461 9.85911 7.64523C10.5044 7.39368 11.3247 7.06556 12.1997 7.164C13.4903 7.20775 14.7371 7.87492 15.4918 8.9577C15.5793 9.07801 15.6121 9.23113 15.5793 9.38425C15.5465 9.53737 15.459 9.65768 15.3277 9.74518C14.4309 10.292 13.884 11.2436 13.8731 12.2935C13.8731 13.4529 14.573 14.5138 15.6668 14.9622C15.9293 15.0716 16.0605 15.356 15.973 15.6294C15.7105 16.4825 15.3058 17.2918 14.7918 18.0246C14.1902 18.9215 13.3481 20.1683 11.9372 20.1902C11.3247 20.2011 10.8763 20.0152 10.4935 19.8621C10.1325 19.709 9.8263 19.5777 9.32319 19.5777C8.77633 19.5777 8.44821 19.7199 8.07635 19.873C7.71542 20.0261 7.31074 20.1902 6.74201 20.2121C6.7092 20.2011 6.67639 20.2011 6.65451 20.2011Z" fill=white />
                                <path class=stroke d="M8.71071 6.10931C8.69977 6.10931 8.6779 6.10931 8.66696 6.10931C8.38259 6.10931 8.14197 5.87963 8.13104 5.59526C8.05448 4.4578 8.42634 3.36408 9.18101 2.51097C9.93568 1.65787 10.9638 1.12195 12.1012 1.00164C12.2434 0.990703 12.3965 1.03445 12.5059 1.13289C12.6153 1.23132 12.6918 1.36257 12.7028 1.51569C12.7903 2.67503 12.4294 3.80156 11.6856 4.69841C10.9528 5.59526 9.87005 6.10931 8.71071 6.10931Z" fill=white />
                            </svg>

                        </a>
                        <a href=# class="download__link download__link--android js-link">

                            <svg width=16px height=19px viewBox="0 0 16 19" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                <title>Android-logo (1)</title>
                                <desc>Created with Sketch.</desc>
                                <g id=Page-1 stroke=none stroke-width=1 fill=none fill-rule=evenodd opacity=0.8>
                                    <g id=Android-logo-(1) transform="translate(0.000000, -1.000000)" fill=#FFFFFF>
                                        <g id=fill>
                                            <path d="M2,15.65 C2.56,15.65 3.07,15.42 3.43,15.05 C3.6,15.34 3.8,15.55 4,15.69 L4,17.96 C4,19.08 4.9,19.99 6,19.99 C7.1,19.99 8,19.08 8,17.96 L8,17.99 C8,19.09 8.9,19.99 10,19.99 C11.1,19.99 12,19.08 12,17.96 L12,15.69 C12.21,15.55 12.4,15.35 12.57,15.05 C12.93,15.42 13.44,15.65 14,15.65 C15.1,15.65 16,14.75 16,13.65 L16,8.98 C16,7.88 15.1,6.98 14,6.98 C13.62,6.98 13.26,7.09 12.96,7.27 C12.98,7.18 13,7.08 13,6.98 C13,5.69 12.54,4.45 11.75,3.49 L11.95,3.28 C12.44,2.75 12.44,1.929999 11.95,1.409999 C11.45,0.869999 10.52,0.869999 10.02,1.409999 L9.51,1.949999 C8.53,1.609999 7.44,1.619999 6.47,1.949999 L5.96,1.399999 C5.46,0.859999 4.53,0.859999 4.02,1.399999 C3.53,1.919999 3.53,2.74 4.02,3.27 L4.24,3.5 C3.44,4.46 3,5.68 3,6.98 C3,7.08 3.02,7.18 3.04,7.27 C2.74,7.09 2.38,6.98 2,6.98 C0.9,6.98 0,7.88 0,8.98 L0,13.65 C0,14.75 0.9,15.65 2,15.65 Z" id=Path></path>
                                        </g>
                                        <g id=stroke>
                                            <path d="M2,15.65 C2.56,15.65 3.07,15.42 3.43,15.05 C3.6,15.34 3.8,15.55 4,15.69 L4,17.96 C4,19.08 4.9,19.99 6,19.99 C7.1,19.99 8,19.08 8,17.96 L8,17.99 C8,19.09 8.9,19.99 10,19.99 C11.1,19.99 12,19.08 12,17.96 L12,15.69 C12.21,15.55 12.4,15.35 12.57,15.05 C12.93,15.42 13.44,15.65 14,15.65 C15.1,15.65 16,14.75 16,13.65 L16,8.98 C16,7.88 15.1,6.98 14,6.98 C13.62,6.98 13.26,7.09 12.96,7.27 C12.98,7.18 13,7.08 13,6.98 C13,5.69 12.54,4.45 11.75,3.49 L11.95,3.28 C12.44,2.75 12.44,1.929999 11.95,1.409999 C11.45,0.869999 10.52,0.869999 10.02,1.409999 L9.51,1.949999 C8.53,1.609999 7.44,1.619999 6.47,1.949999 L5.96,1.399999 C5.46,0.859999 4.53,0.859999 4.02,1.399999 C3.53,1.919999 3.53,2.74 4.02,3.27 L4.24,3.5 C3.44,4.46 3,5.68 3,6.98 C3,7.08 3.02,7.18 3.04,7.27 C2.74,7.09 2.38,6.98 2,6.98 C0.9,6.98 0,7.88 0,8.98 L0,13.65 C0,14.75 0.9,15.65 2,15.65 Z M13,8.98 C13,8.43 13.45,7.98 14,7.98 C14.55,7.98 15,8.43 15,8.98 L15,13.65 C15,14.2 14.55,14.65 14,14.65 C13.45,14.65 13,14.2 13,13.65 L13,8.98 Z M5.63,3.53 L4.76,2.6 C4.63,2.46 4.63,2.24 4.76,2.09 C4.89,1.949999 5.1,1.949999 5.23,2.09 L6.22,3.15 C6.76,2.86 7.36,2.7 8,2.7 C8.63,2.7 9.23,2.86 9.76,3.15 L10.75,2.09 C10.88,1.949999 11.09,1.949999 11.22,2.09 C11.35,2.23 11.35,2.45 11.22,2.6 L10.35,3.53 C11.35,4.3 12,5.56 12,6.98 L4,6.98 C4,5.56 4.65,4.31 5.63,3.53 Z M4,7.98 L12,7.98 L12,12.98 C12,13.35 12,14.98 11,14.98 L11,17.95 C11,18.5 10.55,18.98 10,18.98 C9.45,18.98 9,18.53 9,17.98 L9,14.98 L7,14.98 L7,17.95 C7,18.5 6.55,18.98 6,18.98 C5.45,18.98 5,18.5 5,17.95 L5,14.98 C4,14.98 4,13.35 4,12.98 L4,7.98 Z M1,8.98 C1,8.43 1.45,7.98 2,7.98 C2.55,7.98 3,8.43 3,8.98 L3,13.65 C3,14.2 2.55,14.65 2,14.65 C1.45,14.65 1,14.2 1,13.65 L1,8.98 Z" id=Shape fill-rule=nonzero></path>
                                            <polygon id=Path points="7 4.98 6 4.98 6 5.98 7 5.98"></polygon>
                                            <polygon id=Path points="10 4.98 9 4.98 9 5.98 10 5.98"></polygon>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </a>
                    </div>
                    <div class=close-btn-wrap id=close-mob-menu>
                        <a href=# class="burger-menu__link menu-link up-text hamburger">
                            <span class="burger-menu__text link-text up-text">{{__("close")}}</span>
                            <div class=burger-cross></div>
                        </a>
                    </div>
                </div>
            </div>

            <ul id=mobile-menu-list class=mobile-menu-links-list>
                <li class=mobile-menu-link-wrap>
                    <a href="{{route('coaching')}}" class="link-text mobile-menu-link">{{__("coaching")}}</a>
                </li>
                <li class=mobile-menu-link-wrap>
                    <a href="{{route('education')}}" class="link-text mobile-menu-link">{{__("education")}}</a>
                </li>
                <li class=mobile-menu-link-wrap>
                    <a href="{{route('developers')}}" class="link-text mobile-menu-link">{{__("developers")}}</a>
                </li>
                <li class=mobile-menu-link-wrap>
                    <a href="{{route('about')}}" class="link-text mobile-menu-link">{{__("about")}}</a>
                </li>
                <li class=mobile-menu-link-wrap>
                    <a href="{{route('blog')}}" rel="nofollow noreferrer noopener" class="link-text mobile-menu-link">{{__("blog")}}</a>
                </li>
            </ul>

            <div class="container footer__container mobile-menu__footer__container">
                <div class="contact__wrap xxs-visible">
                    <div class=mailto__wrap>
                        <a href=mailto:support@bigelaboration.com rel="nofollow noreferrer noopener" class="link-text up-text email">

                            <svg width=18px height=13px viewBox="0 0 18 13" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <g id=Symbols stroke=none stroke-width=1 fill=none fill-rule=evenodd opacity=0.800000012>
                                    <g id=main-screen-1920/-default transform="translate(-1700.000000, -37.000000)" fill-rule=nonzero stroke=#FFFFFF>
                                        <g id=main-screen-elements transform="translate(48.000000, 27.000000)">
                                            <g id=header>
                                                <g id=right transform="translate(1086.000000, 0.000000)">
                                                    <g id=mail transform="translate(566.000000, 10.000000)">
                                                        <rect id=Rectangle x=0.5 y=0.5 width=17 height=12 rx=2></rect>
                                                        <path d="M0.720991041,2.69934946 L7.21576177,7.22805702 C8.24675027,7.94695005 9.61657431,7.94695005 10.6475628,7.22805702 L17.1423335,2.69934946" id=Path-7 stroke-linecap=round></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <span class="link-text up-text">{{__("support@bigelaboration.com")}}</span>
                        </a>
                    </div>
                    <div class=download-icons__wrap>
                        <a href="#" target=_blank rel="nofollow noreferrer noopener" class="download__link download__link--apple js-link">
                            <svg width=16 height=21 viewBox="0 0 16 21" fill=none xmlns="http://www.w3.org/2000/svg">
                                <path d="M6.65451 20.2011C5.33111 20.2011 4.42332 18.9215 3.76709 17.9699C2.46556 16.0778 1.12029 12.2389 2.74993 9.43893C3.49366 8.1046 4.9155 7.24056 6.46858 7.19681C7.15762 7.17493 7.81386 7.43743 8.3279 7.64523C8.63414 7.76554 8.94039 7.88585 9.11538 7.88585C9.25757 7.88585 9.57474 7.75461 9.85911 7.64523C10.5044 7.39368 11.3247 7.06556 12.1997 7.164C13.4903 7.20775 14.7371 7.87491 15.4918 8.9577C15.5793 9.07801 15.6121 9.23113 15.5793 9.38425C15.5465 9.53737 15.459 9.65768 15.3277 9.74518C14.4309 10.292 13.884 11.2436 13.8731 12.2935C13.8731 13.4529 14.573 14.5138 15.6668 14.9622C15.9293 15.0716 16.0605 15.356 15.973 15.6294C15.7105 16.4825 15.3058 17.2918 14.7918 18.0246C14.1902 18.9215 13.3481 20.1683 11.9372 20.1902C11.3247 20.2011 10.8763 20.0152 10.4935 19.8621C10.1325 19.709 9.8263 19.5777 9.32319 19.5777C8.77633 19.5777 8.44821 19.7199 8.07635 19.873C7.71542 20.0261 7.31074 20.1902 6.74201 20.2121C6.7092 20.2011 6.67639 20.2011 6.65451 20.2011ZM6.52327 8.29053C6.51233 8.29053 6.50139 8.29053 6.49045 8.29053C5.33111 8.32334 4.25927 8.97957 3.70147 9.98579C2.4765 12.0967 3.28585 15.356 4.66394 17.3575C5.32017 18.2981 5.98734 19.1184 6.68732 19.1184C7.05919 19.1074 7.31074 18.998 7.63886 18.8559C8.06541 18.6809 8.54665 18.4731 9.31225 18.4731C10.045 18.4731 10.5263 18.6809 10.9091 18.8449C11.2481 18.9871 11.5106 19.1074 11.9153 19.0855C12.7028 19.0746 13.2496 18.3418 13.8949 17.3903C14.2668 16.8653 14.5621 16.3184 14.7809 15.7278C13.5559 15.0169 12.7793 13.7044 12.7793 12.2717C12.7903 11.0576 13.3371 9.93111 14.2449 9.1655C13.6871 8.60771 12.9215 8.26865 12.1231 8.24678C11.4669 8.18116 10.8216 8.43271 10.2529 8.66239C9.8263 8.83739 9.45443 8.97957 9.10444 8.97957C8.72164 8.97957 8.3279 8.82645 7.91229 8.65146C7.46386 8.47646 6.98263 8.29053 6.52327 8.29053Z" fill=white />
                                <path d="M8.71071 6.10931C8.69977 6.10931 8.6779 6.10931 8.66696 6.10931C8.38259 6.10931 8.14197 5.87963 8.13104 5.59526C8.05448 4.4578 8.42634 3.36408 9.18101 2.51097C9.93568 1.65787 10.9638 1.12195 12.1012 1.00164C12.2434 0.990703 12.3965 1.03445 12.5059 1.13289C12.6153 1.23132 12.6918 1.36257 12.7028 1.51569C12.7903 2.67503 12.4294 3.80156 11.6856 4.69841C10.9528 5.59526 9.87005 6.10931 8.71071 6.10931ZM11.6091 2.21567C10.9856 2.39066 10.4278 2.74065 10.0013 3.23283C9.56381 3.725 9.30132 4.32655 9.23569 4.96091C9.85911 4.8406 10.4278 4.50154 10.8435 3.99843C10.8435 3.99843 10.8435 3.99843 10.8544 3.9875C11.27 3.47345 11.5325 2.86096 11.6091 2.21567Z" fill=white />
                                <path class=stroke d="M6.65451 20.2011C5.33111 20.2011 4.42332 18.9215 3.76709 17.9699C2.46556 16.0778 1.12029 12.2389 2.74993 9.43893C3.49366 8.1046 4.9155 7.24056 6.46858 7.19681C7.15762 7.17493 7.81385 7.43743 8.3279 7.64523C8.63414 7.76554 8.94039 7.88585 9.11538 7.88585C9.25756 7.88585 9.57474 7.75461 9.85911 7.64523C10.5044 7.39368 11.3247 7.06556 12.1997 7.164C13.4903 7.20775 14.7371 7.87492 15.4918 8.9577C15.5793 9.07801 15.6121 9.23113 15.5793 9.38425C15.5465 9.53737 15.459 9.65768 15.3277 9.74518C14.4309 10.292 13.884 11.2436 13.8731 12.2935C13.8731 13.4529 14.573 14.5138 15.6668 14.9622C15.9293 15.0716 16.0605 15.356 15.973 15.6294C15.7105 16.4825 15.3058 17.2918 14.7918 18.0246C14.1902 18.9215 13.3481 20.1683 11.9372 20.1902C11.3247 20.2011 10.8763 20.0152 10.4935 19.8621C10.1325 19.709 9.8263 19.5777 9.32319 19.5777C8.77633 19.5777 8.44821 19.7199 8.07635 19.873C7.71542 20.0261 7.31074 20.1902 6.74201 20.2121C6.7092 20.2011 6.67639 20.2011 6.65451 20.2011Z" fill=white />
                                <path class=stroke d="M8.71071 6.10931C8.69977 6.10931 8.6779 6.10931 8.66696 6.10931C8.38259 6.10931 8.14197 5.87963 8.13104 5.59526C8.05448 4.4578 8.42634 3.36408 9.18101 2.51097C9.93568 1.65787 10.9638 1.12195 12.1012 1.00164C12.2434 0.990703 12.3965 1.03445 12.5059 1.13289C12.6153 1.23132 12.6918 1.36257 12.7028 1.51569C12.7903 2.67503 12.4294 3.80156 11.6856 4.69841C10.9528 5.59526 9.87005 6.10931 8.71071 6.10931Z" fill=white />
                            </svg>

                        </a>
                        <a href=/android class="download__link download__link--android js-link">

                            <svg width=16px height=19px viewBox="0 0 16 19" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                                <title>Android-logo (1)</title>
                                <desc>Created with Sketch.</desc>
                                <g id=Page-1 stroke=none stroke-width=1 fill=none fill-rule=evenodd opacity=0.8>
                                    <g id=Android-logo-(1) transform="translate(0.000000, -1.000000)" fill=#FFFFFF>
                                        <g id=fill>
                                            <path d="M2,15.65 C2.56,15.65 3.07,15.42 3.43,15.05 C3.6,15.34 3.8,15.55 4,15.69 L4,17.96 C4,19.08 4.9,19.99 6,19.99 C7.1,19.99 8,19.08 8,17.96 L8,17.99 C8,19.09 8.9,19.99 10,19.99 C11.1,19.99 12,19.08 12,17.96 L12,15.69 C12.21,15.55 12.4,15.35 12.57,15.05 C12.93,15.42 13.44,15.65 14,15.65 C15.1,15.65 16,14.75 16,13.65 L16,8.98 C16,7.88 15.1,6.98 14,6.98 C13.62,6.98 13.26,7.09 12.96,7.27 C12.98,7.18 13,7.08 13,6.98 C13,5.69 12.54,4.45 11.75,3.49 L11.95,3.28 C12.44,2.75 12.44,1.929999 11.95,1.409999 C11.45,0.869999 10.52,0.869999 10.02,1.409999 L9.51,1.949999 C8.53,1.609999 7.44,1.619999 6.47,1.949999 L5.96,1.399999 C5.46,0.859999 4.53,0.859999 4.02,1.399999 C3.53,1.919999 3.53,2.74 4.02,3.27 L4.24,3.5 C3.44,4.46 3,5.68 3,6.98 C3,7.08 3.02,7.18 3.04,7.27 C2.74,7.09 2.38,6.98 2,6.98 C0.9,6.98 0,7.88 0,8.98 L0,13.65 C0,14.75 0.9,15.65 2,15.65 Z" id=Path></path>
                                        </g>
                                        <g id=stroke>
                                            <path d="M2,15.65 C2.56,15.65 3.07,15.42 3.43,15.05 C3.6,15.34 3.8,15.55 4,15.69 L4,17.96 C4,19.08 4.9,19.99 6,19.99 C7.1,19.99 8,19.08 8,17.96 L8,17.99 C8,19.09 8.9,19.99 10,19.99 C11.1,19.99 12,19.08 12,17.96 L12,15.69 C12.21,15.55 12.4,15.35 12.57,15.05 C12.93,15.42 13.44,15.65 14,15.65 C15.1,15.65 16,14.75 16,13.65 L16,8.98 C16,7.88 15.1,6.98 14,6.98 C13.62,6.98 13.26,7.09 12.96,7.27 C12.98,7.18 13,7.08 13,6.98 C13,5.69 12.54,4.45 11.75,3.49 L11.95,3.28 C12.44,2.75 12.44,1.929999 11.95,1.409999 C11.45,0.869999 10.52,0.869999 10.02,1.409999 L9.51,1.949999 C8.53,1.609999 7.44,1.619999 6.47,1.949999 L5.96,1.399999 C5.46,0.859999 4.53,0.859999 4.02,1.399999 C3.53,1.919999 3.53,2.74 4.02,3.27 L4.24,3.5 C3.44,4.46 3,5.68 3,6.98 C3,7.08 3.02,7.18 3.04,7.27 C2.74,7.09 2.38,6.98 2,6.98 C0.9,6.98 0,7.88 0,8.98 L0,13.65 C0,14.75 0.9,15.65 2,15.65 Z M13,8.98 C13,8.43 13.45,7.98 14,7.98 C14.55,7.98 15,8.43 15,8.98 L15,13.65 C15,14.2 14.55,14.65 14,14.65 C13.45,14.65 13,14.2 13,13.65 L13,8.98 Z M5.63,3.53 L4.76,2.6 C4.63,2.46 4.63,2.24 4.76,2.09 C4.89,1.949999 5.1,1.949999 5.23,2.09 L6.22,3.15 C6.76,2.86 7.36,2.7 8,2.7 C8.63,2.7 9.23,2.86 9.76,3.15 L10.75,2.09 C10.88,1.949999 11.09,1.949999 11.22,2.09 C11.35,2.23 11.35,2.45 11.22,2.6 L10.35,3.53 C11.35,4.3 12,5.56 12,6.98 L4,6.98 C4,5.56 4.65,4.31 5.63,3.53 Z M4,7.98 L12,7.98 L12,12.98 C12,13.35 12,14.98 11,14.98 L11,17.95 C11,18.5 10.55,18.98 10,18.98 C9.45,18.98 9,18.53 9,17.98 L9,14.98 L7,14.98 L7,17.95 C7,18.5 6.55,18.98 6,18.98 C5.45,18.98 5,18.5 5,17.95 L5,14.98 C4,14.98 4,13.35 4,12.98 L4,7.98 Z M1,8.98 C1,8.43 1.45,7.98 2,7.98 C2.55,7.98 3,8.43 3,8.98 L3,13.65 C3,14.2 2.55,14.65 2,14.65 C1.45,14.65 1,14.2 1,13.65 L1,8.98 Z" id=Shape fill-rule=nonzero></path>
                                            <polygon id=Path points="7 4.98 6 4.98 6 5.98 7 5.98"></polygon>
                                            <polygon id=Path points="10 4.98 9 4.98 9 5.98 10 5.98"></polygon>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class=socials__wrap>
                    <ul class="socials__link socials__link--list">
                        <li class="social__item social__item--facebook">
                            <a href=# class=social__item--link target=_blank rel="nofollow noreferrer noopener">

                                <svg width=8px height=16px viewBox="0 0 8 16" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g id=Symbols stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                        <g id=main-screen-1440-/-active transform="translate(-36.000000, -760.000000)" fill=#C3A6FF fill-rule=nonzero>
                                            <g id=social transform="translate(36.000000, 748.000000)">
                                                <g id=facebook-letter-logo transform="translate(0.000000, 12.000000)">
                                                    <path d="M1.61058005,3.09230626 C1.61058005,3.49468213 1.61058005,5.29065429 1.61058005,5.29065429 L0,5.29065429 L0,7.97880278 L1.61058005,7.97880278 L1.61058005,15.9670348 L4.91905336,15.9670348 L4.91905336,7.97902552 L7.13919258,7.97902552 C7.13919258,7.97902552 7.34711833,6.69007889 7.44790719,5.28074246 C7.158942,5.28074246 4.93156381,5.28074246 4.93156381,5.28074246 C4.93156381,5.28074246 4.93156381,3.71686311 4.93156381,3.4427471 C4.93156381,3.16803712 5.2922877,2.79851508 5.64881671,2.79851508 C6.00467749,2.79851508 6.75574942,2.79851508 7.45139675,2.79851508 C7.45139675,2.43251972 7.45139675,1.16792575 7.45139675,0 C6.52272854,0 5.46620882,0 5.00050116,0 C1.52879814,-0.000185614849 1.61058005,2.69063573 1.61058005,3.09230626 Z" id=Facebook></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </li>
                        <li class="social__item social__item--instagram">
                            <a href=# class=social__item--link target=_blank rel="nofollow noreferrer noopener">

                                <svg width=16px height=16px viewBox="0 0 16 16" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g id=Symbols stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                        <g id=main-screen-1440-/-active transform="translate(-94.000000, -760.000000)" fill=#C3A6FF fill-rule=nonzero>
                                            <g id=social transform="translate(36.000000, 748.000000)">
                                                <g id=instagram-hover transform="translate(58.000000, 12.000000)">
                                                    <path d="M11,0 C13.761,0 16,2.239 16,5 L16,11 C16,13.761 13.761,16 11,16 L5,16 C2.239,16 0,13.761 0,11 L0,5 C0,2.239 2.239,0 5,0 L11,0 Z M8,4 C5.791,4 4,5.791 4,8 C4,10.209 5.791,12 8,12 C10.209,12 12,10.209 12,8 C12,5.791 10.209,4 8,4 Z M8,10.5 C6.622,10.5 5.5,9.378 5.5,8 C5.5,6.621 6.622,5.5 8,5.5 C9.378,5.5 10.5,6.621 10.5,8 C10.5,9.378 9.378,10.5 8,10.5 Z M12.767,4.167 C13.3192847,4.167 13.767,3.71928475 13.767,3.167 C13.767,2.61471525 13.3192847,2.167 12.767,2.167 C12.2147153,2.167 11.767,2.61471525 11.767,3.167 C11.767,3.71928475 12.2147153,4.167 12.767,4.167 Z" id=Combined-Shape></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </li>
                        <li class="social__item social__item--twitter">
                            <a href=# class=social__item--link target=_blank rel="nofollow noreferrer noopener">

                                <svg width=19px height=15px viewBox="0 0 19 15" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g id=Symbols stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                        <g id=main-screen-1440-/-active transform="translate(-160.000000, -761.000000)" fill=#C3A6FF fill-rule=nonzero>
                                            <g id=social transform="translate(36.000000, 748.000000)">
                                                <g id=twitter-logo-silhouette transform="translate(124.000000, 13.000000)">
                                                    <path d="M18.999969,1.77159639 C18.3006634,2.07222892 17.5503186,2.27611446 16.7619428,2.36713855 C17.5669281,1.89945783 18.1831242,1.1576506 18.4751716,0.276445783 C17.7200768,0.709548193 16.8865915,1.02403614 15.9984967,1.19451807 C15.2873317,0.458463855 14.275768,0 13.1537745,0 C11.0012484,0 9.25594935,1.69328313 9.25594935,3.78051205 C9.25594935,4.07653614 9.29037908,4.36566265 9.35687908,4.64213855 C6.118,4.48430723 3.24598856,2.97879518 1.32379085,0.691114458 C0.98778268,1.24864458 0.796633987,1.89828313 0.796633987,2.59174699 C0.796633987,3.90376506 1.4852598,5.06141566 2.53007353,5.73870482 C1.89130719,5.71798193 1.29054085,5.5475 0.764594771,5.26412651 L0.764594771,5.31135542 C0.764594771,7.14286145 2.10859641,8.67141566 3.89068464,9.01930723 C3.56417647,9.10454819 3.21987908,9.15177711 2.86369118,9.15177711 C2.61200327,9.15177711 2.36860458,9.12759036 2.12995588,9.08150602 C2.62622222,10.5847289 4.06522386,11.6778614 5.7701634,11.7078313 C4.4368415,12.721506 2.75565196,13.3239458 0.929633987,13.3239458 C0.61501634,13.3239458 0.305117647,13.305512 0,13.2721084 C1.72511928,14.3468373 3.77317647,14.9734639 5.97438235,14.9734639 C13.1443366,14.9734639 17.0635523,9.21168675 17.0635523,4.21478916 L17.050482,3.72524096 C17.8162565,3.19533133 18.4787418,2.52954819 18.999969,1.77159639 Z" id=Shape></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </li>
                        <li class="social__item social__item--medium">
                            <a href=# class=social__item--link target=_blank rel="nofollow noreferrer noopener">

                                <svg width=16px height=16px viewBox="0 0 16 16" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g id=Symbols stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                        <g id=main-screen-1440-/-active transform="translate(-393.000000, -760.000000)" fill=#C3A6FF>
                                            <g id=social transform="translate(36.000000, 748.000000)">
                                                <g id=medium-seeklogo.com transform="translate(357.000000, 12.000000)">
                                                    <path d="M0,0 L16,0 L16,16 L0,16 L0,0 Z M3.81818095,5.35103634 L3.81818095,10.1018104 C3.84741611,10.2731885 3.79339176,10.4482524 3.67265676,10.5733762 L2.54358974,11.9429235 L2.54358974,12.1235231 L5.74512197,12.1235231 L5.74512197,11.9429235 L4.61605497,10.5733762 C4.49443446,10.4484595 4.43706484,10.2745492 4.46049462,10.1018104 L4.46049462,5.99316843 L7.27061694,12.1235231 L7.59679188,12.1235231 L10.0104862,5.99316843 L10.0104862,10.8793923 C10.0104862,11.0098253 10.0104862,11.0349086 9.925179,11.1201918 L9.05705188,11.9629901 L9.05705188,12.1435898 L13.2722354,12.1435898 L13.2722354,11.9629901 L12.4342168,11.1402584 C12.3602399,11.0838689 12.3235436,10.9911888 12.3388734,10.8994589 L12.3388734,4.85438731 C12.3235436,4.76265733 12.3602399,4.66997725 12.4342168,4.61358778 L13.2923077,3.79085606 L13.2923077,3.61025641 L10.321607,3.61025641 L8.203979,8.89279606 L5.79530274,3.61025641 L2.67907779,3.61025641 L2.67907779,3.79085606 L3.68269291,4.99987036 C3.78121846,5.08867536 3.83153541,5.21908982 3.81818095,5.35103634 Z" id=Rectangle-2></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </li>
                        <li class="social__item social__item--meetup">
                            <a href=# class=social__item--link target=_blank rel="nofollow noreferrer noopener">

                                <svg width=33px height=14px viewBox="0 0 33 14" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g id=Symbols stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                                        <g id=main-screen-1440-/-active transform="translate(-229.000000, -762.000000)" fill=#C3A6FF>
                                            <g id=social transform="translate(36.000000, 748.000000)">
                                                <g id=meetup-logo-(1) transform="translate(193.000000, 14.000000)">
                                                    <path d="M6.688,3.24966667 C6.618,3.009 6.41333333,2.70733333 6.199,2.63033333 C5.849,2.50433333 5.44133333,2.51633333 5.05666667,2.51933333 C4.92033333,2.52033333 4.70933333,2.63433333 4.66066667,2.75066667 C4.39233333,3.39366667 4.12333333,4.04133333 3.93266667,4.70933333 C3.73133333,5.415 3.619,6.14566667 3.455,6.91766667 C3.38133333,6.794 3.323,6.70533333 3.27333333,6.61166667 C2.90933333,5.91466667 2.54966667,5.217 2.185,4.52133333 C1.818,3.82166667 1.61233333,3.71766667 0.823666667,3.84766667 C0.627666667,3.87933333 0.427666667,3.926 0.247333333,4.007 C0.157666667,4.04766667 0.0536666667,4.17766667 0.0506666667,4.27033333 C-0.006,5.88266667 -0.067,7.49366667 0.179,9.10033333 C0.349,10.2146667 0.555666667,10.3583333 1.62066667,9.95066667 C1.67366667,9.93 1.72233333,9.9 1.77533333,9.87766667 C2.016,9.77633333 2.12566667,9.61433333 2.08233333,9.339 C2.006,8.863 1.95233333,8.38333333 1.89066667,7.905 C1.91266667,7.90333333 1.93533333,7.90133333 1.95766667,7.89966667 C2.27866667,8.42133333 2.60966667,8.937 2.918,9.46633333 C3.17066667,9.89733333 3.46633333,10.179 4.019,10.0143333 C4.23333333,9.95033333 4.481,9.99366667 4.695,9.92833333 C4.84366667,9.883 5.01666667,9.762 5.08166667,9.628 C5.171,9.44233333 5.19333333,9.214 5.20533333,9.00166667 C5.257,8.104 5.29266667,7.204 5.33966667,6.306 C5.34733333,6.14066667 5.38033333,5.97633333 5.40666667,5.77766667 C5.88866667,7.319 6.359,8.79566667 6.80733333,10.28 C6.88533333,10.539 6.974,10.6463333 7.25,10.5646667 C7.632,10.4526667 8.02,10.3633333 8.33533333,10.2826667 C7.989,8.755 7.65766667,7.26833333 7.31466667,5.78366667 C7.11966667,4.936 6.93,4.08466667 6.688,3.24966667 Z" id=Path></path>
                                                    <path d="M32.4653333,5.06366667 C32.394,4.42 32.2076667,3.79666667 31.4253333,3.61533333 C31.1373333,3.548 30.8743333,3.34866667 30.586,3.30566667 C30.2806667,3.261 29.9116667,3.23633333 29.6506667,3.366 C28.6793333,3.85033333 28.0846667,4.69266667 27.8123333,5.71733333 C27.5643333,6.64933333 27.4356667,7.61333333 27.245,8.56133333 C27.224,8.67 27.1673333,8.817 27.0823333,8.861 C26.626,9.09866667 26.596,9.36566667 26.9673333,9.71633333 C27.0926667,9.83433333 27.1796667,10.0346667 27.2063333,10.2093333 C27.371,11.263 27.509,12.3203333 27.6713333,13.375 C27.6893333,13.495 27.7976667,13.6953333 27.8713333,13.699 C28.3073333,13.7176667 28.744,13.6886667 29.1886667,13.6756667 C28.7606667,11.9233333 28.6553333,10.8016667 28.8343333,10.0236667 C29.1076667,9.924 29.413,9.88133333 29.6326667,9.72066667 C30.2156667,9.29233333 30.7676667,8.82033333 31.3206667,8.353 C32.3506667,7.48566667 32.6046667,6.32666667 32.4653333,5.06366667 Z M28.8823333,8.528 C29.1926667,7.02666667 29.3793333,5.52133333 30.6326667,4.43433333 C31.2346667,5.98166667 30.4696667,7.707 28.8823333,8.528 Z" id=Shape fill-rule=nonzero></path>
                                                    <path d="M11.697,9.63766667 C11.0793333,9.919 10.61,9.80966667 10.256,9.263 C9.891,8.70033333 9.92166667,8.07466667 10.0056667,7.44566667 C10.705,7.48133333 11.3553333,7.51466667 12.0066667,7.55 C12.2553333,7.56366667 12.3263333,7.44233333 12.3836667,7.20433333 C12.6093333,6.26233333 12.4243333,5.329 12.372,4.393 C12.3426667,3.85833333 12.2183333,3.414 11.546,3.29766667 C10.9393333,3.193 10.3973333,3.00233333 9.83933333,3.50433333 C9.27966667,4.00933333 8.75433333,4.49133333 8.53966667,5.25366667 C8.27166667,6.20133333 8.301,7.16433333 8.28566667,8.12933333 C8.27333333,8.94933333 8.56566667,9.54366667 9.296,10.0103333 C10.3043333,10.6543333 12.2466667,10.701 13.2566667,9.98 C12.8143333,9.51933333 12.2373333,9.392 11.697,9.63766667 Z M10.204,5.88366667 C10.3,5.53233333 10.462,5.19966667 10.5946667,4.85833333 C10.6433333,4.87033333 10.691,4.88233333 10.7386667,4.89433333 C10.7506667,5.419 10.7623333,5.944 10.774,6.50333333 C10.063,6.47233333 10.045,6.46733333 10.204,5.88366667 Z" id=Shape fill-rule=nonzero></path>
                                                    <path d="M17.362,9.20933333 C17.243,9.17866667 17.0863333,9.19366667 16.9743333,9.247 C16.718,9.37066667 16.4946667,9.57266667 16.2296667,9.666 C15.79,9.82066667 15.351,9.803 15.0116667,9.41233333 C14.6006667,8.93933333 14.266,8.43933333 14.499,7.72833333 C15.2273333,7.82766667 15.9486667,8.084 16.607,7.669 C16.6726667,6.90533333 16.7316667,6.183 16.7993333,5.463 C16.8583333,4.85633333 16.9473333,4.25233333 16.9786667,3.64466667 C16.9856667,3.5 16.8466667,3.27733333 16.716,3.20966667 C16.3633333,3.031 15.986,2.89033333 15.6036667,2.78666667 C15.4276667,2.73866667 15.133,2.73666667 15.0393333,2.843 C14.525,3.427 13.9256667,3.99 13.6033333,4.67666667 C13.1513333,5.63966667 12.8796667,6.68466667 12.9166667,7.79566667 C12.9553333,8.945 13.6133333,9.90633333 14.718,10.188 C15.0203333,10.2643333 15.343,10.2603333 15.649,10.327 C16.4663333,10.5036667 17.148,10.173 17.8313333,9.824 C17.827,9.469 17.65,9.28133333 17.362,9.20933333 Z M15.295,4.833 C15.3413333,4.84333333 15.387,4.854 15.4333333,4.86533333 C15.4263333,5.38133333 15.4453333,5.89933333 15.405,6.41266667 C15.3576667,7.026 15.314,7.03766667 14.591,6.81966667 C14.8263333,6.15366667 15.0613333,5.493 15.295,4.833 Z" id=Shape fill-rule=nonzero></path>
                                                    <path d="M20.5536667,6.13366667 C20.5463333,6.225 20.4453333,6.308 20.387,6.39533333 C20.3456667,6.28666667 20.2663333,6.17633333 20.2716667,6.07 C20.2846667,5.793 20.3676667,5.518 20.3643333,5.243 C20.3483333,3.98866667 20.2726667,2.734 20.3003333,1.48133333 C20.318,0.666 20.3563333,0.625666667 19.596,0.284 C18.8546667,-0.049 18.505,0.142333333 18.4846667,0.958333333 C18.4653333,1.728 18.4953333,2.501 18.554,3.26866667 C18.6366667,4.33166667 18.769,5.391 18.8856667,6.515 C18.573,6.50133333 18.2923333,6.49133333 18.0136667,6.475 C17.6806667,6.45533333 17.4496667,6.57266667 17.4233333,6.93333333 C17.3946667,7.29833333 17.619,7.395 17.9366667,7.40066667 C18.2753333,7.40733333 18.6126667,7.436 18.9793333,7.45666667 C19.0263333,8.039 19.0746667,8.57666667 19.1113333,9.11466667 C19.1476667,9.64866667 19.1723333,10.183 19.203,10.745 C19.6153333,10.6763333 19.9713333,10.6173333 20.3433333,10.5556667 C20.3213333,10.0276667 20.2946667,9.54433333 20.283,9.05966667 C20.271,8.57666667 20.296,8.09166667 20.262,7.61 C20.2413333,7.336 20.3896667,7.348 20.5666667,7.32533333 C21.069,7.262 21.5693333,7.18866667 22.0696667,7.119 C22.0926667,7.06833333 22.1166667,7.01733333 22.1386667,6.96733333 C21.7366667,6.60833333 21.35,6.22866667 20.9233333,5.90166667 C20.7503333,5.76966667 20.5766667,5.86866667 20.5536667,6.13366667 Z" id=Path></path>
                                                    <path d="M26.5526667,5.606 C26.3566667,5.52133333 26.1703333,5.40733333 25.9653333,5.35133333 C25.83,5.31366667 25.6103333,5.29733333 25.5383333,5.37433333 C25.4176667,5.50366667 25.3373333,5.71166667 25.3266667,5.89233333 C25.3113333,6.142 25.3996667,6.39633333 25.401,6.649 C25.4053333,7.73166667 25.14,8.71033333 24.2213333,9.49433333 C24.1773333,9.35166667 24.1283333,9.26 24.122,9.16566667 C24.068,8.311 23.9896667,7.45633333 23.9813333,6.60033333 C23.9736667,5.76266667 23.9903333,5.73833333 23.203,5.392 C23.085,5.34033333 22.9703333,5.277 22.849,5.23766667 C22.444,5.107 22.245,5.23866667 22.231,5.66266667 C22.22,5.96133333 22.21,6.267 22.2583333,6.56 C22.4293333,7.58233333 22.5996667,8.606 22.8236667,9.61766667 C22.908,10.0046667 23.1923333,10.3193333 23.6363333,10.3306667 C24.0186667,10.3396667 24.412,10.3153333 24.7826667,10.2283333 C24.9903333,10.18 25.1873333,10.0006667 25.342,9.836 C25.569,9.594 25.7413333,9.30266667 25.9576667,9.05066667 C26.562,8.34433333 27.0553333,7.60633333 27.0896667,6.61966667 C27.108,6.137 27.0073333,5.801 26.5526667,5.606 Z" id=Path></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class=copy_wrap>
                    <ul class=copy_wrap_list>
                        <li class=copy_wrap_item><a href="{{route('privacy')}}" class="small-text up-text">{{__("Your Privacy")}}</a></li>
                        <li class="copy_wrap_item small-text up-text">{{__("2019 Big Elaboration, Inc.")}}</li>
                    </ul>
                </div>
            </div>
        </div>


        <span id=css-variable-test></span>



        <script type=text/javascript src={{asset('/')}}js/554b65.js></script>
        @stack('scripts')

    </body>

</html>