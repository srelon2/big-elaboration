@extends('layouts.app')

@section('content')
    <main id=main data-page-id=privacy>
        <div style="background-image:linear-gradient(134deg,#7c55f2 0,#af89fa 51%,#fab6b6 100%)" data-bg='{ "deg": 135, "colors":[{ "c": "#7C55F2", "p": 0 }, { "c": "#A173FF", "p": 0.51 }, { "c": "#CFA5FA", "p": 1 }] }' class=bg>
            <div id=waves class=waves_wrap>
                
                <svg id=wave-1 width=1920px height=371px viewBox="0 0 1920 371" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

                    <title>from_1 </title>
                    <desc>Created with Sketch.</desc>
                    <defs>
                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=linearGradient-1>
                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                        </linearGradient>
                    </defs>
                    <g id=1920 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                        <g id=WAVES transform="translate(0.000000, -37.000000)" fill=url(#linearGradient-1) fill-rule=nonzero>
                            <g id=bg transform="translate(-3.000000, -2.000000)">
                                <g id=Cloud_1 transform="translate(3.000000, 0.000000)">
                                    <path d="M1920,408.651004 C643.117748,410.239444 3.11774828,410.239444 0,408.651004 L0,279.469546 C740.574468,345.677945 843.188992,-128.789792 1920,35.669798 C1920,44.1946085 1920,168.521677 1920,408.651004 Z" id=from_1- transform="translate(960.000000, 205.325502) scale(1, -1) translate(-960.000000, -205.325502) "></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>

                <svg id=wave-2 width=1920px height=387px viewBox="0 0 1920 387" version=1.1 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                        <linearGradient x1=50% y1=90.5469853% x2=50% y2=24.5787128% id=linearGradient-1>
                            <stop stop-color=#FFFFFF stop-opacity=0 offset=0%></stop>
                            <stop stop-color=#FFFFFF stop-opacity=0.15 offset=100%></stop>
                        </linearGradient>
                    </defs>
                    <g id=1920 stroke=none stroke-width=1 fill=none fill-rule=evenodd>
                        <g id=WAVES transform="translate(0.000000, -39.000000)" fill=url(#linearGradient-1) fill-rule=nonzero>
                            <g id=bg transform="translate(0.000000, -2.000000)">
                                <g id=Cloud_2>
                                    <path d="M1920,425.977003 C643.117748,427.565442 3.11774828,427.565442 -2.84217094e-14,425.977003 L-4.54747351e-13,286.99596 C730.197368,344.977003 1195.61921,-102.022997 1920,22.9770026 C1920,31.5018132 1920,165.835146 1920,425.977003 Z" id=from_1- transform="translate(960.000000, 213.988501) scale(1, -1) translate(-960.000000, -213.988501) "></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>
        </div>
        <section class="privacy-section active">
            <div class=container>
                <div class=content>
                    <div class=title_wrap>
                        <h2 class=title-h2>{{__('We’re invested in your privacy.')}}</h2>
                    </div>
                    <div class=text_wrap>
                        <p class=desc-1>{{__('If we can’t guard your privacy as a Big Elaboration user, we shouldn’t exist. Our users’ privacy is everything to us, and it shapes the way we plan our business, design our technology’s architecture, and build our products. At Big Elaboration, we don’t view “privacy” as an obstacle we must work around, but rather a standard that guides our moral compass.')}}</p>
                        <h5 class=title-h5>{{__('As of May 2018, these are the commitments we are making publicly:')}}</h5>
                        <ul>
                            <li class=desc-1>{{__('We will never make money selling your data.')}}</li>
                            <li class=desc-1>{{__('You will always own your data.')}}</li>
                        </ul>
                        <p class=desc-1>{{__('Below is our privacy policy. We strive to communicate our privacy policy using as clear language as possible, from the information we collect to the controls you have as a user. Please ask us questions if something doesn’t make sense. Reach out anytime at')}} <a class=js-link href=mailto:support@bigelaboration.com>support@bigelaboration.com</a>.
                        </p>
                        <h3 class=title-h3>{{__('elaboration inc. privacy policy')}}</h3>
                        <p class="desc-1 mt-12">{!! __('<span>Last Updated: </span>May 17, 2018')!!}</p>
                        <p class="desc-1 mt-12">{!! __("Big Elaboration Inc. (“<span>We</span>” or ”<span>Company</span>”) is committed to protecting your privacy. This privacy policy describes how we collect, store, use, and distribute information through Big Elaboration (the “<span>App</span>”).")!!}
                        </p>
                        <h5 class=title-h5>{{__("consent")}}</h5>
                        <p class="desc-1 mt-12">{{__("By using the App, you consent to the use of your information as described in this privacy policy. You may withdraw your consent at any time by drafting an email to ")}} <a class=js-link href=mailto:support@bigelaboration.com>{{__("support@bigelaboration.com")}}</a> . {{__("However, withdrawing your consent may result in your inability to continue using some or all of the features of the App.")}}
                        </p>
                        <p class="desc-1 mt-12">{{__("Please read this policy carefully to understand our policies and practices regarding your information and how we will treat it.  If you do not agree with our policies and practices, please do not download or use the App. By using this App, you agree to this privacy policy.")}}</p>
                        <h2 class=title-h2>1. {{__("information we collect.")}}</h2>
                        <h3 class=title-h3>{{__("scope")}}</h3>
                        <p class="desc-1 mt-12">{{__("This policy applies to information we collect in the App and in email, text, and other electronic communications sent through or collected in connection with the App.")}}
                        </p>
                        <h5 class=title-h5>{{__("there are three basic categories of information we collect:")}}</h5>
                        <ul>
                            <li class=desc-1>{{__("Information you give us.")}}</li>
                            <li class=desc-1>{{__("Information obtained automatically from your use of our service.")}}</li>
                            <li class=desc-1>{{__("Information obtained from third parties.")}}</li>
                        </ul>
                        <p class="desc-1 mt-12">{{__("Here’s a little more detail on each of these categories.")}}</p>
                        <h3 class=title-h3>a. {{__("information you give us.")}} </h3>
                        <p class="desc-1 mt-12">{{__("The primary purpose of the App is to allow you to journal your life in a new, fun, and interactive way. We are utilizing machine learning algorithms to allow you to use your device in a new way that you may never have thought possible. At any point in the day, you can record a 60 second audio clip through the App.  We plan to aggregate those 60 second recordings to analyze your Elaboration and mood indicators by using speech recognition, facial recognition, sentiment, translation, background audio that collects your life on the go. You will be able to look back on your life, day by day, through the lens of a 60 second window.")}}</p>
                        <p class="desc-1 mt-12">{{__("When registering to use the App and creating an account (“Big Elaboration Account”), we may also ask you to provide certain information about yourself, such as your name, email address, social media username and profiles, and user name and password (“Contact Information”). Your Contact Information, together with any other information we gather through the App that may be used to identify, contact, or locate you individually, is collectively referred to as your “Personal Information.”")}}  </p>
                        <p class="desc-1 mt-12">{{__("You can also link your Big Elaboration Account to your social media accounts when using the services. By linking your Malso Account with your accounts on social media, you are allowing us to access your information from that site. The information that we collect from your social media accounts may depend on the privacy settings you have with that network. Therefore, you may be able to control the information that we collect from your social media accounts by adjusting your privacy settings on that third-party service. You can also de-link your social media accounts from the App at any time via your Big Elaboration Account settings.")}}</p>
                        <h5 class=title-h5>{{__("in summary, we may ask personal Information from you, such as your:")}}
                        </h5>
                        <ul>
                            <li class=desc-1>{{__("Name;")}}</li>
                            <li class=desc-1>{{__("Email address;")}}</li>
                            <li class=desc-1>{{__("Login information (username/password);")}}</li>
                            <li class=desc-1>{{__("Social media accounts you identify with other third-party services.")}}</li>
                        </ul>
                        <p class="desc-1 mt-25">{{__("You can always refuse to supply personal identification information we request, except that it may prevent you from accessing the service or engaging in certain service related activities.")}}</p>
                        <h3 class=title-h3>b. {{__("information obtained from your use of our service.")}}</h3>
                        <p class="desc-1 mt-25">{{__("We may collect non-personally identifiable information in connection with your use of our service.")}}</p>
                        <h5 class=title-h5>{{__("here are some types of information we collect automatically:")}}</h5>
                        <ul>
                            <li class=desc-1>{!! __('<span>Analytics</span>. When you use our App, we automatically track and collect the following categories of information: (1) your Internet protocol address and domain server address; (2) the date and time of your use of our App; (3) the type of device and system you used to access the App; (4) the links that you accessed during your use of the App; (4) the general geolocation of your device; (5) time and date stamps, (6) local weather and (6) other non-personally identifiable information (collectively, “Traffic Data”). We use Traffic Data to evaluate and improve the content of the App and to make the App more useful for our users.  We also use such information for testing and debugging purposes.') !!}
                            </li>
                            <li class=desc-1>{!! __('<span>Biometric Information</span>.  Certain features of the App may actively record biometric information about yourself as you use our service.  This information includes recording your voice, haptic feedback, and anything else that has to do with your physical body and health.') !!}</li>
                            <li class=desc-1>{!! __('<span>Usage Information</span>. Such as how you interact with the services and other users, how you communicate with the App including sentiment, translation, speech recognition, face recognition, and background audio.  Since our app is paired to your microphone and all audio that comes through is personal biometric information, see our “Biometric Information” section for more on this.') !!}</li>
                            <li class=desc-1>{!! __('<span>Cookies</span>.  Like most app providers, we use cookies and other technologies to track, customize and enhance your user experience.  Cookies saved on your mobile device are used for record-keeping purposes, to better understand how you use the service and to allow us to make improvements to the service. You may choose to set your device to refuse cookies, or to alert you when cookies are being sent.  If you do so, some parts of the service may not function properly.') !!}</li>
                        </ul>
                        <h3 class=title-h3>c. {{ __('information obtained from third parties.') }} </h3>
                        <p class="desc-1 mt-12">{{ __('When you use the App or its content, certain third parties may use automatic information collection technologies to collect information about you or your device. These third-party apps may include Amazon Alexa, Google Home or other IP enabled devices.') }}</p>
                        <p class="desc-1 mt-12">{{ __('These third parties may use tracking technologies to collect information about you when you use this app. The information they collect may be associated with your personal information or they may collect information, including personal information, about your online activities over time and across different websites, apps, and other online services websites.') }}</p>
                        <p class="desc-1 mt-12">{{ __('We do not control these third parties’ tracking technologies or how they may be used. If you have any questions about an advertisement or other targeted content, you should contact the responsible provider directly.') }}</p>
                        <h2 class=title-h2>2. {{ __('uses of information collected.') }}</h2>
                        <h5 class=title-h5>{{ __('We may collect and use user’s personal information for the following reasons:') }}</h5>
                        <ul>
                            <li class=desc-1>{{ __('To improve our machine learning algorithms and artificial intelligence models.') }}</li>
                            <li class=desc-1>{{ __('To personalize your user experience. We may use information in the aggregate to understand how our users as a group use the service.') }}</li>
                            <li class=desc-1>{{ __('To improve the service. We may use feedback you provide, and information we gather to improve our products and services and to send users information they agreed to receive about topics we think will be of interest to them.') }}</li>

                            <li class=desc-1>{{ __('Perform internal operations, including, for example, to prevent fraud and abuse of our Services; to troubleshoot software bugs and operational problems; to test, analyze, conduct data analysis, and to monitor and analyze usage and activity trends;') }}</li>
                            <li class=desc-1>{{ __('To send you email or text communications we think will be of interest to you.') }}</li>
                            <li class=desc-1>{{ __('To share information if we believe in good faith that such disclosure is necessary (a) in connection with any legal investigation; (b) to comply with relevant laws or to respond to subpoenas or warrants served on us; or (c) to defend or enforce the rights of our users or of us in connection with a lawsuit or proceeding, including enforcing our agreements, policies and terms of use; or (d) to investigate or assist in preventing any violation of law, this privacy policy or our Terms.') }}</li>
                        </ul>
                        <p class="desc-1 mt-12">{{ __('We use your information to administer and improve our services to you, such as by providing you with patterns in what you share, and reflect insights and feelings-related feedback. We may also use your information in a de-identified, aggregated, and anonymous way to monitor and analyze use of our services, to increase App functionality and user-friendliness, and for purposes of general research and development and AI learning. We want our service to be as beneficial to you and your daily thoughts and feelings.') }}</p>
                        <ul>
                            <li class=desc-1>{{ __('We do not disclose user data gathered from the Big Elaboration APIs or from health-related research for advertising or other use-based data mining purposes other than improving health, or for the purpose of health research. We do not use or disclose to third parties your biometric information for the purpose of advertising or marketing in any way.') }}</li>
                            <li class=desc-1>{{ __('We do not share user data acquired via the App APIs with third parties without user consent.') }}</li>
                            <li class=desc-1>{{ __('We do not sell, trade, or rent Personal Information or any other information collected through our service to third parties. We do not place advertising on the service and we do not sell your user personal or non-personal information to any third-party. We may use, and we may also share non-individual, aggregated information and data with our business partners and with business users solely for the purpose of better understanding the needs, interests and usage of our users.') }}</li>
                        </ul>
                        <h2 class=title-h2>3. {{ __('how we protect your information') }}</h2>
                        <p class="desc-1 mt-12">{{ __('We adopt generally accepted data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data gained from the service and stored on our systems. You understand that no system can guarantee unauthorized intrusion or hacking.') }}</p>
                        <h2 class=title-h2>4. {{ __('California Privacy Law Disclosures') }}</h2>
                        <p class="desc-1 mt-12">{{ __('You have the right to access the Personal Information we hold about you in order to verify the Personal Information we have collected in respect to you and to have a general account of our uses of that information. Upon receipt of your written request, we will provide you with a copy of your Personal Information, although in certain limited circumstances we may not be able to make all relevant information available to you, such as where that information also pertains to another user. In such circumstances we will provide reasons for the denial to you upon request. We will endeavor to deal with all requests for access and modifications in a timely manner.') }} </p>
                        <p class="desc-1 mt-12">{{ __('We will make every reasonable eﬀort to keep your Personal Information accurate and up-to-date, and we will provide you with mechanisms to update, correct, delete or add to your Personal Information as appropriate. As appropriate, this amended Personal Information will be transmitted to those parties to which we are permitted to disclose your information. Having accurate Personal Information about you enables us to give you the best possible service.') }} </p>
                        <p class="desc-1 mt-12">{{ __('Under California Civil Code Sections 1798.83 through 1798.84, California residents are entitled to ask us for a notice identifying the categories of Personal Information which we share with our affiliates and/or third parties for marketing purposes, and providing contact information for such affiliates and/or third parties. If you are a California resident and would like a copy of this notice, please submit a written request to:') }} <a class=js-link href=mailto:support@bigelaboration.com>{{ __('support@bigelaboration.com') }}</a></p>
                        <h2 class=title-h2>5. {{ __('changes to our policy.') }}</h2>
                        <p class="desc-1 mt-12">{{ __('We may update this privacy policy from time to time. If we make changes to how we treat users’ personal information, we will revise the updated date at the top of this page. You are responsible for ensuring we have an up-to-date policy for you.  We encourage users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this policy periodically and become aware of modifications.') }} </p>
                        <p class="desc-1 mt-12">{{ __('By accessing the service, you understand and agree that you accept the terms of this privacy policy. Your continued use of the service following the posting of changes to this privacy policy will be deemed your acceptance of those changes.') }}</p>
                        <h2 class=title-h2>6. {{ __('Other Communications.') }}</h2>
                        <p class="desc-1 mt-12">{{ __('Please note that the service is only intended for the use of individuals at least 13 years of age. No one under the age of 13 is authorized to access the service.  Big Elaboration will deny access to any individual which it learns is attempting to access the service, or who has accessed the service, who is under such age.') }}</p>
                        <p class="desc-1 mt-12">{{ __('Our App is hosted in the United States and our services are intended primarily for visitors located within the United States. If you choose to use the App from the European Union or other regions of the world with laws governing data collection and use that may diﬀer from U.S. law, then please note that you may be transferring your Personal Information outside of those regions to the United States for storage and processing. By providing your Personal Information through our App, you consent to such transfer, storage, and processing.') }} </p>
                        <p class="desc-1 mt-12">{{ __('We are not an entity that is covered by the Health Insurance Portability and Accountability Act (“HIPAA”). The HIPAA privacy rules apply to health plans, health care clearinghouses, to any health care provider who transmits health information in electronic form in connection with transactions for which the Secretary of HHS has adopted standards under HIPAA and their service providers. This means that the information that you provide to us is not protected by the HIPAA privacy rules and regulations.') }} </p>
                        <p class="desc-1 mt-12">{{ __('If you wish to contact us regarding this privacy policy, you may do so by contacting us at:') }} </p>
                        <div class=contacts>
                            <p class="desc-1 mt-2">{{ __('Big Elaboration Inc.') }}</p>
                            <p class="desc-1 mt-2">{{ __('4470 W Sunset Blvd #90283') }}</p>
                            <p class="desc-1 mt-2">{{ __('Los Angeles, CA 90027') }} </p>
                            <p class="desc-1 mt-2"><a class=js-link href=mailto:support@bigelaboration.com>{{ __('support@bigelaboration.com') }}</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection