<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Controllers extends Model
{
    use SoftDeletes;

    protected $table = 'controllers';
    protected $dates = ['deleted_at'];


}
