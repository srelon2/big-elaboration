<?php
namespace App\Models\Admin;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use SoftDeletes;
    protected $table = 'admins';
    protected $dates = ['deleted_at'];

    public function role(){
        return $this->belongsTo('App\Models\Admin\Roles', 'role_id');
    }
    public function telegram(){
        return $this->hasMany('App\Models\Admin\AdminsTelegram', 'admin_id');
    }
}
