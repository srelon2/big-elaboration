<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Answers extends Model
{
    use SoftDeletes;
    //
    protected $table = 'answers';
    protected $dates = ['deleted_at'];

}
