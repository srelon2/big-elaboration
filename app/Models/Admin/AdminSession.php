<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class AdminSession extends Model
{

    protected $table = 'sessions';
}
