<?php

namespace App\Http\Controllers\Api\Telegram\Exceptions;

/**
 * Class TelegramSDKException.
 */
class TelegramSDKException extends \Exception
{
}
