<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function about()
    {
        return view('about');
    }
    public function privacy()
    {
        return view('privacy');
    }
    public function coaching()
    {
        return view('coaching');
    }
    public function demo()
    {
        return view('demo');
    }
}
