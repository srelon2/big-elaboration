<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EducationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('education');
    }
    public function mindful()
    {
        return view('education-mindful');
    }
    public function heroic()
    {
        return view('education-heroic');
    }
    public function openPalms()
    {
        return view('education-open-palms');
    }
    public function healthy()
    {
        return view('education-healthy');
    }
}
