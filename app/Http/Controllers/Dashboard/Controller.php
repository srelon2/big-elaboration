<?php

namespace App\Http\Controllers\Dashboard;
use Illuminate\Support\Facades\Auth;

use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{

    protected $user;
    protected $data;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->data['user']= $this->user = Auth::guard('web')->user();
            return $next($request);
        });
    }
}
