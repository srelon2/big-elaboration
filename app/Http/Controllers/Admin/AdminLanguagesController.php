<?php

namespace App\Http\Controllers\Admin;

use App\Requests\Admin\LanguagesRequest;
use File;
use App\Traits\Admin\LanguagesTraits;

class AdminLanguagesController extends Controller
{
    use LanguagesTraits;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();

        $this->middleware(function ($request, $next) {
            $accesses= $this->accessesRoles($this->admin,'AdminLanguagesController');

            if(!$accesses['view']) abort('403');
            $this->data['edit']= $accesses['edit'];

            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('admin.languages', $this->data)->with('pagetitle', 'LanguagesTraits');
    }

    public function info($id= false, $backup= false){
        if($id) {
            if($backup) {
                $backup= $this->getBackup('languages', $id, $backup);
            }

            if($backup){
                $item= $backup;
                $this->data['translation']= $item->translation;
            } else {
                $item= $this->getLanguage($id);
                $this->data['translation']= File::get(resource_path('lang/'.$item->key.'.json'));
            }
            $this->data['item']= $item;
            $this->data['backups']= $this->getLog('languages', $id);
        } else {
            $this->data['translation']= File::get(resource_path('lang/en.json'));
        }
        return view('admin.languages-info', $this->data)->with('pagetitle', (($id) ? 'Edit Language ID:'.$id : 'Create Language').(($backup) ? ' Restored' : ''));
    }

    public function search() {
        $response= $this->ajaxTableLanguages($_POST);
        echo json_encode($response);
    }

    public function update(LanguagesRequest $request, $id= false) {
        $data= $request->except('_token');

        $response= $this->updateLanguage($data, $id);
        return redirect()->route('admin.languages.info', ['id'=>$response['item']->id])->with($response['status'], $response['mess']);
    }
}
