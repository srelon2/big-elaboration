<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Models\Admin\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use DateTime;
use App\Traits\Admin\AdministratorsTraits;
use App\Traits\Admin\LogsTraits;
use App\Requests\Admin\ValidateAdminSecretRequest;
use App\Traits\GlobalTraits;
use App\Traits\Admin\SettingsTraits;

class LoginController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

    use AuthenticatesUsers, AdministratorsTraits, LogsTraits, GlobalTraits, SettingsTraits;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/adminroute';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard('admin')->logout();
        $request->session()->invalidate();

        return redirect('/adminroute/login');
    }


    public function login(Request $request)
    {
        $this->validateLogin($request);
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function authenticated(Request $request, $admin)
    {
        $admin= Auth::guard('admin')->user();

        if ($admin->google_two_step || $admin->email_two_step || $admin->telegram_two_step) {
            Auth::guard('admin')->logout();
            $this->openTwoStep($admin);
            $request->session()->put('2fa:admin:id', $admin->id);

            $time = new DateTime();
            $request->session()->put('time', $time->getTimestamp());

            return redirect($this->redirectTo.'/auth/validate');
        }
        $this->authLogs($admin, 'admin_login', 'Вошел в админ панель');

        return redirect()->intended($this->redirectTo);
    }

    public function getValidateToken()
    {
        if (session('2fa:admin:id')) {
            return view('admin.auth.validate');
        }

        return redirect($this->redirectTo.'/login');
    }


    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function postValidateToken(ValidateAdminSecretRequest $request)
    {
        $admin= Admin::find($request->session()->get('2fa:admin:id'));
        if ($admin->google_two_step) {
            $google_two_step= $this->checkSecret($admin, $request->secret, 'google_two_step');
        } else {
            $google_two_step= false;
        }
        if ($admin->telegram_two_step) {
            $telegram_two_step= $this->checkSecret($admin, $request->secret, 'telegram_two_step');
        } else {
            $telegram_two_step= false;
        }
        if ($admin->email_two_step) {
            $email_two_step= $this->checkSecret($admin, $request->secret, 'email_two_step');
        } else {
            $email_two_step= false;
        }
        if ($google_two_step || $email_two_step || $telegram_two_step) {

            //get user id and create cache key
            $userId = $request->session()->pull('2fa:admin:id');
            Auth::guard("admin")->loginUsingId($userId);

            $this->authLogs($admin, 'admin_login', 'Вошел в админ панель');

            return redirect()->intended($this->redirectTo);
        } else {
            return redirect($this->redirectTo.'/auth/validate')->withErrors(['secret'=>'Не правильный Secret code']);
        }

    }
    public function sendAuthCodeEmail(Request $request,$id) {
        $admin = Admin::find($id);

        Auth::guard('admin')->logout();
        $this->openTwoStep($admin);
        $request->session()->put('2fa:admin:id', $admin->id);

        $time = new DateTime();
        $request->session()->put('time', $time->getTimestamp());

        return redirect($this->redirectTo.'/auth/validate');
    }
}