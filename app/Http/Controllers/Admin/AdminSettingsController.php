<?php

namespace App\Http\Controllers\Admin;

use App\Requests\Admin\SettingsRequest;
use Illuminate\Http\Request;
use App\Traits\Admin\SettingsTraits;

class AdminSettingsController extends Controller
{
    use SettingsTraits;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
//            $accesses = $this->accessesRoles($this->admin, 'AdminSettingsController');
//
//            if (!$accesses['view']) abort('403');
//            $this->data['edit'] = $accesses['edit'];

            return $next($request);
        });
    }
    public function index($type='main') {
        $this->data['type']= $type;

        switch ($type) {
            case 'safety':
                $this->data= array_merge($this->data, $this->getTwoStep($this->admin->id));
                $this->data['logs']= $this->logsAdmin($this->admin->id);
                $this->data['sessions']= $this->getAdminSessions();
            break;
        }

        return view('admin.settings', $this->data)->with('pagetitle', 'Настройки аккаунта');
    }

    public function update(SettingsRequest $request) {
        $data= $request->except('_token');
        $response= $this->updateSettings($data);
        if($response['status']== 'success') {
            return redirect()->back()->with([$response['status']=> $response['mess'], 'table'=> $response['submit']]);
        } else {
            return redirect()->back()->withErrors([$response['status']=>$response['mess']])->with('table', $response['submit']);
        }
    }

    public function secret(Request $request, $type='email_two_step', $id= false) {
        $data= $request->except('_token');
        $response= $this->secretSend($data, $type, $id);
        if($response['status']== 'success') {
            return redirect()->back()->with([$response['status']=> $response['mess'], 'table'=> $type]);
        } else {
            return redirect()->back()->withErrors([$response['status']=>$response['mess']])->with('table', $type);
        }
    }
}
