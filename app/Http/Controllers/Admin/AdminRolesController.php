<?php

namespace App\Http\Controllers\Admin;

use App\Requests\Admin\RolesRequest;

use App\Traits\Admin\RolesTraits;
use App\Traits\Admin\RoutesTraits;
use Illuminate\Support\Facades\Input;

class AdminRolesController extends Controller
{
    use RoutesTraits, RolesTraits;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();

        $this->middleware(function ($request, $next) {
            $accesses= $this->accessesRoles($this->admin,'AdminRolesController');

            if(!$accesses['view']) abort('403');
            $this->data['edit']= $accesses['edit'];

            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $this->data['items']= $this->getRoles();

        return view('admin.roles', $this->data)->with('pagetitle', 'RolesTraits');
    }

    public function info($id= false, $backup= false){
        if($id) {
            if($backup) {
                $backup= $this->getBackup('roles', $id, $backup);
                $accesses= json_decode($backup->accesses_id);
                $backup->view= collect($accesses->view);
                $backup->edit= collect($accesses->edit);
            }

            if($backup){
                $this->data['item']= $backup;
            } else {
                $this->data['item']= $this->getRole($id);
            }
            $this->data['backups']= $this->getLog('roles', $id);
        }
        $this->data['routes']= $this->getRouteControllers();
        return view('admin.roles-info', $this->data)->with('pagetitle', (($id) ? 'Edit Roles ID:'.$id : 'Create Roles').(($backup) ? ' Restored' : ''));
    }

    public function search() {
        $response= $this->ajaxTableRoles($_POST);
        echo json_encode($response);
    }

    public function update(RolesRequest $request, $id= false) {
        $data= $request->except('_token');
        $response= $this->updateRole($data, $id);
        return redirect()->route('admin.roles.info', ['id'=>$response['item']->id])->with($response['status'], $response['mess']);
    }

    public function itemAction($action,$id= false) {
        if(empty($id)) {
            $id= Input::get('data');
        }
        $ajax= Input::get('ajax');
        $response= $this->actionRoles($action, $id);
        switch ($response['status']) {
            case 'success':
                $msg= $response['mess'];
                $status= 'success';
                break;
            default:
                $msg= 'Error. Не удалось изменить! '.json_encode($response['mess']);
                $status= 'danger';
        }
        if($ajax) {
            return response()->json(array('msg'=> $msg, 'status'=>$status));
        } else {
            return redirect()->back()->with($status, $msg);
        }

    }
}
