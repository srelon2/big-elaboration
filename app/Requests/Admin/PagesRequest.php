<?php

namespace App\Requests\Admin;

use App\Traits\Admin\RolesTraits;
use App\Requests\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\Admin\LanguagesTraits;

class PagesRequest extends Request
{
    use RolesTraits, LanguagesTraits;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $admin= Auth::guard('admin')->user();
        if($this->accessesRoles($admin,'AdminPagesController')['edit']) {
            return true;
        } else {
            return abort('403');
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $languages= $this->getLangs();

        $validation= [];
        foreach ($languages as $lang) {
            $validation['title_'.$lang]= 'required';
            $validation['content_'.$lang]= 'required';
//            $validation['desc_'.$lang]= 'required';
        }
        $validation['url'] = 'nullable|unique:pages,url,' . $this->id;

        return $validation;
    }

    public function messages()
    {
        return [
//            'name.required' => 'Please select page.',
        ];
    }

}
