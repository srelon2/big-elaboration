<?php

namespace App\Requests\Admin;

use App\Traits\Admin\RolesTraits;
use App\Requests\Request;
use Illuminate\Support\Facades\Auth;

class SettingsRequest extends Request
{
    use RolesTraits;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
//        $admin= Auth::guard('admin')->user();
//        if($this->accessesRoles($admin,'AdminAdministratorsController')['edit']) {
//            return true;
//        } else {
//            return abort('403');
//        }
        if(Auth::guard('admin')->user()) {
            return true;
        } else {
            return abort('403');
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $admin= Auth::guard('admin')->user();
        switch ($this->submit) {
            case 'google_two_step':
            case 'email_two_step':
            case 'telegram_two_step':
            case 'chat_id':
                $data= [
                    $this->submit => 'required',
                ];
                break;
            default :
            $data= [
                'name' => 'required|unique:admins,name,' . $admin->id . '|max:50',
                'email' => 'required|unique:admins,email,' . $admin->id . '|email|max:100',
                'checked_password' => 'required',
            ];
        }

        return $data;
    }

    public function messages()
    {
        return [
//            'name.required' => 'Please select page.',
        ];
    }

}
