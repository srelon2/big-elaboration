<?php

namespace App\Requests\Admin;

use App\Traits\Admin\RolesTraits;
use App\Requests\Request;
use Illuminate\Support\Facades\Auth;

class LanguagesRequest extends Request
{
    use RolesTraits;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $admin= Auth::guard('admin')->user();
        if($this->accessesRoles($admin,'AdminLanguagesController')['edit']) {
            return true;
        } else {
            return abort('403');
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $key = 'required|min:2|max:100|unique:languages,key,' . $this->id . '|max:50';
        if (isset($this->id)) {
            $key = '';
        }
        //echo $id_str;exit;
        return [
            'key' => $key,
            'name' => 'required|unique:admins,name,' . $this->id . '|max:50',
            'translation' => 'required',
        ];
    }

    public function messages()
    {
        return [
//            'name.required' => 'Please select page.',
        ];
    }

}
