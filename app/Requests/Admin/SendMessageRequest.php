<?php

namespace App\Requests\Admin;

use App\Traits\Admin\RolesTraits;
use App\Requests\Request;
use Illuminate\Support\Facades\Auth;

class SendMessageRequest extends Request
{
    use RolesTraits;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $admin= Auth::guard('admin')->user();
        if($this->accessesRoles($admin,'AdminSupportsController')['edit']) {
            return true;
        } else {
            return abort('403');
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->submit=='draft') {
            return [
                'text' => 'required',
            ];
        } else {
            return [
                'email' => 'required',
                'text' => 'required',
            ];
        }
    }

    public function messages()
    {
        return [
//            'name.required' => 'Please select page.',
        ];
    }

}
