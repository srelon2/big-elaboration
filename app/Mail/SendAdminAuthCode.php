<?php

namespace App\Mail;

use App\Models\Admin\Admin;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendAdminAuthCode extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public function __construct(Admin $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            "hy" => "Hi dear admin! ",
            "msg" => "<p>Your code: <b>{$this->user->email_secret}</b> </p> <p>Thank you,</p>",
        ];
        return $this->subject("Admin OTP code")->markdown("emails.default_mail")->with($data);

    }
}
