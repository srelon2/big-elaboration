<?php

namespace App\Traits\Admin;
use App\Models\Admin\Route;
use App\Models\Admin\Controllers;
use File;
use Mockery\Exception;
use Illuminate\Support\Facades\Artisan;

trait RoutesTraits {

    public function getRoutes() {
        return Route::get();
    }
    public function getRoute($id) {
        return Route::find($id);
    }

    public function searchRoutes($searchValue , $post= false, $table= false) {
        $controllers= Controllers::where([
            ['name', 'LIKE', '%' . $searchValue . '%']
        ])->pluck('id');

        $items= Route::where(function ($query) use ($searchValue, $controllers){
            $query->where('id', '=', $searchValue)
                ->orWhere('title', 'LIKE', '%' . $searchValue . '%')
                ->orWhere('description', 'LIKE', '%' . $searchValue . '%')
                ->orWhere('search', 'LIKE', '%' . $searchValue . '%')
                ->orWhereIn('controller', $controllers);
        });
        if($post) {
            if($post['order'][0]['dir']=='asc') {
                $items= $items->oldest($post['table'][$post['order'][0]['column']]);
            } else {
                $items= $items->latest($post['table'][$post['order'][0]['column']]);
            }
            $data= [
                'total'=> $items->count(),
                'data'=> $items->skip($post['start'])->take($post['length'])->get()
            ];
            return $data;
        } else {
            return $items->get();
        }
    }

    public function ajaxTableRoutes($post) {
        $post['table']= [
            'id', 'title', 'name', 'controller'
        ];
        $items= $this->searchRoutes($post['search']['value'], $post);
        $data= array();
        foreach ($items['data'] as $item) {
            $data[]= [
                $item->id,
                $item->title,
                $item->name,
                $item->getController->name,
                "<div class='pull-right'>
                    <a href='#' class='btn btn-danger btn-sm sa-warning' data-id='".$item->id."'><i class=\"fa fa-trash\"></i></a>
                    <a href=".route('admin.routes.info', ['id'=> $item->id])." class='btn btn-primary btn-sm'><i class=\"fa fa-edit mg-r-10\"></i>Редактировать</a>
                </div>",
            ];
        }
//        if(isset($post['order'][0]['dir'])) {
//            if($post['order'][0]['dir']=='asc') {
//                $data= $data->sortByDesc(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            } else {
//                $data= $data->sortBy(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            }
//        };
        ## Response
        return array(
            "iTotalRecords" => $items['total'],
            "iTotalDisplayRecords" => $items['total'],
            "aaData" => $data
        );
    }

    public function updateRoute($data, $id) {
        if($id) {
            $item= Route::find($id);
            $mess= 'изменен';
        } else {
            $item= new Route;
            $mess= 'создан';
        }

//        $test= file(database_path('/seeds/admin/RoutesSeeder.php'));
//        $test[11].='        $data[]= [["type"=> "get",]];'."\r\n";
//        file_put_contents( database_path('/seeds/admin/RoutesSeeder.php'), $test );
        if(isset($data['add'])) {
            $controller= new Controllers;
            $controller->name= $data['newController'];
            $controller->save();
            $data['controller']= $controller->id;

            $str= '';
            foreach (collect($controller)->except(['updated_at', 'created_at', 'deleted_at']) as $key=> $value) {
                $str.="            '".$key."'=> '".$value."',\r\n";
            }
            $seeds= file(database_path('/seeds/admin/ControllersSeeder.php'));
            $seeds[count($seeds)-4].='        $data[]= ['."\r\n".$str."        ];\r\n";
            file_put_contents( database_path('/seeds/admin/ControllersSeeder.php'), $seeds );
            $this->saveHistory($controller, 'controllers', 'Создал контроллер ID:'.$controller->id);
            Artisan::call('make:controller Admin/'.$controller->name);
        }
        unset($data['newController'], $data['add']);

        foreach ($data as $key=> $value) {
            $item->$key= $value;
        }
        $item->search= json_encode($data);
//        $item->save()

        if($item->save()) {
            $data= [
                'mess'=>'Маршрут успешно '.$mess,
                'status'=> 'success',
            ];
            $this->saveHistory($item, 'routes', (($id) ? 'Изменил' : 'Создал').' машрут ID: '.$item->id);
        } else {
            $data= [
                'mess'=>'Маршрут не был '.$mess,
                'status'=> 'error',
            ];
        }
        $data['item']= $item;

        if(empty($id)) {
            $str= '';
            foreach (collect($item)->except(['updated_at', 'created_at', 'deleted_at']) as $key=> $value) {
                $str.="            '".$key."'=> '".$value."',\r\n";
            }
            $seeds= file(database_path('/seeds/admin/RoutesSeeder.php'));
            $seeds[count($seeds)-4].='        $data[]= ['."\r\n".$str."        ];\r\n";
            file_put_contents( database_path('/seeds/admin/RoutesSeeder.php'), $seeds );
        };
        return $data;

    }

    public function getRouteControllers() {
        return Controllers::get();
    }

    public function routesRefresh() {
        try {
            $seeds= file(database_path('/seeds/admin/RoutesSeeder.php'));
            array_splice($seeds, 13);
            File::put(database_path('/seeds/admin/RoutesSeeder.php'), $seeds);

            $items= Route::get();
            foreach ($items as $item) {
                $str= '';
                foreach (collect($item)->except(['updated_at', 'created_at', 'deleted_at']) as $key=> $value) {
                    $str.="            '".$key."'=> '".$value."',\r\n";
                }
                $seeds= file(database_path('/seeds/admin/RoutesSeeder.php'));
                $seeds[count($seeds)-1].='        $data[]= ['."\r\n".$str."        ];\r\n";
                file_put_contents( database_path('/seeds/admin/RoutesSeeder.php'), $seeds );
            }

            $seeds= file(database_path('/seeds/admin/RoutesSeeder.php'));
            $str='';
            $str.='        DB::table(\'routes\')->insert($data);'."\r\n";
            $str.="    }\r\n";
            $str.="}";
            $seeds[count($seeds)-1].= $str;
            file_put_contents( database_path('/seeds/admin/RoutesSeeder.php'), $seeds );

            return 'success';
        } catch (Exception $e) {
            return false;
        }
    }

    public function actionRoutes($action, $id) {
        $item= Route::withTrashed()->find($id);

        try {
            switch ($action) {
                case 'delete':
                    $item->delete();
                    $mess= 'Добавлено в корзину';
                    break;
                case 'forceDelete':
                    $item->forceDelete();
                    $mess= 'Удалено с базы';
                    break;
                case 'view':
                    if($item->view) {
                        $item->view= 0;
                        $mess= 'Помечено как не прочитанное';
                    } else {
                        $item->view= 1;
                        $mess= 'Помечено как прочитанное';
                    }
                    break;
                default:
                    $item->restore();
                    $mess= 'Востанновлено с корзины';
            }
            $item->save();
            $data= [
                'status'=> 'success',
                'mess'=> $mess
            ];
            return $data;
        } catch(Exception $e) {
            $data= [
                'status'=> 'error',
                'mess'=> $e
            ];
            return $data;
        }
    }
}