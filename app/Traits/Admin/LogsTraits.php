<?php

namespace App\Traits\Admin;

use App\Requests\Request;
use File;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use Carbon\Carbon;

trait LogsTraits {

    private static $file;
    private static $levels_classes = [
        'debug' => 'info',
        'info' => 'info',
        'notice' => 'info',
        'warning' => 'warning',
        'error' => 'danger',
        'critical' => 'danger',
        'alert' => 'danger',
        'emergency' => 'danger',
        'processed' => 'info',
    ];
    private static $levels_imgs = [
        'debug' => 'info',
        'info' => 'info',
        'notice' => 'info',
        'warning' => 'warning',
        'error' => 'warning',
        'critical' => 'warning',
        'alert' => 'warning',
        'emergency' => 'warning',
        'processed' => 'info',
    ];
    private static $log_levels = [
        'emergency',
        'alert',
        'critical',
        'error',
        'warning',
        'notice',
        'info',
        'debug',
        'processed',
    ];

    public static function setFile($file)
    {
        $file = self::pathToLogFile($file);
        if (app('files')->exists($file)) {
            self::$file = $file;
        }
    }
    public static function pathToLogFile($file)
    {
        $logsPath = storage_path('logs');

        if (app('files')->exists($file)) { // try the absolute path
            return $file;
        }
        $file = $logsPath.'/'.$file;
        if (dirname($file) !== $logsPath) {
            throw new \Exception('No such log file');
        }
        return $file;
    }
    public static function getFileName()
    {
        return basename(self::$file);
    }
    public static function getBugs() {
        $log = [];
        $pattern = '/\[\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\].*/';
        if (!self::$file) {
            $log_file = self::getFiles();
            if (!count($log_file)) {
                return [];
            }
            self::$file = $log_file[0];
        }
        if (app('files')->size(self::$file) > 52428800) {
            return;
        }
        $file = app('files')->get(self::$file);
        preg_match_all($pattern, $file, $headings);
        if (!is_array($headings)) {
            return $log;
        }
        $log_data = preg_split($pattern, $file);
        if ($log_data[0] < 1) {
            array_shift($log_data);
        }
        foreach ($headings as $h) {
            for ($i = 0, $j = count($h); $i < $j; $i++) {
                foreach (self::$log_levels as $level) {
                    if (strpos(strtolower($h[$i]), '.'.$level) || strpos(strtolower($h[$i]), $level.':')) {
                        preg_match('/^\[(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})\](?:.*?(\w+)\.|.*?)'.$level.': (.*?)( in .*?:[0-9]+)?$/i', $h[$i], $current);
                        if (!isset($current[3])) {
                            continue;
                        }
                        $log[] = [
                            'context' => $current[2],
                            'level' => $level,
                            'level_class' => self::$levels_classes[$level],
                            'level_img' => self::$levels_imgs[$level],
                            'date' => $current[1],
                            'text' => $current[3],
                            'in_file' => isset($current[4]) ? $current[4] : null,
                            'stack' => preg_replace("/^\n*/", '', $log_data[$i]),
                        ];
                    }
                }
            }
        }
        return array_reverse($log);
    }
    public static function getFiles($basename = false)
    {
        $files = glob(storage_path().'/logs/*.log');
        $files = array_reverse($files);
        $files = array_filter($files, 'is_file');
        if ($basename && is_array($files)) {
            foreach ($files as $k => $file) {
                $files[$k] = basename($file);
            }
        }
        return array_values($files);
    }


    public static function getLogs() {
        $dir  = scandir(storage_path("admin/history/"), 1);
        $logs= collect();

        foreach ($dir as $key=> $file) {
            if(!strpos($file, 'json')) continue;

            $logFile=collect(json_decode(File::get(storage_path('admin/history/'.$file))));
            $logs->push($logFile);
        }

        return $logs->collapse();
    }

    public static function getLog($model, $id) {
        if(file_exists(storage_path('admin/history/'.$model.'.json'))) {
            $items= collect(json_decode(File::get(storage_path('admin/history/' . $model . '.json'))));
            return $items->where('id', $id);
        } else {
            return collect();
        }
    }
    public static function getBackup($model, $id, $backup) {
        if(file_exists(storage_path('admin/history/'.$model.'.json'))) {
            $items= collect(json_decode(File::get(storage_path('admin/history/' . $model . '.json'))))->where('id', $id);
            if(isset($items[$backup])) {
                return $items[$backup]->item;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public function authLogs($admin, $file, $mess) {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {$ip=$_SERVER['HTTP_CLIENT_IP'];} elseif (!empty($_SERVER['HTTP_CF_CONNECTING_IP'])) {$ip=$_SERVER['HTTP_CF_CONNECTING_IP'];} else {$ip=$_SERVER['REMOTE_ADDR'];}
        $location = $this->ip_info($ip);
        $location = $location['city'].', '.$location['country'];
        $json= [
            'admin_id'=> $admin->id,
            'admin'=> $admin->name,
            'date'=> date('Y-m-d h:i:s'),
            'desc'=> $mess,
            'model'=> $file,
            'ip'=> $ip,
            'browser'=> request()->header('User-Agent'),
            'location' =>$location,
        ];
        if(file_exists(storage_path('admin/history/'.$file.'.json'))) {
            $logs= collect(json_decode(File::get(storage_path('admin/history/'.$file.'.json'))));
        }
        if(empty($logs)) {
            $logs= collect();
        }
        $logs->put(date('Y-m-d h:i:s'), $json);

        File::put(storage_path('admin/history/'.$file.'.json'), $logs);

        return true;
    }

    public function logsAdmin($id, $type= false) {
        $dir  = scandir(storage_path("admin/history/"), 1);
        $logs= collect();

        foreach ($dir as $key=> $file) {
            if(!strpos($file, 'json')) continue;

            $logFile=collect(json_decode(File::get(storage_path('admin/history/'.$file))));
            $logs->push($logFile);
        }
        $logs= $logs->collapse();
        if($type) {
            $logs= $logs->where('admin_id', $id)->where('model', $type);
        }

        return $logs;
    }

    public function saveDebug($request, $exception) {
        $time= Carbon::now();
        $user= Auth::user();
        $data= [
            'user_id'=> (isset($user)) ? $user->id : 'Null',
            'table'=> (isset($user)) ? $user->getTable() : 'Null',
            'data'=> $time->format('Y-m-d H:i:s'),
            'route'=> $request->url(),
            'controller'=> $request->route()->getActionName(),
            'message'=>$exception->getMessage(),
            'file'=> $exception->getFile(),
            'line'=> $exception->getLine(),
        ];
        File::put(storage_path('/app/logs/'.$time->format('Y-m-d').'.txt'), $data);

//        GlobalFunctions::SendBotMsg(urlencode("Error Debug - ".$exception->getMessage()." Detail: Exception - ".get_class($exception).". File - ".$exception->getFile().". Line - ".$exception->getLine()));
    }
}