<?php

namespace App\Traits\Admin;

use App\Models\Languages;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;

trait LanguagesTraits {

    public function getLanguages() {
        return Languages::get();
    }
    public function getLanguage($key, $type= 'id') {
        return Languages::where($type, $key)->first();
    }
    public function getLangs() {
        return Languages::pluck('key');
    }

    public function searchLanguages($searchValue , $post= false, $table= false) {
        $items= Languages::where(function ($query) use ($searchValue){
            $query->where('id', '=', $searchValue)
                ->orwhere('key', 'LIKE', '%' . $searchValue . '%')
                ->orwhere('name', 'LIKE', '%' . $searchValue . '%');
        });
        if($post) {
            if($post['order'][0]['dir']=='asc') {
                $items= $items->oldest($post['table'][$post['order'][0]['column']]);
            } else {
                $items= $items->latest($post['table'][$post['order'][0]['column']]);
            }
            $data= [
                'total'=> $items->count(),
                'data'=> $items->skip($post['start'])->take($post['length'])->get()
            ];
            return $data;
        } else {
            return $items->get();
        }
    }

    public function ajaxTableLanguages($post) {
        $post['table']= [
            'id', 'name', 'email', 'role_id',
        ];
        $items= $this->searchLanguages($post['search']['value'], $post);
        $accessRole=$this->accessesRoles($this->admin, 'AdminRolesController')['view'];

        $data= array();
        foreach ($items['data'] as $item) {
            $data[]= [
                $item->id,
                $item->key,
                $item->name,
                "<a href=".route('admin.languages.info', ['id'=> $item->id])." class='btn btn-primary btn-sm pull-right'><i class=\"fa fa-edit mg-r-10\"></i>Редактировать</a>",
            ];
        }
//        if(isset($post['order'][0]['dir'])) {
//            if($post['order'][0]['dir']=='asc') {
//                $data= $data->sortByDesc(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            } else {
//                $data= $data->sortBy(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            }
//        };
        ## Response
        return array(
            "iTotalRecords" => $items['total'],
            "iTotalDisplayRecords" => $items['total'],
            "aaData" => $data
        );
    }

    public function updateLanguage($data, $id) {
        if($id) {
            $item= Languages::find($id);
            $key= $item->key;
            $mess= 'изменен';

        } else {
            $item= new Languages;
            $key= $data['key'];
            $mess= 'создан';
        }
        $file=$key.'.json';
        $translation= $data['translation'];
        File::put(resource_path('/lang/'.$file), $data['translation']);

        unset($data['translation']);
        foreach ($data as $key=> $value) {
            $item->$key= $value;
        }

//        $item->save()
        if($item->save()) {
            $data= [
                'mess'=>'Перевод успешно '.$mess,
                'status'=> 'success',
            ];
            $item->translation= $translation;
            $this->saveHistory($item, 'languages', (($id) ? 'Изменил' : 'Создал').' перевод ID: '.$item->id);
        } else {
            $data= [
                'mess'=>'Перевод не был '.$mess,
                'status'=> 'error',
            ];
        }
        $data['item']= $item;

        if(empty($id)) {
            $lang= $item->key;

            Schema::table('pages', function($table) use ($lang)
            {
                $table->string('title_'.$lang);
                $table->text('desc_'.$lang)->nullable();
                $table->text('content_'.$lang)->nullable();
                $table->string('meta_title_'.$lang)->nullable();
                $table->string('meta_desc_'.$lang)->nullable();
                $table->string('meta_key_'.$lang)->nullable();
            });
        }

        return $data;

    }
}
