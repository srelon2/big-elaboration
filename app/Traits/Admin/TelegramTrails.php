<?php

namespace App\Traits\Admin;

use App\Models\Admin\TelegramBot;
use App\Models\Admin\AdminsTelegram;
use Mockery\Exception;

trait TelegramTrails {
    public function updateAdminTelegram($chat_id, $data= false, $id= false) {
        $id= ($id) ? $id : $this->admin->id;
        $item= AdminsTelegram::where([
            ['chat_id', '=', $chat_id]
        ])->first();

        if(isset($item) && $item->admin_id!=$id) return false;
        if(empty($item)) {
            $item= new AdminsTelegram;
            $item['admin_id']= $id;
            $item['chat_id']= $chat_id;
            $item['active']= 1;
        }

        $item->save();
        return $item->id;
    }
    public function getChatID($id) {
        return AdminsTelegram::find($id)->chat_id;
    }
}