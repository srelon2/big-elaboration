<?php

namespace App\Traits\Admin;

use App\Mail\SendMessage;
use App\Models\Admin\Answers;
use App\Models\Supports;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;

trait SupportsTraits {



    public function getSupports($where= false) {
        if($where) {
            $supports= Supports::where([$where]);
        } else {
            $supports= Supports::query();
        }

        return $supports->get();
    }
    public function getSupport($key, $type= 'id') {
        return Supports::where($type, $key)->first();
    }

    public function searchSupports($searchValue , $post= false, $table= false, $type, $id) {
        switch ($type) {
            case 'inbox':
                $items= Supports::where('type', '!=', 'archive');
                break;
            case 'trash':
                if($table=='supports') {
                    $items= Supports::onlyTrashed();
                } else {
                    $items= Answers::onlyTrashed();
                }
                break;
            case 'draft':
                $items= Answers::where('drafts', 1);
                break;
            case 'sent':
                $items= Answers::where('drafts', 0);
                break;
            default:
                $items= Supports::where('type', '=', $type);
        }
        switch($table) {
            case 'answers':
                if($id) $items= $items->where('indicator_id', $id);
                $items= $items->where(function ($query) use ($searchValue){
                    $query->where('id', '=', $searchValue)
                        ->orwhere('name', 'LIKE', '%' . $searchValue . '%')
                        ->orwhere('email', 'LIKE', '%' . $searchValue . '%')
                        ->orwhere('subject', 'LIKE', '%' . $searchValue . '%')
                        ->orwhere('text', 'LIKE', '%' . $searchValue . '%');
                });
            break;
            default:
            $items= $items->where(function ($query) use ($searchValue){
                $query->where('id', '=', $searchValue)
                    ->orwhere('name', 'LIKE', '%' . $searchValue . '%')
                    ->orwhere('email', 'LIKE', '%' . $searchValue . '%')
                    ->orwhere('text', 'LIKE', '%' . $searchValue . '%')
                    ->orwhere('number', 'LIKE', '%' . $searchValue . '%');
            });
        }
        if($post) {
            if($post['order'][0]['dir']=='asc') {
                $items= $items->oldest($post['table'][$post['order'][0]['column']]);
            } else {
                $items= $items->latest($post['table'][$post['order'][0]['column']]);
            }
            $data= [
                'total'=> $items->count(),
                'data'=> $items->skip($post['start'])->take($post['length'])->get()
            ];
            return $data;
        } else {
            return $items->get();
        }
    }

    public function ajaxTableSupports($post, $type, $table, $id) {
        $post['table']= [
            false, false, 'email', 'text', 'created_at'
        ];
        $items= $this->searchSupports($post['search']['value'], $post, $table, $type, $id);

        $data= array();
        foreach ($items['data'] as $item) {
            switch ($type) {
                case 'trash':
                    $btnGroup=
                        '<div class="btn-group btn-item" data-id="'.$item->id.'">
                            <a href="#" class="btn btn-table btn-success btn-sm mg-r-5" data-type="inbox" title="Восстановить" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-inbox"></i></a>
                            <a href="#"  class="btn btn-table btn-danger btn-sm mg-r-5" data-type="forceDelete" title="Удалить" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-trash-o"></i></a>
                        </div>';
                    break;
                case 'draft':
                case 'sent':
                    $btnGroup= '<div class="btn-group btn-item" data-id="'.$item->id.'"><a href="#" class="btn btn-table btn-danger btn-sm mg-r-5" data-type="deleted" title="Добавить в корзину" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-trash-o"></i></a></div>';
                    break;
                default:
//                    <a href="#" class="btn btn-table btn-warning btn-sm mg-r-5" data-type="'.(($item->type=='archive') ? '' : 'archive').'" title="'.(($item->type=='archive') ? 'Убрать из архива' : 'Добавить в архив').'" data-value="archive" data-toggle="tooltip" data-placement="bottom"><i class="fa '.(($item->type=='archive') ? 'fa-folder-open-o' : 'fa-folder-o').'"></i></a>
                    $btnGroup=
                        '<div class="btn-group btn-item" data-id="'.$item->id.'">
                            <a href="#" class="btn '.(($item->view) ? 'btn-default' : 'btn-primary').' btn-table btn-sm mg-r-5" data-type="'.(($item->view) ? 'viewed' : 'not-viewed').'" title="'.(($item->view) ? 'Пометить как непрочитанное' : 'Пометить как прочитанное').'" data-toggle="tooltip" data-placement="bottom" data-value="view"><i class="fa '.(($item->view) ? 'fa-envelope-open' : 'fa-envelope').'"></i></a>
                            <a href="'.route('admin.support.compose', ['id'=>$item->id, 'answer'=> 'answer']).'" class="btn btn-success btn-sm mg-r-5" data-toggle="tooltip" data-container="body" title="Ответить"><i class="fa fa-reply"></i></a>
                            <a href="'.route('admin.supports', ['type'=>'sent', 'table'=>'answers', 'id'=> $item->id]).'" class="btn btn-info btn-sm mg-r-5" data-toggle="tooltip" data-container="body" title="Ответы"><i class="fa fa-share"></i></a>
                            <a href="#" class="btn btn-table btn-danger btn-sm mg-r-5" data-type="deleted" title="Добавить в корзину" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-trash-o"></i></a>
                        </div>';
            }
            $data[]= [
                '<div class="d-xs-flex"><input id="item-'.$item->id.'" data-id="'.$item->id.'" data-group="all" type="checkbox" class="checkbox-all checkbox filled-in" value="'.$item->id.'"  name="id[]"><label for="item-'.$item->id.'" class="ckbox mg-b-0"></label></div>',
                ($table== 'supports') ? '<a href="#" class="btn-table" data-type="'.(($item->type=='important') ? '' : 'important').'" data-value="important" title="'.(($item->type=='important') ? 'Убрать из избранного' : 'Добавить в избранное').'" data-toggle="tooltip" data-placement="bottom"><i class="fa text-yellow '.(($item->type=='important') ? 'fa-star' : 'fa-star-o').'"></i></a>' : '',
                (isset($item->email)) ? $item->email : 'Не указан',
                '<a href="'.(($item->drafts && !$item->trashed()) ? route('admin.support.compose', ['id'=>$item->id]) : route('admin.support.read', ['id'=>$item->id, 'table'=>$table])).'" class="message '.(($item->view==0) ? 'tx-bold' : '').'">'.mb_strimwidth(htmlspecialchars( $item->text), 0, 80, "...").'</a>',
                $item->created_at->format('Y-m-d H:i:s'),
                $btnGroup,
            ];
        }
//        if(isset($post['order'][0]['dir'])) {
//            if($post['order'][0]['dir']=='asc') {
//                $data= $data->sortByDesc(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            } else {
//                $data= $data->sortBy(function($query, $key) use($post) {
//                    return $query[$post['order'][0]['column']];
//                });
//            }
//        };
        ## Response
        return array(
            "iTotalRecords" => $items['total'],
            "iTotalDisplayRecords" => $items['total'],
            "aaData" => $data
        );
    }

    public function actionInbox($data, $table) {
        switch ($table) {
            case 'answers':
                $items= Answers::whereIn('id',$data['checked']);
                break;
            default:
                $items= Supports::whereIn('id',$data['checked']);
        }
        try {
            switch ($data['type']) {
                case 'deleted':
                    $items->delete();
                    $mess= 'Добавлено в корзину';
                    break;
                case 'not-viewed':
                    $items->update(['view'=>1]);
                    $mess= 'Помечено как прочитанное';
                    break;
                case 'viewed':
                    $items->update(['view'=>0]);
                    $mess= 'Помечено как не прочитанное';
                    break;
                case 'inbox':
                    $items->restore();
                    $mess= 'Востанновлено с корзины';
                    break;
                case 'forceDelete':
                    $items->forceDelete();
                    $mess= 'Удалено с базы';
                    break;
                case 'archive':
                    $items->update(['type'=>'archive']);
                    $mess= 'Добавлено в архив';
                    break;
                case 'important':
                    $items->update(['type'=>'important']);
                    $mess= 'Добавлено в избранное';
                    break;
                default:
                    $items->update(['type'=>'inbox']);
                    $mess= 'Добавлено в входящие';
            }
            $data= [
                'status'=> 'success',
                'mess'=> $mess
            ];
            return $data;
        } catch(Exception $e) {
            $data= [
                'status'=> 'error',
                'mess'=> $e
            ];
            return $data;
        }
    }

    public function actionMessage($action, $id, $table) {
        switch ($table) {
            case 'answers':
                $item= Answers::withTrashed()->find($id);
                break;
            default:
                $item= Supports::withTrashed()->find($id);
        }
        try {
            switch ($action) {
                case 'delete':
                    $item->delete();
                    $mess= 'Добавлено в корзину';
                    break;
                case 'forceDelete':
                    $item->forceDelete();
                    $mess= 'Удалено с базы';
                    break;
                case 'view':
                    if($item->view) {
                        $item->view= 0;
                        $mess= 'Помечено как не прочитанное';
                    } else {
                        $item->view= 1;
                        $mess= 'Помечено как прочитанное';
                    }
                    break;
                default:
                    $item->restore();
                    $mess= 'Востанновлено с корзины';
            }
            $item->save();
            $data= [
                'status'=> 'success',
                'mess'=> $mess
            ];
            return $data;
        } catch(Exception $e) {
            $data= [
            'status'=> 'error',
            'mess'=> $e
            ];
            return $data;
        }
    }
    public function sendMessage($data, $id, $type) {
        if($type) {
            $item= new Answers;
            $mess= 'добавлен';
            $item->type= $type;
            $item->indicator_id= $id;
            $new= true;
        } else {
            if($id) {
                $item= Answers::withTrashed()->find($id);
                if($item->drafts) {
                    $mess= 'изменен';
                } else {
                    $item= new Answers;
                    $mess= 'добавлен';
                    $new= true;
                }
                $item->drafts= 0;
            } else {
                $item= new Answers;
                $mess= 'добавлен';
                $new= true;
            }
        }
        $submit= $data['submit'];
        unset($data['submit']);

        foreach ($data as $key=> $value) {
            $item->$key= $value;
        }
        $item->admin_id= $this->admin->id;

        if($submit=='draft') {
            $item->drafts= 1;
            $data= [
                'mess'=>'Черновик успешно '.$mess,
                'status'=> 'success'
            ];
        } else {
            try {
                Mail::to($item->email)->send(new SendMessage($item));
                $data= [
                    'mess'=>'Сообщение успешно отправлено',
                    'status'=> 'success'
                ];
            }catch (\Exception $e){
                $data= [
                    'mess'=>'Сообщение не было отправлено',
                    'status'=> 'error'
                ];
            }
        }
        $item->save();
        $this->saveHistory($item, 'answers', (($submit=='draft') ? (($new) ? 'Изменил' : 'Добавил').' черновик' : 'Отправил').' сообщение ID: '.$item->id);

        return $data;
    }

    public function getMessage($id, $table, $select= false) {
        switch ($table) {
            case 'answers':
                $item= Answers::withTrashed();
                break;
            default:
                $item= Supports::withTrashed();
        }
        if($select) {
            $item= $item->select($select);
        }

        return $item->find($id);
    }
}
