<?php

namespace App\Traits;

use App\Models\Supports;
use Mockery\Exception;

trait SupportsTraits{

    public function takeMessage($data) {
        $item= New Supports;
        foreach ($data as $key=>$value) {
            $item->$key= htmlspecialchars($value);
        }
        if($item->save()) {
            return 'success';
        } else {
            return 'error';
        }
    }
}