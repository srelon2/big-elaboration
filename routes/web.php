<?php
    Route::post('/connect/bd', 'Controller@connect');
    try {
        DB::connection()->getPdo();
    } catch (\Exception $e) {
        dd(DB::connection()->getPdo());
    }
$real_path = realpath(__DIR__).DIRECTORY_SEPARATOR.'front_routes'.DIRECTORY_SEPARATOR;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*///Переключение языков
Route::get('setlocale/{lang}', function ($lang) {

    $referer = Redirect::back()->getTargetUrl(); //URL предыдущей страницы
    $parse_url = parse_url($referer, PHP_URL_PATH); //URI предыдущей страницы

    //разбиваем на массив по разделителю
    $segments = explode('/', $parse_url);

    //Если URL (где нажали на переключение языка) содержал корректную метку языка
    if (in_array($segments[1], App\Http\Middleware\LocaleMiddleware::languages())) {

        unset($segments[1]); //удаляем метку
    }

    //Добавляем метку языка в URL (если выбран не язык по-умолчанию)
    if ($lang != App\Http\Middleware\LocaleMiddleware::mainLanguage()){
        array_splice($segments, 1, 0, $lang);
    }

    //формируем полный URL
    $url = Request::root().implode("/", $segments);

    //если были еще GET-параметры - добавляем их
    if(parse_url($referer, PHP_URL_QUERY)){
        $url = $url.'?'. parse_url($referer, PHP_URL_QUERY);
    }
    return redirect($url); //Перенаправляем назад на ту же страницу

})->name('setlocale');

Route::group(['prefix' => App\Http\Middleware\LocaleMiddleware::getLocale()], function() use($real_path){

    Route::get('/', 'IndexController@index')->name('main');
    Route::get('/about', 'PagesController@about')->name('about');
    Route::get('/education', 'EducationController@index')->name('education');
    Route::get('/education/mindful', 'EducationController@mindful')->name('education.mindful');
    Route::get('/education/heroic-virtues', 'EducationController@heroic')->name('education.heroic');
    Route::get('/education/healthy-breathing', 'EducationController@healthy')->name('education.healthy');
    Route::get('/education/open-palms', 'EducationController@openPalms')->name('education.open-palms');
    Route::get('/privacy', 'PagesController@privacy')->name('privacy');
    Route::get('/coaching', 'CoachingController@index')->name('coaching');
    Route::get('/coaching-form', 'CoachingController@form')->name('coaching.form');
    Route::get('/blog', 'BlogsController@index')->name('blog');
    Route::get('/developers', 'DevelopersController@index')->name('developers');
    Route::get('/demo', 'DevelopersController@demo')->name('demo');


    include_once($real_path .'routes.php');
    /********** AjaxController *************/
    include_once($real_path .'ajax.php');

    /********** User Auth *************/
    Auth::routes();
    /********** Admin Auth *************/

    //pages
    Route::get('/page/{url}', 'PagesController@page')->name('page');

    Route::post('/support', 'PagesController@support')->name('support');

});
include_once($real_path .'admin_auth.php');