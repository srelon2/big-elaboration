<?php
Route::prefix('adminroute')->group(function () {
    Route::get('/login', 'Admin\Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Admin\Auth\LoginController@login');
    Route::get('/logout', 'Admin\Auth\LoginController@logout')->name('admin.logout');
    Route::get('/auth/validate','Admin\Auth\LoginController@getValidateToken');
    Route::post('/auth/validate', 'Admin\Auth\LoginController@postValidateToken')->name('admin.login.validate');
    Route::get('/auth/resend_auth_code/{id}','Admin\Auth\LoginController@sendAuthCodeEmail')->name('admin.auth.validate.resend');

    Route::get('/password/reset', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset/{token}', 'Admin\Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('/password/reset', 'Admin\Auth\ResetPasswordController@reset');
});